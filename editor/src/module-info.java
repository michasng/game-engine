module dice.editor {
  exports de.hsa.dice.editor;
  exports de.hsa.dice.editor.controller;
  exports de.hsa.dice.editor.controller.editors;
  exports de.hsa.dice.editor.controller.editors.nested;
  exports de.hsa.dice.editor.controller.tree;
  exports de.hsa.dice.editor.view;
  exports de.hsa.dice.editor.view.scripts;

  requires java.desktop;
  requires java.logging;

  requires javafx.fxml;
  requires transitive javafx.graphics;
  requires transitive javafx.controls;

  requires transitive dice.core;
  requires transitive fxtabs;
  requires fxcombobox;
  requires JBox2D;

  // open controllers to be read from fxml files
  opens de.hsa.dice.editor.controller;
  opens de.hsa.dice.editor.controller.editors;
  opens de.hsa.dice.editor.controller.editors.nested;
  // opens view.ui;
  opens view.graphics;
}