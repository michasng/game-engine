package de.hsa.dice.editor;

import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import com.eclipsesource.json.ParseException;

import de.hsa.dice.core.util.io.JsonSerializable;

public class ClipboardData implements JsonSerializable {

  private String type;
  private JsonValue value;

  public ClipboardData(String type, JsonValue value) {
    this.type = type;
    this.value = value;
  }

  public ClipboardData(String stringData) throws ParseException {
    this(JsonObject.readFrom(stringData));
  }

  public ClipboardData(JsonObject json) {
    type = json.get("type").asString();
    value = json.get("value");
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public JsonValue getValue() {
    return value;
  }

  public void setValue(JsonValue value) {
    this.value = value;
  }

  @Override
  public JsonObject toJson() {
    return new JsonObject().add("type", type).add("value", value);
  }

  @Override
  public String toString() {
    return toJson().toString();
  }

}
