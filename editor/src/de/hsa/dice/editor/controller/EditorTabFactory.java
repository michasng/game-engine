package de.hsa.dice.editor.controller;

import java.util.LinkedList;
import java.util.logging.Logger;

import de.hsa.dice.core.dataobject.DataObject;
import de.hsa.dice.core.view.FXMLUtil;
import de.hsa.dice.core.view.FXMLUtil.FxNodeContainer;
import de.hsa.dice.editor.controller.editors.EditorController;
import fxtabs.DraggableTab;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class EditorTabFactory {

  private static final Logger logger = Logger.getLogger(EditorTabFactory.class.getName());

  protected Image icon;
  protected String fxmlPath;

  protected TabPane appTabPane;
  protected LinkedList<DraggableTab> tabs;

  public EditorTabFactory(Image icon, String fxmlPath, TabPane appTabPane) {
    this.icon = icon;
    this.fxmlPath = fxmlPath;
    this.appTabPane = appTabPane;

    tabs = new LinkedList<>();
  }

  public static class TabContainer {
    public final FxNodeContainer<Node, EditorController> fxNodeContainer;
    public final DraggableTab tab;

    public TabContainer(FxNodeContainer<Node, EditorController> fxNodeContainer, DraggableTab tab) {
      this.fxNodeContainer = fxNodeContainer;
      this.tab = tab;
    }
  }

  public void clearTabs() {
    // iterate backwards to prevent ConcurrentModificationException
    for (int i = tabs.size() - 1; i >= 0; i--) {
      var tab = tabs.get(i);
      tab.getOnClosed().handle(null);
      tab.getTabPane().getTabs().remove(tab);
    }
  }

  public TabContainer addTab(String objectName) {
    logger.fine("Adding Tab for DataObject: " + objectName);
    DraggableTab tab = new DraggableTab();
    var container = initTabContent(tab, objectName);
    // tab.getStyleClass().add(title.toLowerCase() + "-editor");
    ImageView graphic = new ImageView(icon);
    graphic.setFitWidth(16);
    graphic.setFitHeight(16);
    tab.setTitleGraphic(graphic);
    appTabPane.getTabs().add(tab);
    SingleSelectionModel<Tab> selectionModel = appTabPane.getSelectionModel();
    selectionModel.select(tab);
    setContextMenu(tab);
    return new TabContainer(container, tab);
  }

  private void setContextMenu(DraggableTab tab) {
    var tabsToRemove = new LinkedList<Tab>();
    Runnable removeTabs = () -> {
      for (var tabToRemove : tabsToRemove) {
        if (tabToRemove.getOnClosed() != null)
          tabToRemove.getOnClosed().handle(null);
        tabToRemove.getTabPane().getTabs().remove(tabToRemove);
      }
      tabsToRemove.clear();
    };

    var contextMenu = new ContextMenu();
    var closeItem = new MenuItem("Close");
    closeItem.setOnAction(actionEvent -> {
      tab.getOnClosed().handle(null);
      tab.getTabPane().getTabs().remove(tab);
    });
    var closeOthersItem = new MenuItem("Close Others");
    closeOthersItem.setOnAction(actionEvent -> {
      var tabPaneTabs = tab.getTabPane().getTabs();
      for (var tabPaneTab : tabPaneTabs)
        if (tabPaneTab != tab)
          tabsToRemove.add(tabPaneTab);
      removeTabs.run();
    });
    var closeRightItem = new MenuItem("Close Tabs to the Right");
    closeRightItem.setOnAction(actionEvent -> {
      var tabPaneTabs = tab.getTabPane().getTabs();
      var index = tabPaneTabs.indexOf(tab);
      for (var tabPaneTab : tabPaneTabs)
        if (tabPaneTabs.indexOf(tabPaneTab) > index)
          tabsToRemove.add(tabPaneTab);
      removeTabs.run();
    });

    var closeAllItem = new MenuItem("Close All");
    closeAllItem.setOnAction(actionEvent -> {
      var tabPaneTabs = tab.getTabPane().getTabs();
      for (var tabPaneTab : tabPaneTabs)
        tabsToRemove.add(tabPaneTab);
      removeTabs.run();
    });

    var detachItem = new MenuItem("Detach");
    detachItem.setOnAction(actionEvent -> tab.detachAtOrigin());

    contextMenu.getItems().addAll(closeItem, closeOthersItem, closeRightItem, new SeparatorMenuItem(), closeAllItem,
        new SeparatorMenuItem(), detachItem);
    tab.setContextMenu(contextMenu);
  }

  public void refreshTab(final DraggableTab tab, String objectName) {
    logger.fine("Refreshing tab: " + objectName);
    tab.getOnClosed().handle(null);
    initTabContent(tab, objectName);
    SingleSelectionModel<Tab> selectionModel = tab.getTabPane().getSelectionModel();
    selectionModel.select(tab);
  }

  public FxNodeContainer<Node, EditorController> initTabContent(final DraggableTab tab, String objectName) {
    FxNodeContainer<Node, EditorController> container = FXMLUtil.load(fxmlPath);
    DataObject dataObject = container.controller.loadDataObject(objectName);

    tab.setLabelText(dataObject.getName());
    var tabNameSub = dataObject.getNameProperty().addListener((oldValue, value) -> tab.setLabelText(value));

    // auto-remove listeners when tab is closed
    tab.setOnClosed(event -> {
      tabs.remove(tab);
      logger.fine("On Tab Closed: " + tab.getLabelText());
      container.controller.onClose();
      tabNameSub.cancelSubscription();
    });

    tab.setContent(container.node);

    tab.setOnSelectionChanged((event) -> {
      if (!tab.isSelected())
        return;
      logger.fine("Selecting tab: " + dataObject.getName());
      if (container.controller.needsRefresh()) {
        refreshTab(tab, dataObject.getName());
      }
    });

    tabs.add(tab);
    return container;
  }

}
