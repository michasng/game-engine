package de.hsa.dice.editor.controller;

import de.hsa.dice.core.dataobject.DataObject;
import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.editor.controller.editors.DataEditorController;
import fxtabs.DraggableTab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;

public class DataEditorTabFactory extends EditorTabFactory {

  protected DataType dataType;

  public DataEditorTabFactory(Image icon, String fxmlPath, TabPane appTabPane, DataType dataType) {
    super(icon, fxmlPath, appTabPane);
    this.dataType = dataType;
  }

  @Override
  public TabContainer addTab(String objectName) {
    var tab = findTab(objectName);
    if (tab == null)
      return super.addTab(objectName);

    // refresh not needed, if all controllers have implemented required listeners
    refreshTab(tab, objectName);
    return null;
  }

  public void removeTab(String objectName) {
    var tabToRemove = findTab(objectName);
    if (tabToRemove == null)
      return;
    tabToRemove.getOnClosed().handle(null);
    tabToRemove.getTabPane().getTabs().remove(tabToRemove);
  }

  public DraggableTab findTab(String objectName) {
    for (var tab : tabs) {
      Object userData = tab.getContent().getUserData();
      if (userData instanceof DataEditorController<?>) {
        @SuppressWarnings("unchecked")
        DataEditorController<DataObject> ctrl = (DataEditorController<DataObject>) userData;
        if (ctrl.getDataType() == dataType && ctrl.getDataObject().getName().equals(objectName)) {
          return tab;
        }
      } else
        throw new Error("Controller is not of type DataEditorController!");
    }
    return null;
  }

  public DataType getDataType() {
    return dataType;
  }

}
