package de.hsa.dice.editor.controller;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import de.hsa.dice.core.Config;
import de.hsa.dice.core.Console;
import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.manager.ProjectDataManager;
import de.hsa.dice.core.property.PropertyListener;
import de.hsa.dice.core.property.PropertySubscription;
import de.hsa.dice.core.util.io.ProjectFileOperator;
import de.hsa.dice.core.view.IconRegistry;
import de.hsa.dice.editor.Editor;
import de.hsa.dice.editor.controller.tree.ContextMenuTreeItem.ContextMenuTreeCell;
import de.hsa.dice.editor.controller.tree.DataTypeTreeItem;
import de.hsa.dice.editor.controller.tree.ProjectTreeItem;
import fxtabs.DraggableTab;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.Window;

public class AppController {

  private static final Logger logger = Logger.getLogger(AppController.class.getName());

  @FXML
  private TreeView<String> treeView;

  @FXML
  private Menu createMenu;

  @FXML
  private TabPane tabPane;

  private EditorTabFactory playTabFactory, projectSettingsTabFactory, previewScriptTabFactory;

  private EnumMap<DataType, DataEditorTabFactory> dataTypeTabFactories;

  private DraggableTab currentPlayTab;

  @FXML
  private Menu recentProjects;

  @FXML
  private ConsoleController consoleController;

  @FXML
  private ImageView playIconView;

  private PropertySubscription<String> themeSub;

  public void initialize() {
    // treeView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    treeView.setShowRoot(true);
    treeView.setCellFactory(treeView -> new ContextMenuTreeCell()); // enables context menus
    treeView.setRoot(new ProjectTreeItem());
    treeView.getRoot().setExpanded(true);

    IconRegistry icons = IconRegistry.getInstance();
    playIconView.setImage(icons.play);
    playTabFactory = new EditorTabFactory(icons.dice, "game.fxml", tabPane);
    projectSettingsTabFactory = new DataEditorTabFactory(icons.folder, "project.fxml", tabPane, DataType.PROJECT);
    previewScriptTabFactory = new EditorTabFactory(icons.scripts, "scriptPreview.fxml", tabPane);
    dataTypeTabFactories = new EnumMap<>(DataType.class);
    dataTypeTabFactories.put(DataType.SPRITE,
        new DataEditorTabFactory(icons.sprites, "sprite.fxml", tabPane, DataType.SPRITE));
    dataTypeTabFactories.put(DataType.TILE, new DataEditorTabFactory(icons.tiles, "tile.fxml", tabPane, DataType.TILE));
    dataTypeTabFactories.put(DataType.MAP, new DataEditorTabFactory(icons.maps, "map.fxml", tabPane, DataType.MAP));
    dataTypeTabFactories.put(DataType.ENTITY,
        new DataEditorTabFactory(icons.entities, "entity.fxml", tabPane, DataType.ENTITY));
    dataTypeTabFactories.put(DataType.SOUND,
        new DataEditorTabFactory(icons.sounds, "sound.fxml", tabPane, DataType.SOUND));
    dataTypeTabFactories.put(DataType.SCRIPT,
        new DataEditorTabFactory(icons.scripts, "script.fxml", tabPane, DataType.SCRIPT));

    List<TreeItem<String>> items = new LinkedList<>();
    items.add(new DataTypeTreeItem(dataTypeTabFactories.get(DataType.SPRITE)));
    items.add(new DataTypeTreeItem(dataTypeTabFactories.get(DataType.TILE)));
    items.add(new DataTypeTreeItem(dataTypeTabFactories.get(DataType.MAP)));
    items.add(new DataTypeTreeItem(dataTypeTabFactories.get(DataType.ENTITY)));
    items.add(new DataTypeTreeItem(dataTypeTabFactories.get(DataType.SOUND)));
    items.add(new DataTypeTreeItem(dataTypeTabFactories.get(DataType.SCRIPT)));
    treeView.getRoot().getChildren().addAll(items);

    for (DataType dataType : DataType.values()) {
      MenuItem createType = new MenuItem(dataType.name);
      createType.setOnAction(actionEvent -> {
        dataTypeTabFactories.get(dataType).addTab(null);
      });
      createMenu.getItems().add(createType);
    }

    createRecentProjectsMenu();
  }

  public void createRecentProjectsMenu() {
    recentProjects.getItems().clear();
    var history = Config.getInstance().getProjectHistory();
    for (var projectFilePath : history) {
      var menuItem = new MenuItem(projectFilePath);
      menuItem.setOnAction(actionEvent -> {
        if (ProjectDataManager.getInstance().isDirty()) {
          boolean saveAndContinue = openSaveDialog();
          if (!saveAndContinue)
            return;
          onFileSave();
        }
        Config.getInstance().setProject(projectFilePath);
        reloadProject();
        var projectName = ProjectFileOperator.getInstance().getProjectName();
        Console.log("Project \"" + projectName + "\" opened.");
      });
      recentProjects.getItems().add(menuItem);
    }
  }

  public void loadTheme() {
    if (themeSub != null)
      themeSub.cancelSubscription();

    PropertyListener<String> themeListener = (oldValue, value) -> {
      for (var w : Window.getWindows())
        w.getScene().getRoot().setStyle("-fx-base: " + value);
    };
    themeSub = ProjectData.getCurrent().getThemeProperty().addListener(themeListener);

    var theme = ProjectData.getCurrent().getTheme();
    Editor.getInstance().getRoot().setStyle("-fx-base: " + theme);
  }

  public void reloadProject() {
    ProjectDataManager.getInstance().reload();
    clearTabs();
    ((ProjectTreeItem) treeView.getRoot()).reloadChildren();
    loadTheme();
    createRecentProjectsMenu();
  }

  public void clearTabs() {
    for (var tabFactory : dataTypeTabFactories.values())
      tabFactory.clearTabs();
    playTabFactory.clearTabs();
    projectSettingsTabFactory.clearTabs();
    previewScriptTabFactory.clearTabs();
  }

  @FXML
  public void onPlay() {
    ProjectData projectData = ProjectDataManager.getInstance().getCurrentProject();
    if (!ProjectDataManager.getInstance().startMapExists()) {
      Console.logError("Invalid startMap \"" + projectData.getStartMapName() + '\"');
      return;
    }

    if (currentPlayTab == null) {
      var container = playTabFactory.addTab(null);
      currentPlayTab = container.tab;
    } else {
      var tempTab = currentPlayTab; // currentPlayTab reference is lost in setOnClosed below
      playTabFactory.refreshTab(currentPlayTab, null);
      currentPlayTab = tempTab;
    }

    EventHandler<Event> oldHandler = currentPlayTab.getOnClosed();
    currentPlayTab.setOnClosed(event -> {
      oldHandler.handle(event);
      logger.fine("On tab Closed 2, setting old tab to null.");
      currentPlayTab = null;
    });

  }

  private boolean openSaveDialog() {
    Alert alert = new Alert(AlertType.CONFIRMATION);
    alert.setTitle("Confirm Save");
    alert.setHeaderText("Do you want to save the project?");
    // alert.setContentText("Save changes?");
    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
    stage.getIcons().add(IconRegistry.getInstance().dice);
    ImageView graphic = new ImageView(IconRegistry.getInstance().folder);
    graphic.setFitWidth(32);
    graphic.setFitHeight(32);
    alert.setGraphic(graphic);

    Optional<ButtonType> result = alert.showAndWait();
    if (result.get() == ButtonType.OK) {
      return true;
    } else {
      return false;
    }
  }

  private String openDirectoryDialog() {
    DirectoryChooser dialog = new DirectoryChooser();
    dialog.setInitialDirectory(new File(Config.BASE_DIR));
    File dir = dialog.showDialog(Editor.getInstance().getPrimaryStage());
    if (dir == null)
      return null;
    return dir.getPath() + File.separatorChar;
  }

  @FXML
  public void onFileNew() {
    if (ProjectDataManager.getInstance().isDirty()) {
      boolean saveAndContinue = openSaveDialog();
      if (!saveAndContinue)
        return;
      onFileSave();
    }

    String projectDirPath = openDirectoryDialog();
    if (projectDirPath == null)
      return;

    String projectName = new File(projectDirPath).getName();
    var projectFilePath = projectDirPath + projectName + ".dice";
    Config.getInstance().setProject(projectFilePath);

    reloadProject();
    Console.log("New Project \"" + projectName + "\" created.");
  }

  private String openProjectDialog() {
    FileChooser dialog = new FileChooser();
    ExtensionFilter filter = new ExtensionFilter("dice engine project files (*.dice)", "*.dice");
    dialog.getExtensionFilters().add(filter);
    // dialog.setSelectedExtensionFilter(filter);
    dialog.setInitialDirectory(new File(Config.BASE_DIR));
    File file = dialog.showOpenDialog(Editor.getInstance().getPrimaryStage());
    if (file == null)
      return null;
    return file.getPath();
  }

  @FXML
  public void onFileOpen() {
    if (ProjectDataManager.getInstance().isDirty()) {
      boolean saveAndContinue = openSaveDialog();
      if (!saveAndContinue)
        return;
      onFileSave();
    }

    String projectFilePath = openProjectDialog();
    if (projectFilePath == null)
      return;

    Config.getInstance().setProject(projectFilePath);

    reloadProject();

    var projectName = ProjectFileOperator.getInstance().getProjectName();
    Console.log("Project \"" + projectName + "\" opened.");
  }

  @FXML
  public void onFileClose() {
    Console.logError("onFileClose");
  }

  @FXML
  public void onFileSave() {
    ProjectDataManager.getInstance().save();
    Console.log("ProjectData saved.");
  }

  @FXML
  public void onFileSaveAs() {
    ProjectDataManager.getInstance().save();
    Console.log("ProjectData saved."); // TODO: choose destination directory
  }

  @FXML
  public void onFileRevert() {
    reloadProject();
    Console.log("Projectchanges reverted.");
  }

  @FXML
  public void onPreferences() {
    Console.logError("onPreferences");
  }

  @FXML
  public void onOpenInExplorer() {
    File file = new File(Config.getInstance().getString(Config.CURRENT_PROJECT_PATH));
    Desktop desktop = Desktop.getDesktop();
    try {
      desktop.open(file);
      Console.log("Opened project folder in Explorer.");
    } catch (IOException e) {
      Console.logError("Failed to open project folder in explorer.");
      Console.logError(e);
      // e.printStackTrace();
    }
  }

  @FXML
  public void onQuit() {
    // TODO: prompt "should save" before quit
    Platform.exit();
    System.exit(0);
  }

  @FXML
  public void onEditProjectSettings() {
    var projectName = ProjectData.getCurrent().getName();
    projectSettingsTabFactory.addTab(projectName);
  }

  @FXML
  public void onAbout() {
    Console.logError("onAbout");
  }

  public ConsoleController getConsole() {
    return consoleController;
  }

  public EditorTabFactory getPreviewScriptTabFactory() {
    return previewScriptTabFactory;
  }

}
