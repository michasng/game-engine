package de.hsa.dice.editor.controller.editors;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.Clip;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.dataobject.DataObject;
import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.SoundData;
import de.hsa.dice.core.game.SoundPlayer;
import de.hsa.dice.core.util.io.ProjectFileOperator;
import de.hsa.dice.editor.Editor;
import de.hsa.dice.editor.controller.DataFxUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class SoundEditorController implements DataEditorController<SoundData> {

  @FXML
  private TextField name;

  @FXML
  private Label relPathLabel;

  private SoundData dataObject;

  private Clip clip;

  public SoundEditorController() {
  }

  @Override
  public void initialize() {
    assert name != null;
    assert relPathLabel != null;
  }

  @Override
  public DataObject loadDataObject(String objectName) {
    var manager = ProjectData.getCurrent().getSubManager(DataType.SOUND, SoundData.class);
    if (objectName == null) {
      dataObject = manager.createNew();
      objectName = dataObject.getName();
    } else {
      dataObject = manager.get(objectName);
    }

    createBindings();
    return dataObject;
  }

  public void createBindings() {
    DataFxUtil.bind(dataObject.getNameProperty(), name);

    relPathLabel.setText(dataObject.getPath());
  }

  @FXML
  void onChooseFile(ActionEvent event) {
    var prjFileOp = ProjectFileOperator.getInstance();
    var fileChooser = new FileChooser();
    fileChooser.setInitialDirectory(new File(prjFileOp.getProjectPath()));
    fileChooser.setTitle("Open Resource File");
    fileChooser.getExtensionFilters().add(new ExtensionFilter("Image Files", "*.wav")); // "*.mp3", "*.ogg" don't work
    var selectedFile = fileChooser.showOpenDialog(Editor.getInstance().getPrimaryStage());
    if (selectedFile == null)
      return;

    var newAbsPath = selectedFile.getAbsolutePath();
    var newRelPath = prjFileOp.getRelativePath(newAbsPath);
    dataObject.setPath(newRelPath);
    relPathLabel.setText(dataObject.getPath());
  }

  @FXML
  void onPlay(ActionEvent event) {
    try {
      if (clip != null && clip.isRunning())
        clip.stop();
      var prjFileOp = ProjectFileOperator.getInstance();
      var absPath = prjFileOp.getAbsolutePath(dataObject.getPath());
      clip = SoundPlayer.loadExternalClip(absPath);
      SoundPlayer.play(clip);
    } catch (IOException e) {
      Console.logError(e);
    }
  }

  @FXML
  void onLoop(ActionEvent event) {
    try {
      if (clip != null && clip.isRunning())
        clip.stop();
      var prjFileOp = ProjectFileOperator.getInstance();
      var absPath = prjFileOp.getAbsolutePath(dataObject.getPath());
      clip = SoundPlayer.loadExternalClip(absPath);
      SoundPlayer.loop(clip);
    } catch (IOException e) {
      Console.logError(e);
    }
  }

  @FXML
  void onStop(ActionEvent event) {
    if (clip != null) {
      SoundPlayer.stop(clip);
      clip = null;
    }
  }

  @Override
  public void onClose() {
    if (clip != null)
      SoundPlayer.unloadClip(clip);
  }

  @Override
  public boolean needsRefresh() {
    return false;
  }

  @Override
  public SoundData getDataObject() {
    return dataObject;
  }

  @Override
  public DataType getDataType() {
    return DataType.SOUND;
  }

}
