package de.hsa.dice.editor.controller.editors;

import java.util.logging.Logger;

import de.hsa.dice.core.dataobject.DataObject;
import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.EntityData;
import de.hsa.dice.core.dataobject.MapData;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.TileData;
import de.hsa.dice.core.dataobject.manager.SubDataManager;
import de.hsa.dice.core.view.CanvasPane;
import de.hsa.dice.core.view.IconRegistry;
import de.hsa.dice.editor.controller.DataFxUtil;
import de.hsa.dice.editor.view.EntityPreviewScene;
import de.hsa.dice.editor.view.MapEditorGame;
import de.hsa.dice.editor.view.TilePreviewScene;
import javafx.fxml.FXML;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;

public class MapEditorController implements DataEditorController<MapData> {

  private static final Logger logger = Logger.getLogger(MapEditorController.class.getName());

  // the size of the preview images in pixels
  private static final int PREVIEW_SIZE = 32;

  @FXML
  private TextField name;

  @FXML
  private TextField numCols;

  @FXML
  private TextField numRows;

  @FXML
  private TextField gravity;

  @FXML
  private TilePane tileGrid;

  @FXML
  private TilePane entityGrid;

  @FXML
  private StackPane canvasStackPane;

  private CanvasPane canvasPane;

  private MapEditorGame mapEditor;

  private MapData dataObject;

  private int tilesRevision, entitiesRevision;

  @Override
  public void initialize() {
    var group = new ToggleGroup();

    addTileBtn(null, group); // erase tile button
    var tilesManager = ProjectData.getCurrent().getSubManager(DataType.TILE);
    for (var tileData : tilesManager.getDataObjects().values())
      addTileBtn((TileData) tileData, group);
    tilesRevision = tilesManager.getRevision();

    addEntityBtn(null, group); // erase entity button
    var entitiesManager = ProjectData.getCurrent().getSubManager(DataType.ENTITY);
    for (var entityData : entitiesManager.getDataObjects().values())
      addEntityBtn((EntityData) entityData, group);
    entitiesRevision = entitiesManager.getRevision();
  }

  private void addTileBtn(TileData tileData, ToggleGroup group) {
    Image image = null;
    if (tileData == null)
      image = IconRegistry.getInstance().remove;
    else {
      var preview = new TilePreviewScene(tileData);
      var canvas = new Canvas(PREVIEW_SIZE, PREVIEW_SIZE);
      var gc = canvas.getGraphicsContext2D();
      preview.render(gc);
      var snapshotParams = new SnapshotParameters();
      snapshotParams.setFill(Color.TRANSPARENT);
      image = canvas.snapshot(snapshotParams, new WritableImage(PREVIEW_SIZE, PREVIEW_SIZE));
    }
    var button = addBtn(group, image, tileGrid);
    button.setOnAction(actionEvent -> {
      var selectedToggle = group.getSelectedToggle();
      logger.fine("Select tile toggle: " + String.valueOf(selectedToggle));
      if (tileData == null)
        mapEditor.selectTile(null);
      else
        mapEditor.selectTile(tileData.getName());
    });
  }

  private void addEntityBtn(EntityData entityData, ToggleGroup group) {
    Image image = null;
    if (entityData == null)
      image = IconRegistry.getInstance().remove;
    else {
      var preview = new EntityPreviewScene(entityData);
      var canvas = new Canvas(PREVIEW_SIZE, PREVIEW_SIZE);
      var gc = canvas.getGraphicsContext2D();
      preview.render(gc);
      var snapshotParams = new SnapshotParameters();
      snapshotParams.setFill(Color.TRANSPARENT);
      image = canvas.snapshot(snapshotParams, new WritableImage(PREVIEW_SIZE, PREVIEW_SIZE));
    }
    var button = addBtn(group, image, entityGrid);
    button.setOnAction(actionEvent -> {
      var selectedToggle = group.getSelectedToggle();
      logger.fine("Select entity toggle: " + String.valueOf(selectedToggle));
      if (entityData == null)
        mapEditor.selectEntity(null);
      else
        mapEditor.selectEntity(entityData.getName());
    });
  }

  private ToggleButton addBtn(ToggleGroup group, Image image, TilePane pane) {
    ToggleButton button = new ToggleButton();
    var imageView = new ImageView(image);
    imageView.setFitWidth(PREVIEW_SIZE);
    imageView.setFitHeight(PREVIEW_SIZE);
    button.setGraphic(imageView);
    pane.getChildren().add(button);
    button.setToggleGroup(group);
    return button;
  }

  @Override
  public DataObject loadDataObject(String objectName) {
    var manager = ProjectData.getCurrent().getSubManager(DataType.MAP, MapData.class);
    if (objectName == null) {
      dataObject = manager.createNew();
      objectName = dataObject.getName();
    } else {
      dataObject = manager.get(objectName);
    }

    ProjectData projectData = new ProjectData();
    projectData.setStartMapName(objectName);
    projectData.getSubManager(DataType.MAP, MapData.class).add(dataObject);

    var currentEntityManager = ProjectData.getCurrent().getSubManager(DataType.ENTITY, EntityData.class);
    var entityManager = (SubDataManager<EntityData>) projectData.getSubManager(DataType.ENTITY, EntityData.class);
    for (var entityData : currentEntityManager.getDataObjects().values())
      entityManager.add(entityData);

    var tilesManager = ProjectData.getCurrent().getSubManager(DataType.TILE, TileData.class);
    projectData.getSubManager(DataType.TILE, TileData.class).setDataObjects(tilesManager.getDataObjects());
    mapEditor = new MapEditorGame(projectData);
    canvasPane = new CanvasPane(mapEditor, true);
    canvasStackPane.getChildren().add(canvasPane);

    createBindings();
    return dataObject;
  }

  private void createBindings() {
    DataFxUtil.bind(dataObject.getNameProperty(), name);
    DataFxUtil.bind(dataObject.getNumColsProperty(), numCols);
    DataFxUtil.bind(dataObject.getNumRowsProperty(), numRows);
    DataFxUtil.bind(dataObject.getGravityProperty(), gravity);
  }

  @Override
  public void onClose() {
    canvasPane.stop();
  }

  @Override
  public boolean needsRefresh() {
    var tilesManager = ProjectData.getCurrent().getSubManager(DataType.TILE);
    int newTilesRevision = tilesManager.getRevision();
    var entitiesManager = ProjectData.getCurrent().getSubManager(DataType.ENTITY);
    int newEntitiesRevision = entitiesManager.getRevision();
    boolean needsRefresh = (tilesRevision != newTilesRevision || entitiesRevision != newEntitiesRevision);
    tilesRevision = newTilesRevision;
    entitiesRevision = newEntitiesRevision;
    return needsRefresh;
  }

  @Override
  public MapData getDataObject() {
    return dataObject;
  }

  @Override
  public DataType getDataType() {
    return DataType.MAP;
  }

}
