package de.hsa.dice.editor.controller.editors;

import de.hsa.dice.core.dataobject.DataObject;

public interface EditorController {
  
  void initialize();

  /**
   * Fills the editor from the project with the given name or from a newly
   * created project if objectName is null.
   * 
   * @param objectName
   *          The unique identifying name of the project to load. Set to null
   *          to create a new one.
   * 
   */
  DataObject loadDataObject(String objectName);

  void onClose();
  
	boolean needsRefresh();

}
