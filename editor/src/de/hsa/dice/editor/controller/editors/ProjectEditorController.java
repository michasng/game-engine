package de.hsa.dice.editor.controller.editors;

import de.hsa.dice.core.dataobject.DataObject;
import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.manager.ProjectDataManager;
import de.hsa.dice.editor.controller.DataFxUtil;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class ProjectEditorController implements DataEditorController<ProjectData> {

  @FXML
  private TextField name;

  @FXML
  private ComboBox<String> startmap;
  @FXML
  private TextField pixelPerMeter;

  @FXML
  private CheckBox maximized;
  @FXML
  private CheckBox fullscreen;

  @FXML
  private ColorPicker theme;

  private ProjectData dataObject;

  public ProjectEditorController() {
  }

  @Override
  public void initialize() {
    assert name != null;
    assert startmap != null && pixelPerMeter != null;
    assert maximized != null && fullscreen != null;
    assert theme != null;

    // TODO: listen for changes
    startmap.getItems().addAll(ProjectData.getCurrent().getSubManager(DataType.MAP).getNames());

    startmap.setOnAction(actionEvent -> {
      dataObject.setStartMapName(startmap.getValue());
    });
  }

  @Override
  public DataObject loadDataObject(String objectName) {
    // ignore objectName and load by projectName
    dataObject = ProjectDataManager.getInstance().getCurrentProject();
    createBindings();
    return dataObject;
  }

  private void createBindings() {
    DataFxUtil.bind(dataObject.getNameProperty(), name);
    DataFxUtil.bind(dataObject.getStartMapNameProperty(), startmap);
    DataFxUtil.bind(dataObject.getPixelPerMeterProperty(), pixelPerMeter);
    DataFxUtil.bind(dataObject.getMaximizedProperty(), maximized);
    DataFxUtil.bind(dataObject.getFullscreenProperty(), fullscreen);
    DataFxUtil.bind(dataObject.getThemeProperty(), theme);
  }

  @Override
  public void onClose() {
  }

  @Override
  public boolean needsRefresh() {
    return false;
  }

  @Override
  public ProjectData getDataObject() {
    return dataObject;
  }

  @Override
  public DataType getDataType() {
    return DataType.PROJECT;
  }

}
