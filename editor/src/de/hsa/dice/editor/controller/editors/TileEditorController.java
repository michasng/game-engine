package de.hsa.dice.editor.controller.editors;

import de.hsa.dice.core.dataobject.DataObject;
import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.EntityData.GraphicsMode;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.TileData;
import de.hsa.dice.core.property.PropertySubscription;
import de.hsa.dice.core.view.CanvasPane;
import de.hsa.dice.editor.controller.DataFxUtil;
import de.hsa.dice.editor.view.TilePreviewScene;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.StackPane;

public class TileEditorController implements DataEditorController<TileData> {

  @FXML
  private TextField name;

  @FXML
  private CheckBox solid;
  @FXML
  private TextField density;
  @FXML
  private TextField friction;
  @FXML
  private TextField elasticity;

  @FXML
  private RadioButton graphicsModeColor;
  @FXML
  private RadioButton graphicsModeSprite;
  @FXML
  private RadioButton graphicsModeInvisible;
  @FXML
  private ToggleGroup graphicsModeGroup;

  @FXML
  private ColorPicker color;
  @FXML
  private ComboBox<String> spriteSelect;

  @FXML
  private TextField padLeft;
  @FXML
  private TextField padRight;
  @FXML
  private TextField padTop;
  @FXML
  private TextField padBottom;

  @FXML
  private StackPane preview;

  private PropertySubscription<?> previewSub;

  private TileData dataObject;

  @Override
  public void initialize() {
    assert name != null;
    assert solid != null;
    assert density != null && friction != null && elasticity != null;
    assert graphicsModeColor != null && graphicsModeSprite != null && graphicsModeInvisible != null
        && graphicsModeGroup != null;
    assert color != null;
    assert spriteSelect != null;
    assert padLeft != null && padRight != null && padTop != null && padBottom != null;
    assert preview != null;

    // TODO: replace with real sprites names
    spriteSelect.getItems().addAll("playerGraphics", "enemyGraphics");
  }

  @Override
  public DataObject loadDataObject(String objectName) {
    var manager = ProjectData.getCurrent().getSubManager(DataType.TILE, TileData.class);
    if (objectName == null) {
      dataObject = manager.createNew();
      objectName = dataObject.getName();
    } else {
      dataObject = manager.get(objectName);
      if (dataObject == null)
        throw new Error("DataObject does not exist.");
    }

    createBindings();
    return dataObject;
  }

  private void createBindings() {
    DataFxUtil.bind(dataObject.getNameProperty(), name);
    DataFxUtil.bind(dataObject.getSolidProperty(), solid);
    DataFxUtil.bind(dataObject.getDensityProperty(), density);
    DataFxUtil.bind(dataObject.getFrictionProperty(), friction);
    DataFxUtil.bind(dataObject.getElasticityProperty(), elasticity);
    DataFxUtil.bind(dataObject.getColorProperty(), color);

    graphicsModeColor.setUserData(GraphicsMode.COLOR);
    graphicsModeSprite.setUserData(GraphicsMode.SPRITE);
    graphicsModeInvisible.setUserData(GraphicsMode.INVISIBLE);
    DataFxUtil.bind(dataObject.getGraphicsModeProperty(), graphicsModeGroup);

    ChangeListener<? super Toggle> onGraphicsModeChanged = (observable, oldToggle, selectedToggle) -> {
      var mode = selectedToggle.getUserData();
      if (mode == GraphicsMode.COLOR) {
        spriteSelect.setDisable(true);
        color.setDisable(false);
      } else if (mode == GraphicsMode.SPRITE) {
        spriteSelect.setDisable(false);
        color.setDisable(true);
      } else if (mode == GraphicsMode.INVISIBLE) {
        spriteSelect.setDisable(true);
        color.setDisable(true);
      }
    };

    graphicsModeGroup.selectedToggleProperty().addListener(onGraphicsModeChanged);
    onGraphicsModeChanged.changed(graphicsModeGroup.selectedToggleProperty(), null,
        graphicsModeGroup.getSelectedToggle());

    DataFxUtil.bind(dataObject.getSpriteProperty(), spriteSelect);
    DataFxUtil.bind(dataObject.getColorProperty(), color);
    DataFxUtil.bindDistances(dataObject.getPaddingsProperty(), padLeft, padRight, padTop, padBottom);

    previewSub = dataObject.getRevisionProperty().addListener((oldValue, value) -> updatePreview());

    updatePreview();
  }

  public void updatePreview() {
    preview.getChildren().clear();
    var scene = new TilePreviewScene(dataObject);
    preview.getChildren().add(new CanvasPane(scene, true, false));
  }

  @Override
  public void onClose() {
    previewSub.cancelSubscription();
  }

  @Override
  public boolean needsRefresh() {
    return false;
  }

  @Override
  public TileData getDataObject() {
    return dataObject;
  }

  @Override
  public DataType getDataType() {
    return DataType.TILE;
  }

}
