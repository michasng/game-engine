package de.hsa.dice.editor.controller.editors;

import java.util.LinkedList;

import de.hsa.dice.core.dataobject.DataObject;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.view.CanvasPane;
import de.hsa.dice.editor.Editor;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.control.Tab;
import javafx.scene.layout.StackPane;

public class PlayController implements EditorController {

  @FXML
  private StackPane stackPane;

  private CanvasPane canvasPane;

  private Game game;

  private LinkedList<Tab> previewTabs;

  public PlayController() {
    game = new Game();
    previewTabs = new LinkedList<>();
  }

  @Override
  public void initialize() {
    canvasPane = new CanvasPane(game, true);
    stackPane.getChildren().add(canvasPane);

    for (var entity : game.getMap().getEntities()) {
      for (var scriptCompData : entity.getEntityData().getScripts()) {
        if (!scriptCompData.getPreview())
          continue;

        int scriptIndex = entity.getEntityData().getScripts().indexOf(scriptCompData);
        var script = entity.getScripts().get(scriptIndex);

        var tabFactory = Editor.getInstance().getApp().getPreviewScriptTabFactory();
        var container = tabFactory.addTab(scriptCompData.getName());
        var scriptPreviewController = (ScriptPreviewController) container.fxNodeContainer.controller;
        scriptPreviewController.setScript(script);
        Platform.runLater(() -> container.tab.moveTo(new Point2D(0, 0)));
        container.tab.setLabelText(script.getDataObject().getName() + "-preview");
        previewTabs.add(container.tab);
      }
    }
  }

  @Override
  public DataObject loadDataObject(String objectName) {
    return ProjectData.getCurrent();
  }

  @Override
  public void onClose() {
    canvasPane.stop();
    for (var tab : previewTabs) {
      if (tab.getTabPane() == null)
        continue;
      tab.getTabPane().getTabs().remove(tab);
      tab.getOnClosed().handle(null);
    }
  }

  @Override
  public boolean needsRefresh() {
    return false;
  }

}
