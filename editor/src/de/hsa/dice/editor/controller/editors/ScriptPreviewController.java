package de.hsa.dice.editor.controller.editors;

import de.hsa.dice.core.dataobject.DataObject;
import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.ScriptData;
import de.hsa.dice.core.script.Script;
import de.hsa.dice.core.view.CanvasPane;
import de.hsa.dice.editor.view.scripts.ScriptEditorGame;
import javafx.fxml.FXML;
import javafx.scene.layout.StackPane;

public class ScriptPreviewController implements EditorController {

  @FXML
  private StackPane stackPane;

  private CanvasPane canvasPane;

  private ScriptEditorGame scriptEditor;

  public ScriptPreviewController() {
  }

  @Override
  public void initialize() {
  }

  @Override
  public DataObject loadDataObject(String objectName) {
    var scriptManager = ProjectData.getCurrent().getSubManager(DataType.SCRIPT, ScriptData.class);
    var scriptData = scriptManager.get(objectName);

    scriptEditor = new ScriptEditorGame(scriptData);
    canvasPane = new CanvasPane(scriptEditor, true);
    stackPane.getChildren().add(canvasPane);
    return ProjectData.getCurrent();
  }

  public void setScript(Script script) {
    scriptEditor.setScript(script);
  }

  @Override
  public void onClose() {
    canvasPane.stop();
  }

  @Override
  public boolean needsRefresh() {
    return false;
  }

}
