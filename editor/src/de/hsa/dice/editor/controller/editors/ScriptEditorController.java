package de.hsa.dice.editor.controller.editors;

import java.util.HashMap;

import de.hsa.dice.core.dataobject.DataObject;
import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.ScriptData;
import de.hsa.dice.core.dataobject.nested.NodeData;
import de.hsa.dice.core.script.NodeFactory;
import de.hsa.dice.core.script.NodeRegistry;
import de.hsa.dice.core.view.CanvasPane;
import de.hsa.dice.editor.controller.DataFxUtil;
import de.hsa.dice.editor.controller.editors.nested.NodeInspectorController;
import de.hsa.dice.editor.view.scripts.ScriptEditorGame;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;

public class ScriptEditorController implements DataEditorController<ScriptData> {

  @FXML
  private StackPane stackPane;

  @FXML
  private NodeInspectorController nodeInspectorController;

  @FXML
  private TextField name;

  private CanvasPane canvasPane;

  private ScriptEditorGame scriptEditorGame;

  private ScriptData dataObject;

  private ContextMenu contextMenu;
  private TextField searchTextField;

  public ScriptEditorController() {
  }

  @Override
  public void initialize() {
    assert stackPane != null;
    assert nodeInspectorController != null;
    assert name != null;

    nodeInspectorController.setUpdateMaxId(() -> scriptEditorGame.updateMaxId());

    contextMenu = new ContextMenu();
    stackPane.setOnMouseClicked(mouseEvent -> {
      contextMenu.hide();
      var menuItems = getContextMenuItems();
      if (menuItems == null)
        return;
      contextMenu.getItems().clear();
      contextMenu.getItems().addAll(menuItems);
      if (mouseEvent.getButton() == MouseButton.SECONDARY && mouseEvent.isStillSincePress()) {
        contextMenu.show(stackPane, mouseEvent.getScreenX(), mouseEvent.getScreenY());
        searchTextField.requestFocus();
      }
    });
  }

  public MenuItem[] getContextMenuItems() {
    if (scriptEditorGame.getHovered() == null) {
      return getAddNodeMenuItems();
    } else
      return scriptEditorGame.getHovered().getContextMenuItems();
  }

  private class NodeTreeItem extends TreeItem<String> {
    public NodeFactory factory;

    public NodeTreeItem(NodeFactory factory) {
      super(factory.getDisplayName());
      this.factory = factory;
    }

    public void onDoubleClicked(MouseEvent event) {
      scriptEditorGame.createNode(factory.getTypeName());
      contextMenu.hide();
    }
  }

  public static class TreeCellImpl extends TreeCell<String> {

    @Override
    public void updateItem(String item, boolean empty) {
      super.updateItem(item, empty);

      Tooltip tooltip = null;
      EventHandler<? super MouseEvent> onMouseClicked = null;

      if (empty) {
        setText(null);
        setGraphic(null);
      } else {
        setText(item);
        setGraphic(getTreeItem().getGraphic());

        if (getTreeItem() != null && getTreeItem() instanceof NodeTreeItem) {
          var nodeTreeItem = (NodeTreeItem) getTreeItem();
          tooltip = new Tooltip(nodeTreeItem.factory.getDescription());
          onMouseClicked = mouseEvent -> {
            if (mouseEvent.getClickCount() == 2)
              nodeTreeItem.onDoubleClicked(mouseEvent);
          };
        }
      }

      setTooltip(tooltip);
      setOnMouseClicked(onMouseClicked);

    }
  }

  public MenuItem[] getAddNodeMenuItems() {
    var factories = NodeRegistry.getInstance().getSortedFactories();

    var searchMenuItem = new MenuItem();
    searchTextField = new TextField();
    searchTextField.setPrefWidth(256);
    searchMenuItem.setGraphic(searchTextField);

    var nodesMenuItem = new MenuItem();
    var treeView = new TreeView<String>();
    treeView.setCellFactory(treeViewImpl -> new TreeCellImpl());
    var categoryItems = new HashMap<String, TreeItem<String>>();
    var treeRoot = new TreeItem<String>("Nodes");
    treeView.setRoot(treeRoot);
    treeView.setShowRoot(false);
    treeRoot.setExpanded(true);
    for (var factory : factories) {
      if (!categoryItems.containsKey(factory.getCategory())) {
        var categoryItem = new TreeItem<String>(factory.getCategory());
        categoryItems.put(factory.getCategory(), categoryItem);
      }
    }
    Runnable updateItems = () -> {
      var searchText = searchTextField.getText();
      treeRoot.getChildren().clear();
      for (var categoryItem : categoryItems.values()) {
        categoryItem.setExpanded(!searchText.isEmpty());
        categoryItem.getChildren().clear();
      }
      for (var factory : factories) {
        if (!factory.getDisplayName().toLowerCase().contains(searchText.toLowerCase()))
          continue;
        var categoryItem = categoryItems.get(factory.getCategory());
        var treeItem = new NodeTreeItem(factory);
        categoryItem.getChildren().add(treeItem);
      }
      for (var categoryItem : categoryItems.values()) {
        if (!categoryItem.getChildren().isEmpty())
          treeRoot.getChildren().add(categoryItem);
      }
    };
    updateItems.run();
    searchTextField.setOnKeyTyped(keyEvent -> updateItems.run());
    treeView.setPrefHeight(256);
    nodesMenuItem.setGraphic(treeView);

    return new MenuItem[] { searchMenuItem, nodesMenuItem };
  }

  @Override
  public DataObject loadDataObject(String objectName) {
    var manager = ProjectData.getCurrent().getSubManager(DataType.SCRIPT, ScriptData.class);
    if (objectName == null) {
      dataObject = manager.createNew();
      objectName = dataObject.getName();
    } else {
      dataObject = manager.get(objectName);
    }

    scriptEditorGame = new ScriptEditorGame(dataObject);
    canvasPane = new CanvasPane(scriptEditorGame, true);
    stackPane.getChildren().add(canvasPane);

    createBindings();
    return dataObject;
  }

  private void createBindings() {
    DataFxUtil.bind(dataObject.getNameProperty(), name);

    scriptEditorGame.getSelectedProperty().addListener((oldValue, value) -> {
      if (value.isEmpty())
        updateInspector(null);
      else if (value.size() == 1)
        updateInspector(value.get(0).getDataObject());
    });
  }

  public void updateInspector(NodeData nodeData) {
    nodeInspectorController.update(nodeData);
  }

  @Override
  public void onClose() {
    canvasPane.stop();
    nodeInspectorController.onClose();
  }

  @Override
  public boolean needsRefresh() {
    return false;
  }

  @Override
  public ScriptData getDataObject() {
    return dataObject;
  }

  @Override
  public DataType getDataType() {
    return DataType.SCRIPT;
  }

}
