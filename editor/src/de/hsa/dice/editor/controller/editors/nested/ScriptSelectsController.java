package de.hsa.dice.editor.controller.editors.nested;

import java.util.LinkedList;

import de.hsa.dice.core.dataobject.DataObject;
import de.hsa.dice.core.dataobject.nested.ScriptCompData;
import de.hsa.dice.core.property.PropNotifyList;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.view.FXMLUtil;
import de.hsa.dice.core.view.FXMLUtil.FxNodeContainer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.VBox;

public class ScriptSelectsController {

  @FXML
  private VBox selectsBox;

  private LinkedList<ScriptSelectController> scriptCtrls;

  private DataObject parent;

  private Property<PropNotifyList<ScriptCompData>> prop;

  @FXML
  public void initialize() {
    assert selectsBox != null : "fx:id=\"keyValueFields\" was not injected: check your FXML file 'scriptSelects.fxml'.";
    scriptCtrls = new LinkedList<>();
  }

  public void setDataObject(DataObject parent, Property<PropNotifyList<ScriptCompData>> prop) {
    this.parent = parent;
    this.prop = prop;
    for (var scriptComp : prop.getValue()) {
      var selectCtrl = addEntityScript();
      selectCtrl.setDataObject(scriptComp);
    }
  }

  @FXML
  public void onAddAction(ActionEvent event) {
    var selectCtrl = addEntityScript();
    var scriptComp = new ScriptCompData(parent);
    prop.getValue().add(scriptComp);
    selectCtrl.setDataObject(scriptComp);
  }

  public ScriptSelectController addEntityScript() {
    FxNodeContainer<Node, ScriptSelectController> container = FXMLUtil.load("scriptSelect.fxml");
    selectsBox.getChildren().add(container.node);
    scriptCtrls.add(container.controller);
    container.controller.getRemove().setOnAction(event -> {
      selectsBox.getChildren().remove(container.node);
      scriptCtrls.remove(container.controller);
      var scriptCompData = container.controller.getDataObject();
      prop.getValue().remove(scriptCompData);
    });
    return container.controller;
  }

}
