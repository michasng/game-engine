package de.hsa.dice.editor.controller.editors;

import de.hsa.dice.core.dataobject.DataObject;
import de.hsa.dice.core.dataobject.DataType;

public interface DataEditorController<T extends DataObject> extends EditorController {

  T getDataObject();

  DataType getDataType();

}
