package de.hsa.dice.editor.controller.editors;

import java.io.File;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.dataobject.DataObject;
import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.SpriteData;
import de.hsa.dice.core.dataobject.nested.RectangleData;
import de.hsa.dice.core.dataobject.nested.SpriteFrameData;
import de.hsa.dice.core.game.ImageLoader;
import de.hsa.dice.core.game.Sprite;
import de.hsa.dice.core.property.PropertySubscription;
import de.hsa.dice.core.util.io.ProjectFileOperator;
import de.hsa.dice.core.view.CanvasPane;
import de.hsa.dice.editor.Editor;
import de.hsa.dice.editor.controller.DataFxUtil;
import de.hsa.dice.editor.controller.editors.nested.ListViewController;
import de.hsa.dice.editor.view.SpritePreviewScene;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class SpriteEditorController implements DataEditorController<SpriteData> {

  @FXML
  private TextField name;

  @FXML
  private CheckBox animated;

  @FXML
  private ListViewController<SpriteFrameData> listViewController;

  @FXML
  private TextField x, y, width, height;

  @FXML
  private StackPane preview;
  private PropertySubscription<?> previewSub;

  private SpriteData dataObject;

  private String workingDirectory;

  public SpriteEditorController() {
  }

  @Override
  public void initialize() {
    assert name != null;
    assert animated != null;
    assert listViewController != null;
    assert x != null && y != null && width != null && height != null;
    assert preview != null;
  }

  @Override
  public DataObject loadDataObject(String objectName) {
    var manager = ProjectData.getCurrent().getSubManager(DataType.SPRITE, SpriteData.class);
    if (objectName == null) {
      dataObject = manager.createNew();
      objectName = dataObject.getName();
    } else {
      dataObject = manager.get(objectName);
    }

    createBindings();
    return dataObject;
  }

  private void createBindings() {
    DataFxUtil.bind(dataObject.getNameProperty(), name);
    DataFxUtil.bind(dataObject.getAnimatedProperty(), animated);

    var prjFileOp = ProjectFileOperator.getInstance();
    workingDirectory = prjFileOp.getProjectPath();

    listViewController.setAddButtonText("Add Full Image");
    listViewController.setGraphicsBuilder(cell -> {
      var preview = new ImageView();
      preview.setFitWidth(64);
      preview.setFitHeight(24);
      preview.setPreserveRatio(true);
      var frame = dataObject.getFrames().get(cell.getIndex());
      var relPath = frame.getPath();
      if (!relPath.isEmpty()) {
        var absPath = prjFileOp.getAbsolutePath(relPath);
        var image = ImageLoader.getInstance().load(absPath);
        if (frame.getSubRegion() != null)
          image = Sprite.getSubimage(image, frame.getSubRegion());
        preview.setImage(image);
      }

      var springPane = new Pane();
      HBox.setHgrow(springPane, Priority.ALWAYS);

      var durationLabel = new Label("Duration: ");
      var duration = new TextField();
      duration.setDisable(!animated.isSelected());
      duration.setPrefWidth(60);
      durationLabel.setContentDisplay(ContentDisplay.RIGHT);
      durationLabel.setGraphic(duration);
      DataFxUtil.bind(frame.getDurationProperty(), duration);
      var indexLabel = new Label("Index: " + cell.getIndex());

      return new Node[] { preview, springPane, durationLabel, indexLabel };
    });
    animated.selectedProperty().addListener((obs, value, oldValue) -> listViewController.refreshList());

    listViewController.setFactory(() -> {
      var spriteFrameData = new SpriteFrameData(dataObject);

      var fileChooser = new FileChooser();
      fileChooser.setInitialDirectory(new File(workingDirectory));
      fileChooser.setTitle("Open Resource File");
      fileChooser.getExtensionFilters().add(new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif", ".bmp"));
      var selectedFile = fileChooser.showOpenDialog(Editor.getInstance().getPrimaryStage());
      if (selectedFile == null)
        return null;
      workingDirectory = selectedFile.getParent();

      var newAbsPath = selectedFile.getAbsolutePath();
      var newRelPath = prjFileOp.getRelativePath(newAbsPath);
      spriteFrameData.setPath(newRelPath);

      return spriteFrameData;
    });
    listViewController.setDataObject(dataObject.getFrames());

    updatePreview();
    previewSub = dataObject.getRevisionProperty().addListener((oldValue, newValue) -> updatePreview());
  }

  @FXML
  public void onAddSubimage(ActionEvent event) {
    TextField[] textFields = { x, y, width, height };
    int[] numbers = new int[textFields.length];
    for (int i = 0; i < textFields.length; i++) {
      try {
        numbers[i] = Integer.valueOf(textFields[i].getText());
      } catch (NumberFormatException e) {
        textFields[i].setStyle("-fx-text-fill: red");
        Console.logError(textFields[i].getText() + " is not a normal number.");
        return;
      }
    }
    var data = listViewController.getFactory().makeInstance();
    if (data == null)
      return;
    data.setSubRegion(new RectangleData(numbers[0], numbers[1], numbers[2], numbers[3]));
    listViewController.addItem(data);
  }

  public void updatePreview() {
    ImageLoader.getInstance().clearCache();
    preview.getChildren().clear();
    var scene = new SpritePreviewScene(dataObject);
    preview.getChildren().add(new CanvasPane(scene, true, false));
  }

  @Override
  public void onClose() {
    previewSub.cancelSubscription();
  }

  @Override
  public boolean needsRefresh() {
    return false;
  }

  @Override
  public SpriteData getDataObject() {
    return dataObject;
  }

  @Override
  public DataType getDataType() {
    return DataType.SPRITE;
  }

}