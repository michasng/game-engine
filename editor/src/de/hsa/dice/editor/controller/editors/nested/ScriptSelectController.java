package de.hsa.dice.editor.controller.editors.nested;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.ScriptData;
import de.hsa.dice.core.dataobject.nested.ScriptCompData;
import de.hsa.dice.editor.controller.DataFxUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;

public class ScriptSelectController {

  @FXML
  private ComboBox<String> scriptComboBox;

  @FXML
  private CheckBox showPreviewCheckBox;

  @FXML
  private Button remove;

  private ScriptCompData dataObject;

  @FXML
  void initialize() {
    assert scriptComboBox != null : "fx:id=\"scriptComboBox\" was not injected: check your FXML file 'scriptSelects.fxml'.";
  }

  public void setDataObject(ScriptCompData dataObject) {
    this.dataObject = dataObject;

    createBindings();
  }

  private void createBindings() {
    var scriptsManager = ProjectData.getCurrent().getSubManager(DataType.SCRIPT, ScriptData.class);
    scriptComboBox.getItems().addAll(scriptsManager.getNames());
    DataFxUtil.bind(dataObject.getNameProperty(), scriptComboBox);
    DataFxUtil.bind(dataObject.getPreviewProperty(), showPreviewCheckBox);
  }

  public ScriptCompData getDataObject() {
    return dataObject;
  }

  public Button getRemove() {
    return remove;
  }

}
