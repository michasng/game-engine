package de.hsa.dice.editor.controller.editors.nested;

import java.util.logging.Logger;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.dataobject.ScriptData;
import de.hsa.dice.core.dataobject.nested.FieldData;
import de.hsa.dice.core.dataobject.nested.NodeData;
import de.hsa.dice.core.dataobject.nested.VectorData;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.property.PropertySubscription;
import de.hsa.dice.core.view.IconRegistry;
import de.hsa.dice.editor.controller.DataFxUtil;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

public class NodeInspectorController {

  private static final Logger logger = Logger.getLogger(NodeInspectorController.class.getName());

  @FXML
  private VBox inspectorPane;

  private PropertySubscription<Integer> idSub;

  private Runnable updateMaxId;

  @FXML
  public void initialize() {
    assert inspectorPane != null;
  }

  public void setUpdateMaxId(Runnable updateMaxId) {
    this.updateMaxId = updateMaxId;
  }

  @SuppressWarnings("unchecked")
  public void update(NodeData nodeData) {
    if (idSub != null) {
      idSub.cancelSubscription();
      idSub = null;
    }

    inspectorPane.getChildren().clear();

    if (nodeData == null)
      return;

    if (nodeData.getFactory() == null) {
      var nameLabel = new Label("Invalid Node");
      // TODO: maybe still show the existing (potentially invalid) fields
      var hBox = makeHBox(nameLabel);
      inspectorPane.getChildren().add(hBox);
      return;
    }

    var nameLabel = new Label(nodeData.getFactory().getDisplayName());
    nameLabel.setFont(new Font(18));
    var idLabel = new Label("id:");
    var idTextField = new TextField();
    idTextField.setPrefWidth(60);
    bindIdTextField(nodeData, idTextField);
    var hBox = makeHBox(nameLabel, makeSpring(), idLabel, idTextField);
    inspectorPane.getChildren().add(hBox);
    var descLabel = new Label(nodeData.getFactory().getDescription());
    var descFont = descLabel.getFont();
    descFont = Font.font(descFont.getFamily(), FontWeight.NORMAL, FontPosture.ITALIC, descFont.getSize());
    descLabel.setFont(descFont);
    descLabel.setWrapText(true);
    hBox = makeHBox();
    hBox.getChildren().add(descLabel);
    inspectorPane.getChildren().add(hBox);

    if (nodeData.getFields().length == 0)
      return;

    var name = new Label("name:");
    var nameFont = name.getFont();
    nameFont = Font.font(nameFont.getFamily(), FontWeight.BOLD, FontPosture.REGULAR, nameFont.getSize());
    name.setFont(nameFont);
    var value = new Label("value/variable:");
    value.setPrefWidth(150); // standard TextField size
    var isVariable = new Label("is var:");
    isVariable.setPrefWidth(30); // otherwise cut off, toggle button size is set in stylesheet
    hBox = makeHBox(name, makeSpring(), value, isVariable);
    inspectorPane.getChildren().addAll(new Separator(), hBox);

    var iconReg = IconRegistry.getInstance();
    for (FieldData fieldData : nodeData.getFields()) {
      descLabel = new Label(fieldData.getInspectAnnot().description());
      descLabel.setFont(descFont);
      descLabel.setWrapText(true);
      hBox = makeHBox(descLabel);
      inspectorPane.getChildren().add(hBox);

      name = new Label(fieldData.getFieldName());
      name.setFont(nameFont);
      var varToggle = new ToggleButton();
      var toggleGraphic = new ImageView();
      varToggle.setGraphic(toggleGraphic);
      Control[] ctrls;
      Class<?> typeClass = fieldData.getTypeClass();
      if (typeClass == boolean.class) {
        ctrls = new Control[] { new CheckBox() };
        DataFxUtil.bind((Property<Boolean>) fieldData.getValueProperty(), (CheckBox) ctrls[0]);
      } else if (typeClass == int.class || typeClass == float.class || typeClass == String.class) {
        ctrls = new Control[] { new TextField() };
        DataFxUtil.bind(fieldData.getValueProperty(), (TextField) ctrls[0]);
      } else if (typeClass == Vec2.class) {
        ctrls = new Control[] { new TextField(), new TextField() };
        ctrls[0].setPrefWidth(50);
        ctrls[1].setPrefWidth(50);
        DataFxUtil.bind((Property<VectorData>) fieldData.getValueProperty(), (TextField) ctrls[0],
            (TextField) ctrls[1]);
      } else {
        if (!fieldData.getTypeClass().isEnum())
          throw new Error("Unsupported field type: " + fieldData.getTypeName());
        ctrls = new Control[1];
        ctrls[0] = DataFxUtil.bindAutoCompleteEnumComboBox(fieldData.getValueProperty());
      }
      var varField = new TextField();
      DataFxUtil.bind(fieldData.getVariableProperty(), varField);
      bindVariableColoring(varField);
      for (var ctrl : ctrls) {
        ctrl.setTooltip(new Tooltip(fieldData.getTypeName()));
        if (fieldData.getInspectAnnot().colored() && ctrl instanceof TextField)
          bindVariableColoring((TextField) ctrl);
      }
      var controlsRow = new HBox(); // has controls dynamically added/removed
      hBox = makeHBox(name, makeSpring(), controlsRow, varToggle);

      DataFxUtil.bind(fieldData.getIsVariableProperty(), varToggle);
      var boundToggleAction = varToggle.getOnAction();
      varToggle.setOnAction(actionEvent -> {
        boundToggleAction.handle(actionEvent);
        if (varToggle.isSelected()) {
          for (var ctrl : ctrls)
            controlsRow.getChildren().remove(ctrl);
          controlsRow.getChildren().add(varField);
          toggleGraphic.setImage(iconReg.toggleOn);
        } else {
          for (var ctrl : ctrls) // add before the toggle
            controlsRow.getChildren().add(ctrl);
          controlsRow.getChildren().remove(varField);
          toggleGraphic.setImage(iconReg.toggleOff);
        }
      });
      varToggle.getOnAction().handle(null);
      inspectorPane.getChildren().add(hBox);

    }
  }

  private HBox makeHBox(Node... children) {
    var hBox = new HBox();
    hBox.setAlignment(Pos.CENTER_LEFT);
    hBox.setPrefWidth(HBox.USE_COMPUTED_SIZE);
    hBox.setPrefHeight(HBox.USE_COMPUTED_SIZE);
    hBox.setSpacing(10);
    hBox.getChildren().addAll(children);
    return hBox;
  }

  private Pane makeSpring() {
    var pane = new Pane();
    pane.setPrefWidth(HBox.USE_COMPUTED_SIZE);
    pane.setPrefHeight(HBox.USE_COMPUTED_SIZE);
    HBox.setHgrow(pane, Priority.ALWAYS);
    return pane;
  }

  private void bindIdTextField(NodeData nodeData, TextField idTextField) {
    DataFxUtil.bind(nodeData.getIdProperty(), idTextField);
    idSub = nodeData.getIdProperty().addListener((oldId, newId) -> {
      if (oldId == newId) // TODO: delete this after implementing this check in the Property class
        return;

      if (!(nodeData.getParent() instanceof ScriptData)) {
        logger.severe("NodeData parent is not of type ScriptData");
        return;
      }
      var scriptData = (ScriptData) nodeData.getParent();

      for (var node : scriptData.getNodes()) {
        if (node.getId() == newId && node != nodeData) {
          node.setId(oldId); // change id of previous owner
        }

        // adjust all affected outputs
        for (var output : node.getOutputs()) {
          if (output.getTargetNodeId() == oldId)
            output.setTargetNodeId(newId);
          else if (output.getTargetNodeId() == newId)
            output.setTargetNodeId(oldId);
        }
        updateMaxId.run();
      }

    });
  }

  private void bindVariableColoring(TextField textField) {
    Runnable colorField = () -> {
      var hash = (textField).getText().hashCode();
      int r = (hash & 0xFF0000) >> 16;
      int g = (hash & 0x00FF00) >> 8;
      int b = hash & 0x0000FF;
      var color = Color.rgb(r, g, b);
      color = color.brighter().brighter(); // increase visibility
      textField.setStyle("-fx-text-fill: " + DataFxUtil.toRGBCode(color));
    };
    colorField.run();
    textField.setOnKeyTyped(keyEvent -> colorField.run());
    // override the style change from the original bindings
    textField.focusedProperty().addListener((obs, oldValue, value) -> {
      if (oldValue && !value)
        colorField.run();
    });
    var oldTextFieldAction = textField.getOnAction();
    textField.setOnAction(actionEvent -> {
      oldTextFieldAction.handle(actionEvent);
      colorField.run();
    });
  }

  public void onClose() {
    if (idSub != null)
      idSub.cancelSubscription();
  }

}
