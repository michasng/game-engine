package de.hsa.dice.editor.controller.editors.nested;

import de.hsa.dice.core.property.PropNotifyList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;

public class ListViewController<T> {

  @FXML
  private ListView<String> listView;

  @FXML
  private Button addButton;

  private GraphicsBuilder graphicsBuilder;

  private PropNotifyList<T> dataObject;

  private DataObjectFactory<T> factory;

  public void initialize() {
    listView.setCellFactory(listView -> {
      return new CustomListCell();
    });
  }

  @FXML
  public void onAddAction(ActionEvent event) {
    T data = factory.makeInstance();
    if (data == null)
      return;
    addItem(data);
  }

  public void addItem(T data) {
    dataObject.add(data);
    listView.getItems().add(data.toString());
  }

  public void setAddButtonText(String text) {
    addButton.setText(text);
  }

  public void setGraphicsBuilder(GraphicsBuilder graphicsBuilder) {
    this.graphicsBuilder = graphicsBuilder;
  }

  public void setDataObject(PropNotifyList<T> dataObject) {
    this.dataObject = dataObject;

    for (var data : dataObject)
      listView.getItems().add(data.toString());
  }

  public DataObjectFactory<T> getFactory() {
    return factory;
  }

  public void setFactory(DataObjectFactory<T> factory) {
    this.factory = factory;
  }

  public void refreshList() {
    var items = listView.getItems();
    ObservableList<String> newItems = FXCollections.observableArrayList();
    newItems.addAll(items);
    items.clear();
    listView.setItems(newItems);
  }

  public static interface DataObjectFactory<T> {
    T makeInstance();
  }

  public static interface ListChangeHandler {
    void handle(int index);
  }

  public static interface GraphicsBuilder {
    Node[] buildItems(ListCell<String> cell);
  }

  private class CustomListCell extends ListCell<String> {

    @Override
    public void updateItem(String item, boolean empty) {
      super.updateItem(item, empty);

      if (empty)
        setGraphic(null);
      else
        setGraphic(buildGraphic(item));
    }

    private Node buildGraphic(String item) {
      var removeBtn = new Button("X"); // TODO: create X Icon
      removeBtn.setOnAction(actionEvent -> {
        dataObject.remove(getIndex());
        listView.getItems().remove(getIndex());
      });

      var hBox = new HBox();
      hBox.setAlignment(Pos.CENTER_LEFT);
      hBox.setSpacing(4);

      var children = hBox.getChildren();
      if (graphicsBuilder == null)
        children.add(new Label(item));
      else
        children.addAll(graphicsBuilder.buildItems(this));
      children.add(removeBtn);

      return hBox;
    }
  }

}
