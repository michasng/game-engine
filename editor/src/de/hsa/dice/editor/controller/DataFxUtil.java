package de.hsa.dice.editor.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.dataobject.nested.RectangleData;
import de.hsa.dice.core.dataobject.nested.VectorData;
import de.hsa.dice.core.property.InvalidPropertyException;
import de.hsa.dice.core.property.Property;
import fxcombobox.FxComboBoxUtil;
import fxcombobox.FxComboBoxUtil.HideableItem;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.paint.Color;

public class DataFxUtil {

  public static String toRGBCode(Color color) {
    return String.format("#%02X%02X%02X", (int) (color.getRed() * 255), (int) (color.getGreen() * 255),
        (int) (color.getBlue() * 255));
  }

  @SuppressWarnings("unchecked")
  public static <T> T parseValue(Class<?> c, String valueStr) {
    if (c.equals(String.class)) {
      return (T) valueStr;
    }
    if (c.equals(Integer.class)) {
      try {
        return (T) Integer.valueOf(valueStr);
      } catch (NumberFormatException e) {
        throw new InvalidPropertyException(valueStr + " is not a valid int number.");
      }
    }
    if (c.equals(Float.class)) {
      try {
        return (T) Float.valueOf(valueStr);
      } catch (NumberFormatException e) {
        throw new InvalidPropertyException(valueStr + " is not a valid float number.");
      }
    }
    throw new Error("Cannot parse String value of class " + c);
  }

  public static <T> void bind(Property<T> property, TextField textField) {
    Runnable updateDataFromFx = () -> {
      try {
        String valueStr = textField.getText();
        T value = parseValue(property.getValue().getClass(), valueStr);
        property.setValue(value);
        textField.setStyle("");
      } catch (InvalidPropertyException e) {
        Console.logError(e);
        textField.setStyle("-fx-text-fill: red");
        textField.setText(property.getValue().toString());
      }
    };

    textField.setText(property.getValue().toString());

    textField.focusedProperty().addListener((obs, oldValue, value) -> {
      if (oldValue && !value)
        updateDataFromFx.run();
    });
    textField.setOnAction(actionEvent -> updateDataFromFx.run());
  }

  public static void bind(Property<VectorData> property, TextField xTextField, TextField yTextField) {
    Runnable updateDataFromFx = () -> {
      try {
        String xValueStr = xTextField.getText();
        String yValueStr = yTextField.getText();
        float xValue = parseValue(Float.class, xValueStr);
        float yValue = parseValue(Float.class, yValueStr);
        property.setValue(new VectorData(xValue, yValue));
        xTextField.setStyle("");
        yTextField.setStyle("");
      } catch (InvalidPropertyException e) {
        Console.logError(e);
        xTextField.setStyle("-fx-text-fill: red");
        xTextField.setText(String.valueOf(property.getValue().x));
        yTextField.setStyle("-fx-text-fill: red");
        yTextField.setText(String.valueOf(property.getValue().y));
      }
    };

    xTextField.setText(String.valueOf(property.getValue().x));
    yTextField.setText(String.valueOf(property.getValue().y));

    xTextField.focusedProperty().addListener((obs, oldValue, value) -> {
      if (oldValue && !value)
        updateDataFromFx.run();
    });
    yTextField.focusedProperty().addListener((obs, oldValue, value) -> {
      if (oldValue && !value)
        updateDataFromFx.run();
    });
    xTextField.setOnAction(actionEvent -> updateDataFromFx.run());
    yTextField.setOnAction(actionEvent -> updateDataFromFx.run());
  }

  public static void bind(Property<String> property, ColorPicker colorPicker) {
    Runnable updateDataFromFx = () -> {
      try {
        var value = toRGBCode(colorPicker.getValue());
//        var value = "#" + Integer.toHexString(colorPicker.getValue().hashCode());
        property.setValue(value);
        colorPicker.setStyle("");
      } catch (InvalidPropertyException e) {
        Console.logError(e);
        colorPicker.setStyle("-fx-text-fill: red");
        colorPicker.setValue(Color.web(property.getValue()));
      }
    };

    colorPicker.setValue(Color.web(property.getValue()));

    colorPicker.focusedProperty().addListener((obs, oldValue, value) -> {
      if (oldValue && !value)
        updateDataFromFx.run();
    });
    colorPicker.setOnAction(actionEvent -> updateDataFromFx.run());
  }

  public static <T> void bind(Property<T> property, ComboBox<String> comboBox) {
    Runnable updateDataFromFx = () -> {
      try {
        String valueStr = comboBox.getValue();
        T value = parseValue(property.getValue().getClass(), valueStr);
        property.setValue(value);
        comboBox.setStyle("");
      } catch (InvalidPropertyException e) {
        Console.logError(e);
        comboBox.setStyle("-fx-text-fill: red");
        comboBox.setValue(property.getValue().toString());
      }
    };

    comboBox.setValue(property.getValue().toString());

    comboBox.focusedProperty().addListener((obs, oldValue, value) -> {
      if (oldValue && !value)
        updateDataFromFx.run();
    });
    comboBox.setOnAction(actionEvent -> updateDataFromFx.run());
  }

  public static <T> void bindEnum(Property<T> property, ComboBox<Object> comboBox) {
    Class<?> enumClass = property.getValue().getClass();
    try {
      var valuesMethod = enumClass.getDeclaredMethod("values");
      valuesMethod.setAccessible(true);
      // have the method call itself bc we are in a static context
      Object[] values = (Object[]) valuesMethod.invoke(valuesMethod);
      for (var value : values) {
        comboBox.getItems().add(value);
      }
    } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      e.printStackTrace();
    }
    comboBox.setValue(property.getValue());

    @SuppressWarnings("unchecked")
    Runnable updateDataFromFx = () -> {
      property.setValue((T) comboBox.getValue());
    };

    comboBox.focusedProperty().addListener((obs, oldValue, value) -> {
      if (oldValue && !value)
        updateDataFromFx.run();
    });
    comboBox.setOnAction(actionEvent -> updateDataFromFx.run());
  }

  private static <T> ComboBox<HideableItem<Object>> createEnumComboBox(Property<T> property) {
    ComboBox<HideableItem<Object>> comboBox = null;

    Class<?> enumClass = property.getValue().getClass();
    try {
      var valuesMethod = enumClass.getDeclaredMethod("values");
      valuesMethod.setAccessible(true);
      // have the method call itself bc we are in a static context
      Object[] values = (Object[]) valuesMethod.invoke(valuesMethod);
      var valuesList = Arrays.asList(values);
      comboBox = FxComboBoxUtil.createComboBoxWithAutoCompletionSupport(valuesList);
    } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      e.printStackTrace();
    }
    comboBox.setMaxWidth(150); // seems to be the default value
    return comboBox;
  }

  public static <T> ComboBox<HideableItem<Object>> bindAutoCompleteEnumComboBox(Property<T> property) {
    ComboBox<HideableItem<Object>> comboBox = createEnumComboBox(property);
    FxComboBoxUtil.setValue(comboBox, property.getValue());

    @SuppressWarnings("unchecked")
    Runnable updateDataFromFx = () -> {
      if (comboBox.getValue() != null)
        property.setValue((T) comboBox.getValue().getObject());
    };

    comboBox.focusedProperty().addListener((obs, oldValue, value) -> {
      if (oldValue && !value)
        updateDataFromFx.run();
    });
    comboBox.setOnAction(actionEvent -> updateDataFromFx.run());

    return comboBox;
  }

  public static void bind(Property<Boolean> property, CheckBox checkBox) {
    checkBox.setSelected(property.getValue());

    Runnable updateDataFromFx = () -> {
      try {
        property.setValue(checkBox.isSelected());
        checkBox.setStyle("");
      } catch (InvalidPropertyException e) {
        Console.logError(e);
        checkBox.setStyle("-fx-text-fill: red");
        checkBox.setSelected(property.getValue());
      }
    };

    checkBox.focusedProperty().addListener((obs, oldValue, value) -> {
      if (oldValue && !value)
        updateDataFromFx.run();
    });
    checkBox.setOnAction(actionEvent -> updateDataFromFx.run());
  }

  public static void bind(Property<Boolean> property, ToggleButton toggleBtn) {
    toggleBtn.setSelected(property.getValue());

    Runnable updateDataFromFx = () -> {
      try {
        property.setValue(toggleBtn.isSelected());
        toggleBtn.setStyle("");
      } catch (InvalidPropertyException e) {
        Console.logError(e);
        toggleBtn.setStyle("-fx-text-fill: red");
        toggleBtn.setSelected(property.getValue());
      }
    };

    toggleBtn.focusedProperty().addListener((obs, oldValue, value) -> {
      if (oldValue && !value)
        updateDataFromFx.run();
    });
    toggleBtn.setOnAction(actionEvent -> updateDataFromFx.run());
  }

  public static <T> void bind(Property<T> property, ToggleGroup toggleGroup) {
    Runnable updateDataFromFx = () -> {
      try {
        @SuppressWarnings("unchecked")
        T value = (T) toggleGroup.getSelectedToggle().getUserData();
        property.setValue(value);
      } catch (InvalidPropertyException e) {
        Console.logError(e);
      }
    };

    for (var toggle : toggleGroup.getToggles()) {
      if (toggle.getUserData().equals(property.getValue()))
        toggle.setSelected(true);
    }
    toggleGroup.selectedToggleProperty().addListener(changeEvent -> updateDataFromFx.run());
  }

  public static <T> void bind(Property<RectangleData> property, TextField xTextField, TextField yTextField,
      TextField widthTextField, TextField heightTextField) {

    var textFields = new TextField[] { xTextField, yTextField, widthTextField, heightTextField };

    Runnable updateFxFromData = () -> {
      xTextField.setText(String.valueOf(property.getValue().x));
      yTextField.setText(String.valueOf(property.getValue().y));
      widthTextField.setText(String.valueOf(property.getValue().width));
      heightTextField.setText(String.valueOf(property.getValue().height));
    };

    Runnable updateDataFromFx = () -> {
      try {
        float x = parseValue(Float.class, xTextField.getText());
        float y = parseValue(Float.class, yTextField.getText());
        float width = parseValue(Float.class, widthTextField.getText());
        float height = parseValue(Float.class, heightTextField.getText());
        property.setValue(new RectangleData(x, y, width, height));

        for (var textField : textFields)
          textField.setStyle("");
      } catch (InvalidPropertyException e) {
        Console.logError(e);
        for (var textField : textFields)
          textField.setStyle("-fx-text-fill: red");
        updateFxFromData.run();
      }
    };

    updateFxFromData.run();

    for (var textField : textFields) {
      textField.focusedProperty().addListener((obs, oldValue, value) -> {
        if (oldValue && !value)
          updateDataFromFx.run();
      });
      textField.setOnAction(actionEvent -> updateDataFromFx.run());
    }
  }

  public static <T> void bindDistances(Property<RectangleData> property, TextField left, TextField right, TextField top,
      TextField bottom) {

    var textFields = new TextField[] { left, right, top, bottom };

    Runnable updateFxFromData = () -> {
      var value = property.getValue();
      left.setText(String.valueOf(value.width / 2 - value.x));
      right.setText(String.valueOf(value.width / 2 + value.x));
      top.setText(String.valueOf(value.height / 2 - value.y));
      bottom.setText(String.valueOf(value.height / 2 + value.y));
    };

    Runnable updateDataFromFx = () -> {
      try {
        float l = parseValue(Float.class, left.getText());
        float r = parseValue(Float.class, right.getText());
        float t = parseValue(Float.class, top.getText());
        float b = parseValue(Float.class, bottom.getText());
        property.setValue(new RectangleData((r - l) / 2, (b - t) / 2, r + l, t + b));

        for (var textField : textFields)
          textField.setStyle("");
      } catch (InvalidPropertyException e) {
        Console.logError(e);
        for (var textField : textFields)
          textField.setStyle("-fx-text-fill: red");
        updateFxFromData.run();
      }
    };

    updateFxFromData.run();

    for (var textField : textFields) {
      textField.focusedProperty().addListener((obs, oldValue, value) -> {
        if (oldValue && !value)
          updateDataFromFx.run();
      });
      textField.setOnAction(actionEvent -> updateDataFromFx.run());
    }
  }

}
