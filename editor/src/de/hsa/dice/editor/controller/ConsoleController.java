package de.hsa.dice.editor.controller;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.OutputLogger;
import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class ConsoleController implements OutputLogger {

  private static final int MAX_LINES = 32;

  @FXML
  private TextFlow console;

  @FXML
  private ScrollPane scrollPane;

  @FXML
  private TextField input;

  public ConsoleController() {
    Console.setOutputLogger(this);
  }

  public void initialize() {
    scrollPane.vvalueProperty().bind(console.heightProperty());
  }

  public void log(String message) {
    print(message, "#00DD00");
  }

  public void logWarning(String warning) {
    print(warning, "#DDDD00");
  }

  public void logError(String error) {
    print(error, "#DD0000");
  }

  @Override
  public void logError(Exception e) {
    logError(e.getMessage());
  }

  public void print(String message, String color) {
    String log = ">> " + message + System.lineSeparator();
    Text text = new Text();
    text.getStyleClass().add("console-text");
    text.setStyle("-fx-fill: " + color + ';');
    text.setText(log);
    if (console.getChildren().size() > MAX_LINES)
      console.getChildren().remove(0);
    console.getChildren().add(console.getChildren().size() - 1, text);
  }

  @FXML
  public void onClear() {
    console.getChildren().clear();
    console.getChildren().add(input);
  }

  @FXML
  public void onInput() {
    log(input.getText());
    input.clear();
  }

}
