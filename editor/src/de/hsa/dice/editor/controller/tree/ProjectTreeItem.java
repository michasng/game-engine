package de.hsa.dice.editor.controller.tree;

import de.hsa.dice.core.util.io.ProjectFileOperator;
import de.hsa.dice.core.view.IconRegistry;
import de.hsa.dice.editor.Editor;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;

public class ProjectTreeItem extends ContextMenuTreeItem {

  public ProjectTreeItem() {
    super(ProjectFileOperator.getInstance().getProjectName(), IconRegistry.getInstance().folder);

    MenuItem settings = new MenuItem("Settings");
    settings.setOnAction(actionEvent -> Editor.getInstance().getApp().onEditProjectSettings());
    getMenu().getItems().add(settings);
  }

  public void reloadChildren() {
    setValue(ProjectFileOperator.getInstance().getProjectName());
    for (TreeItem<String> item : this.getChildren()) {
      DataTypeTreeItem dataItem = (DataTypeTreeItem) item;
      dataItem.updateChildren();
      dataItem.subToDataObjects();
    }
  }

}
