package de.hsa.dice.editor.controller.tree;

import java.util.LinkedList;
import java.util.List;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.property.PropertySubscription;
import de.hsa.dice.core.view.IconRegistry;
import de.hsa.dice.editor.controller.DataEditorTabFactory;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;

public class DataTypeTreeItem extends ContextMenuTreeItem {

  private DataEditorTabFactory tabFactory;

  private PropertySubscription<?> sub;

  public DataTypeTreeItem(DataEditorTabFactory tabFactory) {
    super(tabFactory.getDataType().pluralName, IconRegistry.getInstance().fromDataType(tabFactory.getDataType()));
    this.tabFactory = tabFactory;

    MenuItem createNew = new MenuItem("Create New");
    createNew.setOnAction(actionEvent -> tabFactory.addTab(null));
    getMenu().getItems().add(createNew);

    updateChildren();

    subToDataObjects();
  }

  public void subToDataObjects() {
    if (sub != null)
      sub.cancelSubscription();

    var subManager = ProjectData.getCurrent().getSubManager(tabFactory.getDataType());
    if (subManager != null) // TODO: remove this check when all factories are implemented
      sub = subManager.getDataObjectsProperty().addListener((oldValue, value) -> updateChildren());
  }

  public void updateChildren() {
    this.getChildren().clear();
    this.getChildren().addAll(findChildren());
  }

  private List<TreeItem<String>> findChildren() {
    var subManager = ProjectData.getCurrent().getSubManager(getDataType());
    if (subManager == null) // TODO: remove this check when all factories are implemented
      return new LinkedList<>();
    var children = new LinkedList<TreeItem<String>>();
    for (String objectName : subManager.getNames()) {
      var dataObject = subManager.get(objectName);
      children.add(new DataObjectTreeItem(dataObject, tabFactory));
    }
    return children;
  }

  public DataType getDataType() {
    return tabFactory.getDataType();
  }

}
