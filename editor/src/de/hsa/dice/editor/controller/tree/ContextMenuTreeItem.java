package de.hsa.dice.editor.controller.tree;

import de.hsa.dice.core.Console;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class ContextMenuTreeItem extends TreeItem<String> {

  private ContextMenu menu;

  public ContextMenuTreeItem(String title, Image image) {
    super(title);

    ImageView imageView = new ImageView(image);
    imageView.setFitWidth(16);
    imageView.setFitHeight(16);
    this.setGraphic(imageView);

    menu = new ContextMenu();
  }

  public void onDoubleClicked(MouseEvent mouseEvent) {
  }

  public ContextMenu getMenu() {
    return menu;
  }

  public static class ContextMenuTreeCell extends TreeCell<String> {
    @Override
    public void updateItem(String item, boolean empty) {
      super.updateItem(item, empty);

      if (empty) {
        setText(null);
        setGraphic(null);
      } else {
        setText(getItem() == null ? "" : getItem().toString());
        setGraphic(getTreeItem().getGraphic());
        if (getTreeItem() instanceof ContextMenuTreeItem)
          setContextMenu(((ContextMenuTreeItem) getTreeItem()).getMenu());
        else {
          Console.logError("TreeItem " + getTreeItem().getValue() + " doesn't support ContextMenu");
        }
      }

      setOnMouseClicked(mouseEvent -> {
        if (mouseEvent.getClickCount() == 2 && getTreeItem() != null)
          ((ContextMenuTreeItem) getTreeItem()).onDoubleClicked(mouseEvent);
      });
    }
  }

}
