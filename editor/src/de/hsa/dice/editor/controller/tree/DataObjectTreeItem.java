package de.hsa.dice.editor.controller.tree;

import java.util.Optional;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.SubDataObject;
import de.hsa.dice.core.view.IconRegistry;
import de.hsa.dice.editor.controller.DataEditorTabFactory;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class DataObjectTreeItem extends ContextMenuTreeItem {

  private SubDataObject dataObject;

  private DataEditorTabFactory tabFactory;

  public DataObjectTreeItem(SubDataObject dataObject, DataEditorTabFactory tabFactory) {
    super(dataObject.getName(), IconRegistry.getInstance().fromDataType(dataObject.getDataType()));
    this.dataObject = dataObject;
    this.tabFactory = tabFactory;

    dataObject.getNameProperty().addListener((oldValue, value) -> this.setValue(value));

    MenuItem edit = new MenuItem("Edit");
    edit.setOnAction(actionEvent -> {
      tabFactory.addTab(dataObject.getName());
    });
    getMenu().getItems().add(edit);

    MenuItem duplicate = new MenuItem("Duplicate");
    duplicate.setOnAction(actionEvent -> {
      var subManager = dataObject.getManager();
      var newDataObject = subManager.duplicateFromJson(dataObject.getName(), dataObject.toJson());
      tabFactory.addTab(newDataObject.getName());
    });
    getMenu().getItems().add(duplicate);

    MenuItem delete = new MenuItem("Delete");
    delete.setOnAction(actionEvent -> {
      boolean deleteAndContinue = openDeleteDialog();
      if (!deleteAndContinue)
        return;

      var dataTypeTreeItem = ((DataTypeTreeItem) getParent());
      tabFactory.removeTab(dataObject.getName());
      var subManager = ProjectData.getCurrent().getSubManager(getDataType());
      subManager.remove(dataObject.getName());
      dataTypeTreeItem.getChildren().remove(this);
    });
    getMenu().getItems().add(delete);
  }

  private boolean openDeleteDialog() {
    Alert alert = new Alert(AlertType.CONFIRMATION);
    alert.setTitle("Confirm Delete");
    alert.setHeaderText("Do you want to delete the project \"" + dataObject.getName() + "\"?");
    alert.setContentText("You can only revert this change by reverting ALL current changes.");
    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
    stage.getIcons().add(IconRegistry.getInstance().dice);
    ImageView graphic = new ImageView(IconRegistry.getInstance().folder);
    graphic.setFitWidth(32);
    graphic.setFitHeight(32);
    alert.setGraphic(graphic);

    Optional<ButtonType> result = alert.showAndWait();
    if (result.get() == ButtonType.OK) {
      return true;
    } else {
      return false;
    }
  }

  public DataType getDataType() {
    var dataTypeTreeItem = ((DataTypeTreeItem) getParent());
    return dataTypeTreeItem.getDataType();
  }

  @Override
  public void onDoubleClicked(MouseEvent mouseEvent) {
    tabFactory.addTab(dataObject.getName());
  }

}
