package de.hsa.dice.editor;

import java.net.URL;

import de.hsa.dice.core.Config;
import de.hsa.dice.core.script.NodeRegistry;
import de.hsa.dice.core.util.io.ProjectFileOperator;
import de.hsa.dice.core.view.IconRegistry;
import de.hsa.dice.editor.controller.AppController;
import de.hsa.dice.editor.view.GraphicsRegistry;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Editor extends Application {

  private AppController appController;
  private Parent root;

  private Stage primaryStage;

  public static Editor instance;

  // has to be public, otherwise Singleton
  public Editor() {
    Editor.instance = this;
  }

  @Override
  public void start(Stage primaryStage) throws Exception {

    // initiate singletons
    Config.getInstance();
    ProjectFileOperator.getInstance();
    IconRegistry.getInstance();
    GraphicsRegistry.getInstance();
    NodeRegistry.getInstance();

    this.primaryStage = primaryStage;

    URL location = Editor.class.getClassLoader().getResource("app.fxml");
    FXMLLoader fxmlLoader = new FXMLLoader(location);
    root = fxmlLoader.load();
    appController = fxmlLoader.getController();
    appController.loadTheme();

    primaryStage.setOnCloseRequest(windowEvent -> {
      Config.getInstance().save();
      Platform.exit();
      System.exit(0);
    });
    primaryStage.setTitle("Console Editor");
    primaryStage.setScene(new Scene(root));
    primaryStage.getIcons().add(IconRegistry.getInstance().dice);
    primaryStage.getScene().getStylesheets().add("styles.css");

    primaryStage.show();
  }

  public Stage getPrimaryStage() {
    return primaryStage;
  }

  public Parent getRoot() {
    return root;
  }

  public AppController getApp() {
    return appController;
  }

  public static Editor getInstance() {
    return instance;
  }

  public static void main(String[] args) {
    Application.launch(Editor.class, args);
  }

}