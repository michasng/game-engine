package de.hsa.dice.editor.view;

import java.util.LinkedList;
import java.util.logging.Logger;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.InputManager;
import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.EntityData;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.TileData;
import de.hsa.dice.core.dataobject.nested.MapEntityData;
import de.hsa.dice.core.dataobject.nested.MapTileData;
import de.hsa.dice.core.dataobject.nested.VectorData;
import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.game.entity.Entity;
import de.hsa.dice.core.game.entity.EntityType;
import de.hsa.dice.core.game.entity.MockEntity;
import de.hsa.dice.core.game.map.Tile;
import de.hsa.dice.core.game.map.TileVec;
import de.hsa.dice.core.property.PropertySubscription;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class MapEditorGame extends Game {

  private static Logger logger = Logger.getLogger(MapEditorGame.class.getName());

  private static final int TILE_MODE = 0, ENTITY_MODE = 1;

  private int selectionMode;

  private int selectedTile;

  private String selectedEntity;
  private MockEntity entityPreview;

  private PropertySubscription<?> numColsSub, numRowsSub;

  public MapEditorGame(ProjectData project) {
    super(project, false);

    numColsSub = map.getDataObject().getNumColsProperty().addListener((oldValue, value) -> map.updateTilesFromData());
    numRowsSub = map.getDataObject().getNumRowsProperty().addListener((oldValue, value) -> map.updateTilesFromData());
  }

  public void selectTile(String tileName) {
    selectionMode = TILE_MODE;
    if (tileName == null) { // deleting
      selectedTile = 0;
      logger.fine("Selecting Tile Eraser with id " + selectedTile);
      return;
    }

    LinkedList<MapTileData> tileIds = map.getDataObject().getTileIds();
    int maxId = 0;
    for (MapTileData tileId : tileIds) {
      if (tileId.getId() > maxId)
        maxId = tileId.getId();

      if (tileId.getName().equals(tileName)) {
        selectedTile = tileId.getId();
        logger.fine("Selecting already existing Tile " + tileName + " with id " + selectedTile);
        return;
      }
    }

    // tile unknown so far
    var mapTileData = new MapTileData(map.getDataObject());
    mapTileData.setId(maxId + 1);
    mapTileData.setName(tileName);
    tileIds.add(mapTileData);
    var tile = new Tile((TileData) projectData.getSubManager(DataType.TILE).get(tileName));
    map.addTile(mapTileData.getId(), tile);
    selectedTile = mapTileData.getId();
    logger.fine("Selecting new Tile " + tileName + " with id " + selectedTile);
  }

  public void selectEntity(String entityName) {
    selectionMode = ENTITY_MODE;
    selectedEntity = entityName;
    if (entityName != null) {
      var entityData = projectData.getSubManager(DataType.ENTITY, EntityData.class).get(selectedEntity);
      entityPreview = new MockEntity(new EntityType(entityData));
    }
  }

  private Entity highlightedEntity;

  @Override
  public void handleInput() {
    camMouseMove();
    camMouseScroll();
    camera.updateScaledOffset(sceneView.getSize());

    Vec2 mouseWorldPos = camera.pixelToWorld(input.getMousePos());
    TileVec mouseTilePos = new TileVec(mouseWorldPos);

    if (highlightedEntity != null)
      highlightedEntity.setShowHitbox(false);
    highlightedEntity = map.getEntityAtPos(mouseWorldPos);
    if (highlightedEntity != null && selectedEntity == null) {
      highlightedEntity.setShowHitbox(true);
    }

    if (input.isMouseClicked(InputManager.MOUSE_PRIMARY)) {
      if (selectionMode == ENTITY_MODE) {
        if (selectedEntity == null) {
          if (highlightedEntity != null) {
            // remove the DataObject
            var mapEntity = highlightedEntity.getMapEntityData();
            map.getDataObject().getEntities().remove(mapEntity);
            mapEntity.onDestory();
            // update current map
            map.despawnEntity(highlightedEntity);
          }
        } else {
          // add the DataObject
          MapEntityData mapEntity = new MapEntityData(map.getDataObject());
          mapEntity.setName(selectedEntity);
          VectorData posData = new VectorData(mouseWorldPos.x, mouseWorldPos.y);
          mapEntity.setPos(posData);
          map.getDataObject().getEntities().add(mapEntity);
          // update current map
          map.spawnEntity(mapEntity);
        }
      }
    }

    if (input.isMousePressed(InputManager.MOUSE_PRIMARY)) {
      if (map.isWithinBounds(mouseTilePos)) {
        if (selectionMode == TILE_MODE && map.getTile(mouseTilePos) != selectedTile) {
          map.setTile(mouseTilePos, selectedTile);
          var mapData = map.getDataObject();
          mapData.getTiles()[mouseTilePos.y][mouseTilePos.x] = selectedTile;
          mapData.setTiles(mapData.getTiles()); // to invoke listeners
        }
      }
    }

  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    numColsSub.cancelSubscription();
    numRowsSub.cancelSubscription();
    projectData.onDestroy();
  }

  @Override
  public void cameraRender(GraphicsContext gc) {
    super.cameraRender(gc);

    // render entity preview
    if (selectionMode == ENTITY_MODE && selectedEntity != null) {
      var mousePos = input.getMousePos();
      entityPreview.setPosition(camera.pixelToWorld(mousePos));
      entityPreview.render(gc);
    }

    gc.setLineWidth(1);
    // render grid when tile is selected
    gc.setStroke(new Color(1, 1, 1, .5));
    if (selectionMode == TILE_MODE && selectedTile != 0) {
      for (int row = 0; row < map.getNumRows(); row++) {
        for (int col = 0; col < map.getNumCols(); col++) {
          gc.strokeRect(col * Game.PIXEL_PER_METER, row * Game.PIXEL_PER_METER, Game.PIXEL_PER_METER,
              Game.PIXEL_PER_METER);
        }
      }
    }

    // render outline of map
    gc.setStroke(Color.WHITE);
    gc.strokeRect(0, 0, getWidth(), getHeight());
  }

}
