package de.hsa.dice.editor.view.scripts;

import org.jbox2d.common.Vec2;

import javafx.scene.canvas.GraphicsContext;

public class Connection extends Highlightable {

  private NodeOutput source;
  private NodeInput target;

  private ScriptEditorGame scriptContext;

  public Connection(NodeOutput source, NodeInput target, ScriptEditorGame scriptContext) {
    this.source = source;
    this.target = target;
    this.scriptContext = scriptContext;
  }

  @Override
  public boolean contains(Vec2 scenePoint) {
    return false;
  }

  @Override
  public void render(GraphicsContext gc) {
    var drawMinimal = (scriptContext.getCamera().getScale() < 0.5);
    if (drawMinimal) {
      gc.strokeLine(getX() - ScriptEditorGame.BASE_SIZE / 4, getY(), getX2() + ScriptEditorGame.BASE_SIZE / 4, getY2());
    } else {
      gc.beginPath();
      gc.bezierCurveTo(getX(), getY(), (getX() + getCenterX()) / 2, getY(), getCenterX(), getCenterY());
      gc.bezierCurveTo(getCenterX(), getCenterY(), (getCenterX() + getX2()) / 2, getY2(), getX2(), getY2());
      gc.stroke();
    }
  }

  @Override
  public void renderHighlight(GraphicsContext gc) {
  }

  @Override
  public float getX() {
    return source.getCenterX();
  }

  @Override
  public float getY() {
    return source.getCenterY();
  }

  @Override
  public float getX2() {
    return target.getCenterX();
  }

  @Override
  public float getY2() {
    return target.getCenterY();
  }

  @Override
  public float getWidth() {
    return getX2() - getX();
  }

  @Override
  public float getHeight() {
    return getY2() - getY();
  }

  public NodeOutput getSource() {
    return source;
  }

  public NodeInput getTarget() {
    return target;
  }

}
