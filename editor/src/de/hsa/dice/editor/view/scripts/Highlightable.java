package de.hsa.dice.editor.view.scripts;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.view.Renderable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.MenuItem;

public abstract class Highlightable implements Renderable {

  public boolean contains(Vec2 scenePoint) {
    return (scenePoint.x > getX() && scenePoint.x < getX2() && scenePoint.y > getY() && scenePoint.y < getY2());
  }

  public boolean overlaps(Vec2 minSceneCorner, Vec2 maxSceneCorner) {
    return (getX() < maxSceneCorner.x && getY() < maxSceneCorner.y && getX2() > minSceneCorner.x
        && getY2() > minSceneCorner.y);
  }

  public MenuItem[] getContextMenuItems() {
    return null;
  }

  public void renderSelected(GraphicsContext gc) {
    gc.setFill(ScriptEditorGame.SELECT_COLOR);
    gc.fillRect(getX(), getY(), getWidth(), getHeight());
  }

  public abstract void renderHighlight(GraphicsContext gc);

  public float getCenterX() {
    return getX() + getWidth() / 2;
  }

  public float getCenterY() {
    return getY() + getHeight() / 2;
  }

  public Vec2 getCenterPos() {
    return new Vec2(getCenterX(), getCenterY());
  }

  public float getX2() {
    return getX() + getWidth();
  }

  public float getY2() {
    return getY() + getHeight();
  }

  public Vec2 getSize() {
    return new Vec2(getWidth(), getHeight());
  }

  public abstract float getX();

  public abstract float getY();

  public abstract float getWidth();

  public abstract float getHeight();

}
