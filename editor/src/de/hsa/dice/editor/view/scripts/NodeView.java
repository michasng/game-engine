package de.hsa.dice.editor.view.scripts;

import java.util.LinkedList;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.dataobject.nested.NodeData;
import de.hsa.dice.editor.view.GraphicsRegistry;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.text.TextAlignment;

public class NodeView extends Highlightable {

  public static final float NODE_WIDTH = ScriptEditorGame.BASE_SIZE * 4;
  public static final float NODE_HEIGHT = ScriptEditorGame.BASE_SIZE * 4;

  private NodeData dataObject;

  private NodeInput[] inputs;
  private LinkedList<NodeOutput> outputs;

  private ScriptEditorGame scriptContext;

  private String name;
  private Image graphics;

  public NodeView(NodeData dataObject, ScriptEditorGame scriptContext) {
    this.dataObject = dataObject;
    this.scriptContext = scriptContext;

    inputs = new NodeInput[getNumInputs()];
    for (int i = 0; i < inputs.length; i++)
      inputs[i] = new NodeInput(this, i);
    outputs = new LinkedList<>();
    outputs.add(new NodeOutput(this));

    var factory = dataObject.getFactory();
    if (factory == null) {
      graphics = GraphicsRegistry.getInstance().get("Warning");
      name = dataObject.getType();
    } else {
      graphics = GraphicsRegistry.getInstance().get(factory.getGraphics());
      name = factory.getDisplayName();
    }
  }

  public MenuItem[] getContextMenuItems() {
    MenuItem delete = new MenuItem("Delete");
    delete.setOnAction(actionEvent -> scriptContext.deleteSelectedNodes());

    return new MenuItem[] { delete };
  }

  public Highlightable getContainer(Vec2 scenePoint) {
    for (var connector : inputs) {
      if (connector.contains(scenePoint))
        return connector;
    }
    for (var connector : outputs) {
      if (connector.contains(scenePoint))
        return connector;
    }

    if (contains(scenePoint))
      return this;

    return null;
  }

  @Override
  public void render(GraphicsContext gc) {
    var drawMinimal = (scriptContext.getCamera().getScale() < 0.5);

    // base shape
    gc.setFill(ScriptEditorGame.LIGHT_GRAY);
    gc.fillRoundRect(getX(), getY(), getWidth(), getHeight(), getArcSize(), getArcSize());

    // graphics
    if (graphics != null) {
      gc.drawImage(graphics, getX() + getWidth() / 2 - getGraphicsSize() / 2, getY() + getHeight() - getGraphicsSize(),
          getGraphicsSize(), getGraphicsSize());
    }

    if (!drawMinimal) {

      // title
      gc.setTextAlign(TextAlignment.CENTER);
      gc.setTextBaseline(VPos.CENTER);
      gc.setFill(ScriptEditorGame.DARK_GRAY);
      gc.fillText(name, getX() + getWidth() / 2, getY() + getTitleSize() / 2, getWidth() - getArcSize());

      // connectors
      for (var connector : inputs)
        connector.render(gc);
      for (var connector : outputs)
        connector.render(gc);

      // outline
      gc.setStroke(ScriptEditorGame.DARK_GRAY);
      gc.setLineWidth(ScriptEditorGame.BASE_SIZE / 16);
      gc.strokeRoundRect(getX(), getY(), getWidth(), getHeight(), getArcSize(), getArcSize());

      // title separator
      gc.strokeLine(getX() + getArcSize() / 2, getY() + getTitleSize(), getX() - getArcSize() / 2 + getWidth(),
          getY() + getTitleSize());

    }
  }

  @Override
  public void renderHighlight(GraphicsContext gc) {
    gc.strokeRoundRect(getX(), getY(), getWidth(), getHeight(), getArcSize(), getArcSize());
  }

  @Override
  public void renderSelected(GraphicsContext gc) {
    gc.setFill(ScriptEditorGame.SELECT_COLOR);
    gc.fillRoundRect(getX(), getY(), getWidth(), getHeight(), getArcSize(), getArcSize());
  }

  public int getNumInputs() {
    if (dataObject.getFactory() == null)
      return 2; // fallback
    return dataObject.getFactory().getNumInputs();
  }

  public NodeInput getInput(int index) {
    return inputs[index];
  }

  public void removeInput(int index) {
    inputs[index] = null;
  }

  public LinkedList<NodeOutput> getOutputs() {
    return outputs;
  }

  public NodeOutput addOutput() {
    var output = new NodeOutput(this);
    outputs.add(outputs.size() - 1, output);
    return output;
  }

  public NodeOutput getOutput(int index) {
    return outputs.get(index);
  }

  public NodeData getDataObject() {
    return dataObject;
  }

  @Override
  public float getX() {
    return dataObject.getPos().x;
  }

  @Override
  public float getY() {
    return dataObject.getPos().y;
  }

  public float getGraphicsSize() {
    return getWidth() < (getHeight() - getTitleSize()) ? getWidth() : (getHeight() - getTitleSize());
  }

  @Override
  public float getWidth() {
    return NODE_WIDTH;
  }

  @Override
  public float getHeight() {
    return NODE_HEIGHT;
  }

  public float getTitleSize() {
    return ScriptEditorGame.BASE_SIZE;
  }

  public float getArcSize() {
    return ScriptEditorGame.BASE_SIZE;
  }

}
