package de.hsa.dice.editor.view.scripts;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;

public abstract class NodePort extends Highlightable {

  protected final NodeView nodeView;

  protected static Stop[] STOPS = new Stop[] { new Stop(0, ScriptEditorGame.LIGHT_GRAY),
      new Stop(0.5, ScriptEditorGame.LIGHT_GRAY), new Stop(1, Color.TRANSPARENT) };

  public NodePort(NodeView nodeView) {
    this.nodeView = nodeView;
  }

  @Override
  public void render(GraphicsContext gc) {
//    gc.setFill(ScriptEditorGame.LIGHT_GRAY);
    gc.setFill(getGradient());
    gc.fillRect(getX(), getY(), getWidth(), getHeight());
  }

  @Override
  public void renderHighlight(GraphicsContext gc) {
    gc.strokeRect(getX(), getY(), getWidth(), getHeight());
  }

  protected abstract LinearGradient getGradient();

  @Override
  public float getWidth() {
    return ScriptEditorGame.BASE_SIZE / 2;
  }

  @Override
  public float getHeight() {
    return ScriptEditorGame.BASE_SIZE / 2;
  }

  @Override
  public float getY() {
    return nodeView.getY() + nodeView.getTitleSize() + (2 * getHeight() * getIndex());
  }

  public abstract int getIndex();

}
