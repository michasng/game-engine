package de.hsa.dice.editor.view.scripts;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;

public class NodeOutput extends NodePort {

  private static LinearGradient GRADIENT = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, NodePort.STOPS);

  public NodeOutput(NodeView nodeView) {
    super(nodeView);
  }

  @Override
  public void render(GraphicsContext gc) {
    super.render(gc);
    // if this is not a "fake" output, with no connection
//    if (nodeView.getDataObject().getOutputs().size() > getIndex()) {
//      gc.setFill(Color.BLACK);
//      gc.setTextBaseline(VPos.TOP);
//      var output = nodeView.getDataObject().getOutputs().get(getIndex());
//      gc.fillText(output.getTargetNodeId() + "," + output.getTargetPort(), getX() + getWidth() / 2, getY(), getWidth());
//    }
  }

  @Override
  protected LinearGradient getGradient() {
    return GRADIENT;
  }

  @Override
  public float getX() {
    return nodeView.getX() + nodeView.getWidth();
  }

  @Override
  public int getIndex() {
    for (int i = 0; i < nodeView.getOutputs().size(); i++) {
      if (this == nodeView.getOutput(i))
        return i;
    }
    return -1;
  }

}
