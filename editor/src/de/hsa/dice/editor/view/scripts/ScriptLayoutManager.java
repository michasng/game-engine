package de.hsa.dice.editor.view.scripts;

import java.util.HashMap;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

import de.hsa.dice.core.dataobject.nested.NodeData;
import de.hsa.dice.core.dataobject.nested.VectorData;
import de.hsa.dice.core.script.Script;
import de.hsa.dice.core.script.ScriptNode;

public class ScriptLayoutManager {

  private ScriptEditorGame scriptContext;

  private boolean locked;

  private int maxIndex;
  private HashMap<ScriptNode, Object> alreadyLayedOut;

  public ScriptLayoutManager(ScriptEditorGame scriptContext) {
    this.scriptContext = scriptContext;
    alreadyLayedOut = new HashMap<>();
  }

  public void applyLayout() {
    if (locked)
      return;
    locked = true;

    var script = new Script(scriptContext.getDataObject(), null);

//    var nodeIndexMap = new HashMap<ScriptNode, Integer>();
//    var indexCounts = new int[1000];
//    for (var node : script.getNodes()) {
//      int maxDepth = node.getMaxDepth();
//      nodeIndexMap.put(node, maxDepth);
//      indexCounts[maxDepth] = indexCounts[maxDepth] + 1;
//    }

    if (script.getNodes().size() == 0)
      return;

    var halfBodySize = new Vec2(NodeView.NODE_WIDTH, NodeView.NODE_HEIGHT).mul(0.75f);
    World world = new World(new Vec2(0, 0));
    var bodyDef = new BodyDef();
    bodyDef.type = BodyType.DYNAMIC;
    var shape = new PolygonShape();
    shape.setAsBox(halfBodySize.x, halfBodySize.y); // half size
    var fixtureDef = new FixtureDef();
    fixtureDef.density = 1f;
    fixtureDef.friction = 0.5f;
    fixtureDef.restitution = 0.5f;
    fixtureDef.shape = shape;
    for (var node : script.getNodes()) {
      var nodeData = node.getDataObject();
      var nodePos = nodeData.getPos();
      bodyDef.position.set(nodePos.x + halfBodySize.x, nodePos.y + halfBodySize.y); // center
      var body = world.createBody(bodyDef);
      body.createFixture(fixtureDef);
      body.setUserData(nodeData);
    }

    for (int i = 0; i < 10000; i++) {
      world.step(1f / 30, 1, 1);
    }
    var body = world.getBodyList();
    do {
      var node = (NodeData) body.getUserData();
      node.setPos(new VectorData(body.getPosition().sub(halfBodySize)));
    } while ((body = body.getNext()) != null);
  }

  @SuppressWarnings("unused")
  private void applyLayoutOld() {
    maxIndex = 0;
    alreadyLayedOut.clear();

    var script = new Script(scriptContext.getDataObject(), null);
    for (int i = 0; i < script.getBeginNodes().size(); i++) {
      layoutNodeOld(script.getBeginNodes().get(i), 0);
      maxIndex++;
    }
  }

  private void layoutNodeOld(ScriptNode node, int depth) {
    if (alreadyLayedOut.containsKey(node))
      return;
    alreadyLayedOut.put(node, null);
    node.getDataObject()
        .setPos(new VectorData(depth * NodeView.NODE_WIDTH * 1.5f, maxIndex * NodeView.NODE_HEIGHT * 1.5f));
    depth++;
    for (int i = 0; i < node.getOutputNodes().size(); i++) {
      if (i > 0)
        maxIndex++;
      layoutNodeOld(node.getOutputNodes().get(i), depth);
    }
  }

  public void reset() {
    if (locked)
      locked = false;
  }

}
