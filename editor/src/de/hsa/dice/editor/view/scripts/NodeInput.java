package de.hsa.dice.editor.view.scripts;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;

public class NodeInput extends NodePort {

  private int index;

  private static LinearGradient GRADIENT = new LinearGradient(1, 0, 0, 0, true, CycleMethod.NO_CYCLE, NodePort.STOPS);

  public NodeInput(NodeView nodeView, int index) {
    super(nodeView);
    this.index = index;
  }

  @Override
  public void render(GraphicsContext gc) {
    super.render(gc);
//    gc.setFill(Color.BLACK);
//    gc.setTextBaseline(VPos.TOP);
//    gc.fillText(String.valueOf(nodeView.getDataObject().getId()), getX() + getWidth() / 2, getY(), getWidth());
  }

  @Override
  protected LinearGradient getGradient() {
    return GRADIENT;
  }

  @Override
  public float getX() {
    return nodeView.getX() - getWidth();
  }

  @Override
  public int getIndex() {
    return index;
  }

}
