package de.hsa.dice.editor.view.scripts;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Vec2;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.ParseException;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.InputManager;
import de.hsa.dice.core.dataobject.ScriptData;
import de.hsa.dice.core.dataobject.nested.FieldData;
import de.hsa.dice.core.dataobject.nested.NodeData;
import de.hsa.dice.core.dataobject.nested.NodeOutputData;
import de.hsa.dice.core.dataobject.nested.VectorData;
import de.hsa.dice.core.game.GameBase;
import de.hsa.dice.core.property.PropNotifyList;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.script.NodeFactory;
import de.hsa.dice.core.script.NodeRegistry;
import de.hsa.dice.core.script.Script;
import de.hsa.dice.core.script.ScriptNode;
import de.hsa.dice.editor.ClipboardData;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

public class ScriptEditorGame extends GameBase {

  public static final float BASE_SIZE = 32f;
  public static final float GRID_SIZE = BASE_SIZE / 2;

  public static final Color DARK_GRAY = Color.rgb(0x2D, 0x2D, 0x2D), LIGHT_GRAY = Color.rgb(0xD2, 0xD2, 0xD2),
      LIGHT_COLOR = Color.rgb(0xFA, 0xF0, 0x02), BASE_COLOR = Color.rgb(0xFC, 0xD7, 0x02),
      DARK_COLOR = Color.rgb(0xFC, 0x9F, 0x02), WARN_COLOR = Color.rgb(0xFA, 0x7B, 0x02);
  public static final Color SELECT_COLOR = Color.color(0, 0, 1, 0.5);

  private ScriptData dataObject;

  private LinkedList<NodeView> nodeViews;

  private LinkedList<Connection> connections;

  private Highlightable hovered;
  private Property<PropNotifyList<NodeView>> selectedProperty;

  private Vec2 dragSelectStart;

  private NodePort draggedPort;

  private int maxId; // starting at 1, 0 means no connection

  private boolean justCopied;
  private boolean justPasted;

  private ScriptLayoutManager layoutManager;

  private Script script;

  public ScriptEditorGame(ScriptData dataObject) {
    super();

    this.dataObject = dataObject;
    nodeViews = new LinkedList<>();
    connections = new LinkedList<>();

    selectedProperty = new Property<>("selected", null);
    var selectedList = new PropNotifyList<>(selectedProperty);
    selectedProperty.setValue(selectedList, false);

    // add existing nodes to view
    for (var nodeData : dataObject.getNodes())
      nodeViews.add(new NodeView(nodeData, this));
    updateMaxId();

    // add connections to view
    for (var nodeData : dataObject.getNodes())
      addConnectionsFromNodeData(nodeData);

    mouseMapMovementBtn = InputManager.MOUSE_SECONDARY;
    // center camera
    camera.focus(0, 0);

    layoutManager = new ScriptLayoutManager(this);
  }

  private void addConnectionsFromNodeData(NodeData nodeData) {
    var nodeView = getNodeById(nodeData.getId());
    var outputs = nodeData.getOutputs();
    // for each output of each node
    for (var output : outputs) {
      var sourcePort = nodeView.addOutput();
      var target = getNodeById(output.getTargetNodeId());
      var targetPort = target.getInput(output.getTargetPort());
      connections.add(new Connection(sourcePort, targetPort, this));
    }
  }

  public void updateMaxId() {
    for (var nodeData : dataObject.getNodes())
      if (nodeData.getId() > maxId)
        maxId = nodeData.getId();
  }

  public NodeView getNodeById(int id) {
    for (NodeView nodeView : nodeViews)
      if (nodeView.getDataObject().getId() == id)
        return nodeView;

    return null;
  }

  public void createNode(String nodeName) {
    Vec2 scenePos = camera.pixelToScene(input.getMousePos());
    scenePos = toGrid(scenePos);
    NodeData nodeData = new NodeData(dataObject);
    nodeData.setId(++maxId);
    nodeData.setType(nodeName);
    // only deserialize nodeData for getNumInputs()
    NodeFactory factory = NodeRegistry.getInstance().getFactory(nodeName);
    if (factory == null) {
      Console.logError("Node type does not exist: " + nodeName);
      return;
    }
    if (factory.getNumInputs() == 0) { // event node
      for (var nodeView : nodeViews) {
        if (nodeView.getDataObject().getType().equals(factory.getTypeName())) {
          camera.focus(nodeView.getCenterPos());
          camera.setScale(1); // no need to update camera, bc that happens in the render loop anyway
          Console.logWarning("Event already exists.");
          return;
        }
      }
    }

    List<Field> fields = factory.getInspectedFields();
    FieldData[] fieldDatas = new FieldData[fields.size()];
    for (int i = 0; i < fields.size(); i++) {
      var field = fields.get(i);
      String typeName = field.getType().getName();
      String fieldName = field.getName();
      fieldDatas[i] = new FieldData(dataObject, typeName, fieldName, field);
      // keep the default value from the node class
      try {
        ScriptNode nodeInstance = factory.createNode(null, nodeData);
        fields.get(i).setAccessible(true); // allow access to private fields
        Object defaultValue = fields.get(i).get(nodeInstance);
        if (defaultValue != null) // unless it is null
          fieldDatas[i].setValue(defaultValue);
      } catch (IllegalArgumentException | IllegalAccessException e) {
        e.printStackTrace();
      }
    }
    nodeData.setFields(fieldDatas);
    nodeData.setPos(new VectorData(scenePos));
    dataObject.getNodes().add(nodeData);
    nodeViews.add(new NodeView(nodeData, this));
  }

  public void deleteSelectedNodes() {
    for (var selected : getSelectedList()) {
      nodeViews.remove(selected);
      dataObject.getNodes().remove(selected.getDataObject());
      if (hovered == selected)
        hovered = null;

      var toRemove = getConnections(selected);
      for (Connection connection : toRemove) {
        deleteConnection(connection);
      }
    }
    updateMaxId();

    getSelectedList().clear();
  }

  public void deleteConnection(Connection connection) {
    connections.remove(connection);

    // remove the output port entirely, but keep the input
    int outputIndex = connection.getSource().getIndex();
    connection.getSource().nodeView.getOutputs().remove(outputIndex);

    // erase the connection from data
    int targetNodeId = connection.getTarget().nodeView.getDataObject().getId();
    int inputIndex = connection.getTarget().getIndex();
    var outputNodeData = connection.getSource().nodeView.getDataObject();
    var outputs = outputNodeData.getOutputs();
    for (var output : outputs) {
      if (output.getTargetNodeId() == targetNodeId && output.getTargetPort() == inputIndex) {
        outputs.remove(output);
        break;
      }
    }
  }

  public List<Connection> getConnections(NodePort port) {
    var<Connection> filtered = new LinkedList<Connection>();
    for (var connection : connections) {
      if (connection.getSource() == port || connection.getTarget() == port)
        filtered.add(connection);
    }
    return filtered;
  }

  public void copyToClipboard() {
    Clipboard clipboard = Clipboard.getSystemClipboard();
    ClipboardContent content = new ClipboardContent();

    var jsonArray = new JsonArray();
    for (var selected : getSelectedList())
      jsonArray.add(selected.getDataObject().toJson());
    var cbData = new ClipboardData("nodes", jsonArray);
    content.putString(cbData.toString());
    clipboard.setContent(content);
  }

  public void pasteFromClipboard() {
    Clipboard clipboard = Clipboard.getSystemClipboard();
    if (!clipboard.hasString())
      return;

    ClipboardData cbData;
    try {
      cbData = new ClipboardData(clipboard.getString());
    } catch (ParseException e) {
      return;
    }
    if (!cbData.getType().equals("nodes"))
      return;

    var jsonArray = cbData.getValue().asArray();
    var mousePos = camera.pixelToScene(input.getMousePos());
    var idMap = new TreeMap<Integer, Integer>();
    var newNodeDatas = new LinkedList<NodeData>();
    var minX = Float.MAX_VALUE;
    var minY = Float.MAX_VALUE;
    getSelectedList().clear();
    // fix IDs, add to scriptData
    for (var jsonNode : jsonArray) {
      var nodeData = new NodeData(dataObject, jsonNode.asObject());
      var pos = nodeData.getPos();
      if (minX > pos.x)
        minX = pos.x;
      if (minY > pos.y)
        minY = pos.y;

      idMap.put(nodeData.getId(), ++maxId);
      nodeData.setId(maxId);
      newNodeDatas.add(nodeData);

      dataObject.getNodes().add(nodeData);
      var nodeView = new NodeView(nodeData, this);
      nodeViews.add(nodeView);
      getSelectedList().add(nodeView);
    }

    // fix up positions and references, requires working IDs
    mousePos.subLocal(new Vec2(minX, minY));
    for (var nodeData : newNodeDatas) {
      nodeData.setPos(new VectorData(mousePos.add(nodeData.getPos().toVec2())));

      var outsideReferences = new LinkedList<NodeOutputData>();
      for (var output : nodeData.getOutputs()) {
        var newTargetId = idMap.get(output.getTargetNodeId());
        if (newTargetId != null)
          output.setTargetNodeId(newTargetId);
        else
          outsideReferences.add(output);
      }
      // second loop prevents ConcurrentModificationException
      for (var output : outsideReferences)
        nodeData.getOutputs().remove(output);
    }

    // add connections to view, requires working references
    for (var nodeData : newNodeDatas)
      addConnectionsFromNodeData(nodeData);
  }

  public List<Connection> getConnections(NodeView nodeView) {
    var<Connection> filtered = new LinkedList<Connection>();
    for (var connection : connections)
      if (connection.getSource().nodeView == nodeView || connection.getTarget().nodeView == nodeView)
        filtered.add(connection);
    return filtered;
  }

  public List<Connection> getInputs(NodeInput target) {
    var<Connection> filtered = new LinkedList<Connection>();
    for (var connection : connections)
      if (connection.getTarget() == target)
        filtered.add(connection);
    return filtered;
  }

  public List<Connection> getOutputs(NodeOutput source) {
    var<Connection> filtered = new LinkedList<Connection>();
    for (var connection : connections)
      if (connection.getSource() == source)
        filtered.add(connection);
    return filtered;
  }

  @Override
  public void cameraRender(GraphicsContext gc) {
    float visibleWidth = camera.getVisibleWidth();
    float visibleHeight = camera.getVisibleHeight();
    float minX = camera.getX() - visibleWidth / 2;
    float minY = camera.getY() - visibleHeight / 2;
    float maxX = minX + visibleWidth;
    float maxY = minY + visibleHeight;

    // background
    gc.setFill(Color.DARKGRAY);
    gc.fillRect(minX, minY, visibleWidth, visibleHeight);

    // grid
    gc.setStroke(Color.DIMGRAY);
    for (float y = toGrid(minY - 1); y < maxY + 1; y += GRID_SIZE) {
      var row = y / GRID_SIZE;
      if (row % 16 == 0)
        gc.setLineWidth(2);
      else if (row % 4 == 0)
        gc.setLineWidth(1.5);
      else
        gc.setLineWidth(1);
      gc.strokeLine(toGrid(minX), y, toGrid(maxX), y);
    }
    for (float x = toGrid(minX - 1); x < maxX + 1; x += GRID_SIZE) {
      var column = x / GRID_SIZE;
      if (column % 16 == 0)
        gc.setLineWidth(2);
      else if (column % 4 == 0)
        gc.setLineWidth(1.5);
      else
        gc.setLineWidth(1);
      gc.strokeLine(x, toGrid(minY), x, toGrid(maxY));
    }

    // center dot
    gc.setFill(Color.DIMGRAY);
    gc.fillRect(-GRID_SIZE / 2, -GRID_SIZE / 2, GRID_SIZE, GRID_SIZE);

    if (script == null) { // edit mode
      gc.setStroke(BASE_COLOR);
      gc.setLineWidth(4);
      for (var connection : connections)
        if (camera.isVisible(connection.getSource().getCenterPos())
            || camera.isVisible(connection.getTarget().getCenterPos()))
          connection.render(gc);
    } else { // preview mode

      for (int i = 0; i < nodeViews.size(); i++) {
        var nodeView = nodeViews.get(i);
        var node = script.getNodes().get(i);

        if (node.getValue()) {
          gc.setStroke(WARN_COLOR);
          gc.setLineWidth(6);
        } else {
          gc.setStroke(BASE_COLOR);
          gc.setLineWidth(4);
        }
        for (var conn : connections) {
          if (conn.getSource().nodeView == nodeView) {
            conn.render(gc);
          }
        }
      }
    }

    var mouseScenePos = camera.pixelToScene(input.getMousePos());
    // draggedPort Connection
    if (draggedPort != null) {
      gc.setStroke(BASE_COLOR);
      gc.setLineWidth(4);
      gc.strokeLine(draggedPort.getCenterX(), draggedPort.getCenterY(), mouseScenePos.x, mouseScenePos.y);
    }

    // nodes
    for (var node : nodeViews) {
      if (camera.isVisible(node.getCenterPos(), node.getSize()))
        node.render(gc);
    }
    if (hovered != null) {
      gc.setStroke(WARN_COLOR);
      hovered.renderHighlight(gc);
    }

    for (var selected : getSelectedList())
      selected.renderSelected(gc);

    // selected area
    if (dragSelectStart != null) {
      var deltaScene = mouseScenePos.sub(dragSelectStart).abs();
      gc.setFill(SELECT_COLOR);
      gc.fillRect(Math.min(dragSelectStart.x, mouseScenePos.x), Math.min(dragSelectStart.y, mouseScenePos.y),
          deltaScene.x, deltaScene.y);
    }
  }

  @Override
  public void onDestroy() {
    // do nothing so far
  }

  public Highlightable getContainer(Vec2 scenePoint) {
    for (int i = nodeViews.size() - 1; i >= 0; i--) { // reverse iterate, bc of rendering order
      var nodeView = nodeViews.get(i);
      var container = nodeView.getContainer(scenePoint);
      if (container != null)
        return container;
    }
    return null;
  }

  public NodeView[] getNodesInArea(Vec2 sceneCorner1, Vec2 sceneCorner2) {
    var minSceneCorner = new Vec2(Math.min(sceneCorner1.x, sceneCorner2.x), Math.min(sceneCorner1.y, sceneCorner2.y));
    var maxSceneCorner = new Vec2(Math.max(sceneCorner1.x, sceneCorner2.x), Math.max(sceneCorner1.y, sceneCorner2.y));
    var list = new ArrayList<NodeView>();
    for (var nodeView : nodeViews) {
      if (nodeView.overlaps(minSceneCorner, maxSceneCorner))
        list.add(nodeView);
    }
    return list.toArray(new NodeView[list.size()]);
  }

  public void tryConnect(NodePort conn1, NodePort conn2) {
//    System.out.println("tryConnect: " + conn1 + " and " + conn2);
    if (conn1 == null || conn2 == null || conn1 == conn2)
      return;

    if (conn1 instanceof NodeOutput && conn2 instanceof NodeInput)
      connect((NodeOutput) conn1, (NodeInput) conn2);
    else if (conn2 instanceof NodeOutput && conn1 instanceof NodeInput)
      connect((NodeOutput) conn2, (NodeInput) conn1);
  }

  public void connect(NodeOutput source, NodeInput target) {
//    System.out.println("connect " + source + " and " + target);
    if (source.nodeView == target.nodeView)
      return;

//    var oldTargets = getConnections(source);
//    if (!oldTargets.isEmpty()) {
//    }
    source = source.nodeView.addOutput();
//    for (var connection : oldTargets)
//      deleteConnection(connection);
    var oldSources = getConnections(target);
    for (var connection : oldSources)
      deleteConnection(connection);

    // add to view
    connections.add(new Connection(source, target, this));

    // add to data
    var outputs = source.nodeView.getDataObject().getOutputs();
    var targetId = target.nodeView.getDataObject().getId();
    var targetPort = target.getIndex();
    outputs.add(new NodeOutputData(source.nodeView.getDataObject(), targetId, targetPort));
  }

  public void select(boolean ctrl, boolean shift, NodeView... nodes) {
    if (ctrl) {
      for (var node : nodes) {
        if (getSelectedList().contains(node))
          getSelectedList().remove(node);
        else
          getSelectedList().add(node);
      }
    } else {
      if (!shift)
        getSelectedList().clear();
      getSelectedList().addAllUnique(nodes);
    }
  }

  @Override
  public void handleInput() {
    camMouseMove();
    camMouseScroll();
    camera.updateScaledOffset(sceneView.getSize());

    if (script != null)
      return;

    var oldHovered = hovered;
    // update hovered
    Vec2 scenePoint = camera.pixelToScene(input.getMousePos());
    if (hovered == null)
      hovered = getContainer(scenePoint);
    else if (!hovered.contains(scenePoint))
      hovered = null;

    boolean ctrlShift = input.isKeyPressedOR(KeyCode.SHIFT, KeyCode.CONTROL);
    boolean ctrlPressed = input.isKeyPressed(KeyCode.CONTROL);
    boolean shiftPressed = input.isKeyPressed(KeyCode.SHIFT);

    if (input.isMousePressed(InputManager.MOUSE_PRIMARY)) {
      if (dragSelectStart == null && draggedPort == null) { // not currently selecting something
        if (oldHovered != null) { // drag nodes
          if (oldHovered instanceof NodePort) {
            draggedPort = (NodePort) oldHovered;
          }
          if (oldHovered instanceof NodeView && getSelectedList().contains(oldHovered)) {
            for (var selected : getSelectedList()) {
              Vec2 sceneDelta = input.getDeltaMouse().mul(1 / camera.getScale());
              Vec2 newPos = selected.getDataObject().getPos().toVec2().add(sceneDelta);
              selected.getDataObject().setPos(new VectorData(newPos));
            }
            hovered = oldHovered;
          }
        } else {
          if (hovered == null) { // on the background
            if (!ctrlShift)
              getSelectedList().clear();
            dragSelectStart = camera.pixelToScene(input.getMousePos());
          } else { // on the foreground
          }
        }
      }
    }

    if (input.isMouseReleased(InputManager.MOUSE_PRIMARY)) {
      if (dragSelectStart == null) { // not dragging selection
        if (oldHovered != null && getSelectedList().contains(oldHovered)) { // drag nodes, with snapping
          for (var selected : getSelectedList()) {
            var pos = selected.getDataObject().getPos();
            var snappedPos = new VectorData(toGrid(pos.x), toGrid(pos.y));
            selected.getDataObject().setPos(snappedPos);
          }
        }
        if (input.isStillSincePress() && hovered != null && hovered instanceof NodeView) { // select single
          select(ctrlPressed, shiftPressed, (NodeView) hovered);
        }
        if (draggedPort != null) {
          if (hovered != null && hovered instanceof NodePort) {
            tryConnect(draggedPort, (NodePort) hovered); // connection draggedPort
          } else {
            for (var connection : getConnections(draggedPort))
              deleteConnection(connection);
          }
          draggedPort = null;
        }
      } else { // area selection made
        var nodes = getNodesInArea(dragSelectStart, camera.pixelToScene(input.getMousePos()));
        select(ctrlPressed, shiftPressed, nodes);
        dragSelectStart = null;
      }
    }

    if (input.isKeyClicked(KeyCode.DELETE) && !getSelectedList().isEmpty())
      deleteSelectedNodes();

    if (input.isKeyPressedAND(KeyCode.CONTROL, KeyCode.R))
      layoutManager.applyLayout();
    else
      layoutManager.reset();

    if (justCopied && input.isKeyReleasedOR(KeyCode.CONTROL, KeyCode.C)) {
      justCopied = false;
    }
    if (input.isKeyPressedAND(KeyCode.CONTROL, KeyCode.C) && !justCopied) {
      copyToClipboard();
      justCopied = true;
    }

    if (justPasted && input.isKeyReleasedOR(KeyCode.CONTROL, KeyCode.V)) {
      justPasted = false;
    }
    if (input.isKeyPressedAND(KeyCode.CONTROL, KeyCode.V) && !justPasted) {
      pasteFromClipboard();
      justPasted = true;
    }
  }

  private Vec2 toGrid(Vec2 sceneVector) {
    return new Vec2(toGrid(sceneVector.x), toGrid(sceneVector.y));
  }

  private float toGrid(float sceneValue) {
    return MathUtils.round(sceneValue / GRID_SIZE) * GRID_SIZE;
  }

  public Highlightable getHovered() {
    return hovered;
  }

  public PropNotifyList<NodeView> getSelectedList() {
    return selectedProperty.getValue();
  }

  public Property<PropNotifyList<NodeView>> getSelectedProperty() {
    return selectedProperty;
  }

  public ScriptData getDataObject() {
    return dataObject;
  }

  public void setScript(Script script) {
    this.script = script;
  }

}
