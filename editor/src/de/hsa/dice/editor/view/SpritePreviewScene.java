package de.hsa.dice.editor.view;

import de.hsa.dice.core.dataobject.SpriteData;
import de.hsa.dice.core.game.Sprite;
import de.hsa.dice.core.view.CanvasScene;
import de.hsa.dice.core.view.ScenePane;
import javafx.scene.canvas.GraphicsContext;

public class SpritePreviewScene implements CanvasScene {

  private Sprite sprite;

  public SpritePreviewScene(SpriteData dataObject) {
    sprite = new Sprite(dataObject);
  }

  @Override
  public void init(ScenePane sceneView) {
  }

  @Override
  public void render(GraphicsContext gc) {
    sprite.render(gc, (float) gc.getCanvas().getWidth(), (float) gc.getCanvas().getHeight());
  }

  @Override
  public void update() {
    sprite.update();
  }

  @Override
  public void onDestroy() {
  }

}
