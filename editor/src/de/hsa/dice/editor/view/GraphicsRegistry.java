package de.hsa.dice.editor.view;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javafx.scene.image.Image;

public class GraphicsRegistry {

  private static final Logger logger = Logger.getLogger(GraphicsRegistry.class.getName());

  private static GraphicsRegistry instance;

  private Map<String, Image> graphics;

  private GraphicsRegistry() {
    graphics = new HashMap<>();

    load("Abacus", "Abs", "Add", "And", "Battery", "Box", "Brain", "Calculator", "Collision", "ComputerMouse",
        "Console", "Destroy", "Dice", "Div", "DotGraph", "Down", "FallingApple", "FastWheel", "Filter", "Fullscreen",
        "Function", "Graphics", "Grid", "Hourglass", "Invert", "Keyboard", "Left", "Location", "Magnet",
        "MagnifyingGlass", "Mod", "MouseClick", "Mul", "Music", "Not", "Or", "Overlap", "Play", "Pow", "Power",
        "Resize", "Right", "Scales", "Speedometer", "Spinner", "Sqrt", "Sub", "Tiles", "TreasureMap", "Up",
        "VideoCamera", "Volume", "Warning", "Weight", "Xor");
  }

  private void load(String... names) {
    for (var name : names)
      load(name);
  }

  private void load(String name) {
    assert get(name) == null;
    var inputStream = getClass().getClassLoader().getResourceAsStream("view/graphics/" + name + ".png");
    logger.finest("Loading graphic: " + name);
    assert inputStream != null;
    graphics.put(name, new Image(inputStream));
  }

  public Image get(String name) {
    return graphics.get(name);
  }

  public static GraphicsRegistry getInstance() {
    if (instance == null)
      instance = new GraphicsRegistry();
    return instance;
  }

}
