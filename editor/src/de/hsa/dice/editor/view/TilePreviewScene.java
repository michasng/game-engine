package de.hsa.dice.editor.view;

import de.hsa.dice.core.dataobject.TileData;
import de.hsa.dice.core.game.Camera;
import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.game.map.Tile;
import de.hsa.dice.core.view.CanvasScene;
import de.hsa.dice.core.view.ScenePane;
import javafx.scene.canvas.GraphicsContext;

public class TilePreviewScene implements CanvasScene {

  private TileData dataObject;

  private Tile tile;

  private Camera cam;

  public TilePreviewScene(TileData dataObject) {
    this.dataObject = dataObject;
    tile = new Tile(dataObject);
    tile.setShowHitbox(true);
    cam = new Camera(0, 0, 1);
  }

  @Override
  public void init(ScenePane sceneView) {
  }

  @Override
  public void render(GraphicsContext gc) {
    var canvasWidth = (float) gc.getCanvas().getWidth();
    var canvasHeight = (float) gc.getCanvas().getHeight();
    var paddings = dataObject.getPaddings();
    var width = 1 + paddings.width / Game.PIXEL_PER_METER;
    var height = 1 + paddings.height / Game.PIXEL_PER_METER;
    var centerX = 0.5f + paddings.x / Game.PIXEL_PER_METER;
    var centerY = 0.5f + paddings.y / Game.PIXEL_PER_METER;
    cam.scaleToShow(width, height, canvasWidth, canvasHeight);
    cam.focusWorld(centerX, centerY);
    cam.updateScaledOffset(gc.getCanvas());
    cam.doTransform(gc);
    tile.render(gc, 0, 0);
    cam.undoTransform(gc);
  }

  @Override
  public void update() {
    tile.update();
  }

  @Override
  public void onDestroy() {
  }

}
