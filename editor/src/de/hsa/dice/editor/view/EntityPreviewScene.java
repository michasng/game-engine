package de.hsa.dice.editor.view;

import de.hsa.dice.core.dataobject.EntityData;
import de.hsa.dice.core.game.Camera;
import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.game.entity.MockEntity;
import de.hsa.dice.core.view.CanvasScene;
import de.hsa.dice.core.view.ScenePane;
import javafx.scene.canvas.GraphicsContext;

public class EntityPreviewScene implements CanvasScene {

  private EntityData dataObject;

  private MockEntity entity;

  private Camera cam;

  public EntityPreviewScene(EntityData dataObject) {
    this.dataObject = dataObject;
    entity = new MockEntity(dataObject);
    cam = new Camera(0, 0, 1);
  }

  @Override
  public void init(ScenePane sceneView) {
  }

  @Override
  public void render(GraphicsContext gc) {
    var canvasWidth = (float) gc.getCanvas().getWidth();
    var canvasHeight = (float) gc.getCanvas().getHeight();
    var paddings = dataObject.getPaddings();
    var width = dataObject.getWidth() + paddings.width / Game.PIXEL_PER_METER;
    var height = dataObject.getHeight() + paddings.height / Game.PIXEL_PER_METER;
    cam.focus(paddings.x, paddings.y);
    cam.scaleToShow(width, height, canvasWidth, canvasHeight);
    cam.updateScaledOffset(gc.getCanvas());
    cam.doTransform(gc);
    entity.render(gc);
    cam.undoTransform(gc);
  }

  @Override
  public void update() {
    entity.update();
  }

  @Override
  public void onDestroy() {
  }

}
