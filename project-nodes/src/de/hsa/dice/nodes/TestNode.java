package de.hsa.dice.nodes;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Brain", description = "A custom node that does nothing of value.")
public class TestNode extends ScriptNode {

  @Inspect(description = "Whether this node should print something to the console.")
  private boolean active;

  protected boolean run() {
    if (!active)
      return false;

    Console.log("TestNode has executed!");
    return true;
  }

}
