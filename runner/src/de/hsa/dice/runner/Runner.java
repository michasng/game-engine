package de.hsa.dice.runner;

import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.view.CanvasPane;
import de.hsa.dice.core.view.IconRegistry;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Runner extends Application {

  @Override
  public void start(Stage primaryStage) throws Exception {
    StackPane stackPane = new StackPane();
    stackPane.setPrefWidth(640);
    stackPane.setPrefHeight(480);
    Scene scene = new Scene(stackPane);
    primaryStage.setScene(scene);
    primaryStage.getIcons().add(IconRegistry.getInstance().dice);

    primaryStage.setOnCloseRequest(windowEvent -> {
      Platform.exit();
      System.exit(0);
    });
    primaryStage.setTitle("Console Runner");

    primaryStage.show(); // show stage before setting maximized and fullscreen

    var maximizedProp = ProjectData.getCurrent().getMaximizedProperty();
    primaryStage.setMaximized(maximizedProp.getValue());
    maximizedProp.addListener((oldValue, value) -> primaryStage.setMaximized(value));
    primaryStage.maximizedProperty().addListener((obs, oldValue, value) -> maximizedProp.setValue(value, false));

    var fullscreenProp = ProjectData.getCurrent().getFullscreenProperty();
    primaryStage.setFullScreen(fullscreenProp.getValue());
    fullscreenProp.addListener((oldValue, value) -> primaryStage.setFullScreen(value));
    primaryStage.fullScreenProperty().addListener((obs, oldValue, value) -> fullscreenProp.setValue(value, false));

    Game game = new Game();
    CanvasPane canvasPane = new CanvasPane(game, true);
    stackPane.getChildren().add(canvasPane);
  }

  public static void main(String[] args) {
    Application.launch(args);
  }

}
