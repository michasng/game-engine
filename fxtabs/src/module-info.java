module fxtabs {
  exports fxtabs;

  requires javafx.base;
  requires javafx.graphics;
  requires transitive javafx.controls;
}