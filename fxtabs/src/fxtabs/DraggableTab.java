package fxtabs;

import java.util.HashSet;
import java.util.Set;

import javafx.collections.ListChangeListener;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * A draggable tab that can optionally be detached from its tab pane and shown
 * in a separate window. This can be added to any normal TabPane, however a
 * TabPane with draggable tabs must *only* have DraggableTabs, normal tabs and
 * DrragableTabs mixed will cause issues!
 * <p>
 * 
 * @author Michael Berry
 */
public class DraggableTab extends Tab {

  private static final Set<TabPane> tabPanes = new HashSet<>();

  private HBox title;
  private Node graphic;
  private Label nameLabel;

  private Text dragText;
  private Stage dragStage;
  private static final Stage markerStage;

  private boolean detachable;

  static {
    // markerStage displays a little indicator where a tab can be inserted
    markerStage = new Stage();
    markerStage.initStyle(StageStyle.UNDECORATED);
    Rectangle dummy = new Rectangle(3, 10, Color.web("#555555"));
    StackPane markerStack = new StackPane();
    markerStack.getChildren().add(dummy);
    markerStage.setScene(new Scene(markerStack));
  }

  /**
   * Create a new draggable tab. This can be added to any normal TabPane, however
   * a TabPane with draggable tabs must *only* have DraggableTabs, normal tabs and
   * DrragableTabs mixed will cause issues!
   * <p>
   * 
   * @param text the text to appear on the tag label.
   */
  public DraggableTab() {
    nameLabel = new Label("Tab");
    title = new HBox();
    title.setSpacing(4);
    title.getChildren().add(nameLabel);
    setGraphic(title);

    detachable = true;

    // an undecorated stage that shows up, whenever the tab is being dragged
    dragStage = new Stage();
    dragStage.initStyle(StageStyle.UNDECORATED);
    StackPane dragStagePane = new StackPane();
    dragStagePane.setStyle("-fx-background-color:#DDDDDD;");
    dragText = new Text();
    StackPane.setAlignment(dragText, Pos.CENTER);
    dragStagePane.getChildren().add(dragText);
    dragStage.setScene(new Scene(dragStagePane));

    getGraphic().setOnMouseDragged(mouseEvent -> {
      dragStage.setWidth(getGraphicRegion().getWidth() + 10);
      dragStage.setHeight(getGraphicRegion().getHeight() + 10);
      dragStage.setX(mouseEvent.getScreenX());
      dragStage.setY(mouseEvent.getScreenY());
      dragStage.show();
      Point2D screenPoint = new Point2D(mouseEvent.getScreenX(), mouseEvent.getScreenY());
      tabPanes.add(getTabPane()); // only adds if not included
      InsertData data = getInsertData(screenPoint);
      if (data == null || data.getInsertPane().getTabs().isEmpty()) {
        markerStage.hide();
      } else {
        int index = data.getIndex();
        boolean end = false;
        if (index == data.getInsertPane().getTabs().size()) {
          end = true;
          index--;
        }
        Rectangle2D rect = getAbsoluteRect(data.getInsertPane().getTabs().get(index));
        if (end) {
          markerStage.setX(rect.getMaxX() + 13);
        } else {
          markerStage.setX(rect.getMinX());
        }
        markerStage.setY(rect.getMaxY() + 10);
        markerStage.show();
      }
    });

    getGraphic().setOnMouseReleased(mouseEvent -> {
      markerStage.hide();
      dragStage.hide();
      if (!mouseEvent.isStillSincePress()) { // make sure it was not just a tiny movement, but a drag
        Point2D screenPoint = new Point2D(mouseEvent.getScreenX(), mouseEvent.getScreenY());
        moveTo(screenPoint);
      }
    });
  }

  public void moveTo(Point2D screenPoint) {
    InsertData insertData = getInsertData(screenPoint);
    if (insertData != null) {
      insert(insertData); // insert into existing TabPane
      return;
    }

    if (!detachable)
      return; // stay within this TabPane

    detach(screenPoint); // move to a new TabPane
  }

  private void insert(InsertData insertData) {
    TabPane oldTabPane = getTabPane();
    int oldIndex = oldTabPane.getTabs().indexOf(this);
    tabPanes.add(oldTabPane); // only adds if not included
    int addIndex = insertData.getIndex();
    if (oldTabPane == insertData.getInsertPane() && oldTabPane.getTabs().size() == 1) {
      return; // same spot, do nothing
    }
    oldTabPane.getTabs().remove(this);
    if (oldIndex < addIndex && oldTabPane == insertData.getInsertPane())
      addIndex--; // inserted at same TabPane as before, behind current spot -> decrement index
    // index is too high
    if (addIndex > insertData.getInsertPane().getTabs().size())
      addIndex = insertData.getInsertPane().getTabs().size();
    insertData.getInsertPane().getTabs().add(addIndex, this); // add to existing TabPane
    insertData.getInsertPane().selectionModelProperty().get().select(addIndex); // select this tab
  }

  public void detachAtOrigin() {
    var rect = getAbsoluteRect(this);
    var pos = new Point2D(rect.getMinX(), rect.getMinY());
    detach(pos);
  }

  private void detach(Point2D screenPoint) {
    // create a new Stage, Scene and TabPane
    final Stage newStage = new Stage();
    final TabPane newTabPane = new TabPane();
    // no hidden event as UTILITY, when minimized. Workaround: use DECORATED
    newStage.initStyle(StageStyle.DECORATED);
    // determines what tabs can be closed (ALL_TABS, SELECTED_TABS, UNAVAILABLE)
    newTabPane.setTabClosingPolicy(getTabPane().getTabClosingPolicy());
    newStage.setScene(new Scene(newTabPane));
    // use all stylesheets of the previous scene
    var stylesheets = this.getTabPane().getScene().getStylesheets();
    newStage.getScene().getStylesheets().addAll(stylesheets);
    var rootStyle = this.getTabPane().getScene().getRoot().getStyle();
    newTabPane.setStyle(rootStyle);

    newStage.setOnHidden(windowEvent -> {
      tabPanes.remove(newTabPane); // forget about newTabPane
      for (var tab : newTabPane.getTabs()) {
        if (tab.getOnClosed() != null)
          tab.getOnClosed().handle(null);
      }
    });

    tabPanes.add(newTabPane); // only adds if not included (can't be, bc it is new)
    newTabPane.getTabs().addListener((ListChangeListener.Change<? extends Tab> change) -> {
      if (newTabPane.getTabs().isEmpty())
        newStage.hide(); // closes the Stage
    });

    getTabPane().getTabs().remove(this); // remove from current TabPane
    newTabPane.getTabs().add(this); // add to new TabPane

    // show newStage
    newStage.setX(screenPoint.getX());
    newStage.setY(screenPoint.getY());
    newStage.show();
    newTabPane.requestLayout();
    newTabPane.requestFocus();

  }

  /**
   * Set whether it's possible to detach the tab from its pane and move it to
   * another pane or another window. Defaults to true.
   * <p>
   * 
   * @param detachable true if the tab should be detachable, false otherwise.
   */
  public void setDetachable(boolean detachable) {
    this.detachable = detachable;
  }

  /**
   * Set the label text on this draggable tab. This must be used instead of
   * setText() to set the label, otherwise weird side effects will result!
   * <p>
   * 
   * @param text the label text for this tab.
   */
  public void setLabelText(String text) {
    nameLabel.setText(text);
    dragText.setText(text);
  }

  public String getLabelText() {
    return nameLabel.getText();
  }

  /**
   * Set the graphic on this draggable tab. This must be used instead of
   * setGraphic(), otherwise weird side effects will result!
   * <p>
   * 
   * @param graphic the graphic node for this tab.
   */
  public void setTitleGraphic(Node graphic) {
    if (this.graphic != null)
      title.getChildren().remove(this.graphic);
    title.getChildren().add(0, graphic);
    this.graphic = graphic;
  }

  private Region getGraphicRegion() {
    return (Region) getGraphic();
  }

  /**
   * @param screenPoint the point at which the tab is currently hovered
   * @return the tabPane and index in which to insert a tab that is hovered at
   *         screenPoint
   */
  private InsertData getInsertData(Point2D screenPoint) {
    for (TabPane tabPane : tabPanes) {
      Rectangle2D tabAbsolute = getAbsoluteRect(tabPane);
      if (tabAbsolute.contains(screenPoint)) {
        int tabInsertIndex = 0;
        if (!tabPane.getTabs().isEmpty()) {
          Rectangle2D firstTabRect = getAbsoluteRect(tabPane.getTabs().get(0));
          if (firstTabRect.getMaxY() + 60 < screenPoint.getY() || firstTabRect.getMinY() > screenPoint.getY()) {
            return null;
          }
          Rectangle2D lastTabRect = getAbsoluteRect(tabPane.getTabs().get(tabPane.getTabs().size() - 1));
          if (screenPoint.getX() < (firstTabRect.getMinX() + firstTabRect.getWidth() / 2)) {
            tabInsertIndex = 0;
          } else if (screenPoint.getX() > (lastTabRect.getMaxX() - lastTabRect.getWidth() / 2)) {
            tabInsertIndex = tabPane.getTabs().size();
          } else {
            for (int i = 0; i < tabPane.getTabs().size() - 1; i++) {
              Tab leftTab = tabPane.getTabs().get(i);
              Tab rightTab = tabPane.getTabs().get(i + 1);
              if (leftTab instanceof DraggableTab && rightTab instanceof DraggableTab) {
                Rectangle2D leftTabRect = getAbsoluteRect(leftTab);
                Rectangle2D rightTabRect = getAbsoluteRect(rightTab);
                if (betweenX(leftTabRect, rightTabRect, screenPoint.getX())) {
                  tabInsertIndex = i + 1;
                  break;
                }
              }
            }
          }
        }
        return new InsertData(tabInsertIndex, tabPane);
      }
    }
    return null;
  }

  private Rectangle2D getAbsoluteRect(Region region) {
    return new Rectangle2D(
        region.localToScene(region.getLayoutBounds().getMinX(), region.getLayoutBounds().getMinY()).getX()
            + region.getScene().getWindow().getX(),
        region.localToScene(region.getLayoutBounds().getMinY(), region.getLayoutBounds().getMinY()).getY()
            + region.getScene().getWindow().getY(),
        region.getWidth(), region.getHeight());
  }

  private Rectangle2D getAbsoluteRect(Tab tab) {
    Region graphic = getGraphicRegion();
    Rectangle2D rect = getAbsoluteRect(graphic);
    // + 16 because of the x (close button)
    if (tab.isClosable() && tab.getTabPane().getTabClosingPolicy() == TabClosingPolicy.ALL_TABS)
      rect = new Rectangle2D(rect.getMinX(), rect.getMinY(), rect.getWidth() + 16, rect.getHeight());
    return rect;
  }

  private boolean betweenX(Rectangle2D r1, Rectangle2D r2, double xPoint) {
    double lowerBound = r1.getMinX() + r1.getWidth() / 2;
    double upperBound = r2.getMaxX() - r2.getWidth() / 2;
    return xPoint >= lowerBound && xPoint <= upperBound;
  }

  /**
   * A combination of TabPane and index to insert a tab into
   */
  public static class InsertData {

    private final int index;
    private final TabPane insertPane;

    public InsertData(int index, TabPane insertPane) {
      this.index = index;
      this.insertPane = insertPane;
    }

    public int getIndex() {
      return index;
    }

    public TabPane getInsertPane() {
      return insertPane;
    }

  }
}
