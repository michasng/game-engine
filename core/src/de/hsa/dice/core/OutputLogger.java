package de.hsa.dice.core;

public interface OutputLogger {

  void log(String message);

  void logWarning(String warning);

  void logError(String error);

  void logError(Exception e);

}
