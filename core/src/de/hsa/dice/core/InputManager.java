package de.hsa.dice.core;

import java.util.LinkedList;

import org.jbox2d.common.Vec2;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

public class InputManager {

  // the current position of the mouse
  private float mouseX, mouseY;
  private float deltaMouseX, deltaMouseY;
  private int mouseScrollX, mouseScrollY;
  private boolean stillSincePress;

  // storing information about the last mouse click/release or current press
  private boolean[] mouseClicked, mousePressed, mouseReleased;

  // a list of all keys that are currently being mousePressed
  private LinkedList<KeyCode> clickedKeys;
  private LinkedList<KeyCode> pressedKeys;
  private LinkedList<KeyCode> releasedKeys;

  public static final int MOUSE_PRIMARY = 0, MOUSE_MIDDLE = 1, MOUSE_SECONDARY = 2, MOUSE_OTHER = 3;

  public InputManager(Node node) {
    mouseClicked = new boolean[4];
    mousePressed = new boolean[4];
    mouseReleased = new boolean[4];

    clickedKeys = new LinkedList<>();
    pressedKeys = new LinkedList<>();
    releasedKeys = new LinkedList<>();

    node.setOnKeyPressed(keyEvent -> {
      if (!pressedKeys.contains(keyEvent.getCode())) {
        pressedKeys.add(keyEvent.getCode());
        clickedKeys.add(keyEvent.getCode());
      }
      keyEvent.consume(); // prevent focus traversal
    });

    node.setOnKeyReleased(keyEvent -> {
      if (!releasedKeys.contains(keyEvent.getCode()))
        releasedKeys.add(keyEvent.getCode());
      pressedKeys.remove(keyEvent.getCode());
    });

    node.setOnMousePressed(mouseEvent -> {
      int index = getIndexFromMouseEvent(mouseEvent);
      mouseClicked[index] = mousePressed[index] = true;
      node.requestFocus();
      mouseEvent.consume();
    });

    node.setOnMouseReleased(mouseEvent -> {
      int index = getIndexFromMouseEvent(mouseEvent);
      mouseReleased[index] = true;
      mousePressed[index] = false;
      stillSincePress = mouseEvent.isStillSincePress();
    });

    EventHandler<MouseEvent> mouseMoveHandler = (mouseEvent -> {
      // += because movement might happen multiple times in one update cycle
      deltaMouseX += (float) mouseEvent.getX() - mouseX;
      deltaMouseY += (float) mouseEvent.getY() - mouseY;
      mouseX = (float) (mouseEvent.getX());
      mouseY = (float) (mouseEvent.getY());
    });
    node.setOnMouseMoved(mouseMoveHandler);
    // also record position when mouse is pressed
    node.setOnMouseDragged(mouseMoveHandler);

    node.setOnScroll((scrollEvent -> {
      mouseScrollX += scrollEvent.getDeltaX();
      mouseScrollY += scrollEvent.getDeltaY();
    }));
  }

  // ---------- mouse handling ----------

  private int getIndexFromMouseEvent(MouseEvent mouseEvent) {
    if (mouseEvent.getButton() == MouseButton.PRIMARY)
      return MOUSE_PRIMARY;
    else if (mouseEvent.getButton() == MouseButton.SECONDARY)
      return MOUSE_SECONDARY;
    else if (mouseEvent.getButton() == MouseButton.MIDDLE)
      return MOUSE_MIDDLE;
    return MOUSE_OTHER;
  }

  public Vec2 getMousePos() {
    return new Vec2(mouseX, mouseY);
  }

  public float getMouseX() {
    return mouseX;
  }

  public float getMouseY() {
    return mouseY;
  }

  public Vec2 getDeltaMouse() {
    return new Vec2(deltaMouseX, deltaMouseY);
  }

  public float getDeltaMouseX() {
    return deltaMouseX;
  }

  public float getDeltaMouseY() {
    return deltaMouseY;
  }

  public boolean isMouseMoving() {
    return (deltaMouseX != 0 || deltaMouseY != 0);
  }

  public boolean isMouseScrolledX() {
    return mouseScrollX != 0;
  }

  public int getMouseScrollX() {
    return mouseScrollX;
  }

  public boolean isMouseScrolledY() {
    return mouseScrollY != 0;
  }

  public int getMouseScrollY() {
    return mouseScrollY;
  }

  public boolean isMouseClicked() {
    for (boolean mouseClicked : mouseClicked)
      if (mouseClicked)
        return true;
    return false;
  }

  public boolean isMouseClicked(int buttonIndex) {
    return mouseClicked[buttonIndex];
  }

  public boolean isMousePressed() {
    for (boolean mousePressed : mousePressed)
      if (mousePressed)
        return true;
    return false;
  }

  public boolean isMousePressed(int buttonIndex) {
    return mousePressed[buttonIndex];
  }

  public boolean isMouseReleased() {
    for (boolean mouseReleased : mouseReleased)
      if (mouseReleased)
        return true;
    return false;
  }

  public boolean isMouseReleased(int buttonIndex) {
    return mouseReleased[buttonIndex];
  }

  public boolean isStillSincePress() {
    return stillSincePress;
  }

  public void reset() {
    for (int i = 0; i < mouseClicked.length; i++)
      mouseClicked[i] = mouseReleased[i] = false;
    mouseScrollX = mouseScrollY = 0; // different Syntax for LinkedList, bc they are pointers, not value-types
    clickedKeys = new LinkedList<>();
    releasedKeys = new LinkedList<>();
    deltaMouseX = 0;
    deltaMouseY = 0;
  }

  // ---------- key handling ----------

  public boolean isKeyClicked(KeyCode keyCode) {
    return clickedKeys.contains(keyCode);
  }

  public boolean isKeyPressed(KeyCode keyCode) {
    return pressedKeys.contains(keyCode);
  }

  public boolean isKeyPressedOR(KeyCode... keyCodes) {
    for (KeyCode keyCode : keyCodes)
      if (isKeyPressed(keyCode))
        return true;
    return false;
  }

  public boolean isKeyPressedAND(KeyCode... keyCodes) {
    for (KeyCode keyCode : keyCodes)
      if (!isKeyPressed(keyCode))
        return false;
    return true;
  }

  public boolean isKeyReleased(KeyCode keyCode) {
    return releasedKeys.contains(keyCode);
  }

  public boolean isKeyReleasedOR(KeyCode... keyCodes) {
    for (KeyCode keyCode : keyCodes)
      if (isKeyReleased(keyCode))
        return true;
    return false;
  }

  public boolean isKeyReleasedAND(KeyCode... keyCodes) {
    for (KeyCode keyCode : keyCodes)
      if (!isKeyReleased(keyCode))
        return false;
    return true;
  }

  public boolean isAnyKeyPressed() {
    return !pressedKeys.isEmpty();
  }

  public boolean isAnyKeyReleased() {
    return !releasedKeys.isEmpty();
  }

  public boolean isAnyKeyClicked() {
    return !clickedKeys.isEmpty();
  }

}
