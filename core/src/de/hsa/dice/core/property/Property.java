package de.hsa.dice.core.property;

import java.util.LinkedList;
import java.util.logging.Logger;

public class Property<T> {

  private static final Logger logger = Logger.getLogger(Property.class.getName());

  private String name;

  private T value;

  private LinkedList<PropertyListener<T>> listeners;

  /**
   * Prevents ConcurrentModificationExceptions. Those would normally go unnoticed
   * until the iteration proceeds.
   */
  private boolean iterating;
  private LinkedList<PropertyListener<T>> listenersToAdd;
  private LinkedList<PropertyListener<T>> listenersToRemove;

  public Property(String name, T value) {
    this.name = name;
    this.value = value;
    listeners = new LinkedList<>();
    listenersToAdd = new LinkedList<>();
    listenersToRemove = new LinkedList<>();
  }

  public PropertySubscription<T> addListener(PropertyListener<T> listener) {
    String valueStr = value == null ? "null" : value.toString();
    logger.fine("Adding listener to " + name + ", total: " + (listeners.size() + 1) + ": " + valueStr);
    if (iterating)
      listenersToAdd.add(listener);
    else
      listeners.add(listener);
    return new PropertySubscription<T>(this, listener);
  }

  public void removeListener(PropertyListener<T> listener) {
    String valueStr = value == null ? "null" : value.toString();
    logger.fine("Removing listener from " + name + ", total: " + (listeners.size() - 1) + ": " + valueStr);
    if (iterating)
      listenersToRemove.add(listener);
    else
      listeners.remove(listener);
  }

  public void clearListeners() {
    logger.fine("Clearing all listener from " + name + ", total: " + (listeners.size() - 1));
    if (iterating)
      listenersToRemove.addAll(listeners);
    else
      listeners.clear();
  }

  public T getValue() {
    return value;
  }

  public void setValue(T value) {
    T oldValue = this.value;
    // TODO: adjust tiles in map and potentially other things to make this viable
//    if (value.equals(oldValue)) { // only notify if value actually changed
//      System.out.println("set " + name + " from " + oldValue + " to " + value);
//      return;
//    }
    this.value = value;
    logger.fine("Changed value of " + name + " from " + oldValue + " to " + value);
    try {
      notifyListeners(oldValue);
    } catch (InvalidPropertyException e) {
      this.value = oldValue;
      throw e;
    }
  }

  public void setValue(T value, boolean notify) {
    if (notify) {
      setValue(value);
    } else {
      this.value = value;
    }
  }

  private void notifyListeners(T oldValue) {
    iterating = true;
    for (var listener : listeners)
      listener.onValueChanged(oldValue, value);
    iterating = false;

    for (var listener : listenersToRemove)
      listeners.remove(listener);
    listenersToRemove.clear();

    for (var listener : listenersToAdd)
      listeners.add(listener);
    listenersToAdd.clear();

  }

  public LinkedList<PropertyListener<T>> getListeners() {
    return listeners;
  }

  public String getName() {
    return name;
  }

}
