package de.hsa.dice.core.property;

public interface PropertyListener<T> {

  void onValueChanged(T oldValue, T value);
  
}
