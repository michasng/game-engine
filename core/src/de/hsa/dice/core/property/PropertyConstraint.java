package de.hsa.dice.core.property;

public interface PropertyConstraint<T> {
	
	boolean isValid(T oldValue, T value);

}
