package de.hsa.dice.core.property;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class PropNotifyList<T> implements List<T> {

  private Property<PropNotifyList<T>> property;

  private ArrayList<T> list;

  private boolean notifyEnabled;

  public PropNotifyList(Property<PropNotifyList<T>> property) {
    this.property = property;
    list = new ArrayList<>();
    notifyEnabled = true;
  }

  public void setNotifyEnabled(boolean notifyEnabled) {
    this.notifyEnabled = notifyEnabled;
  }

  public boolean getNotifyEnabled() {
    return notifyEnabled;
  }

  public void notifyListeners() {
    if (notifyEnabled)
      property.setValue(this, true);
  }

  public List<T> getImpl() {
    return list;
  }

  public T getFirst() {
    return list.get(0);
  }

  public T getLast() {
    if (list.isEmpty())
      return null;
    return list.get(list.size() - 1);
  }

  public void addUnique(T t) {
    if (!list.contains(t))
      list.add(t);
    notifyListeners();
  }

  @SuppressWarnings("unchecked")
  public void addAllUnique(T... ts) {
    for (var t : ts)
      if (!list.contains(t))
        list.add(t);
    notifyListeners();
  }

  @Override
  public boolean add(T arg0) {
    boolean result = list.add(arg0);
    notifyListeners();
    return result;
  }

  @Override
  public void add(int arg0, T arg1) {
    list.add(arg0, arg1);
    notifyListeners();
  }

  @Override
  public boolean addAll(Collection<? extends T> arg0) {
    boolean result = list.addAll(arg0);
    notifyListeners();
    return result;
  }

  @Override
  public boolean addAll(int arg0, Collection<? extends T> arg1) {
    boolean result = list.addAll(arg0, arg1);
    notifyListeners();
    return result;
  }

  @Override
  public void clear() {
    list.clear();
    notifyListeners();
  }

  @Override
  public boolean contains(Object arg0) {
    return list.contains(arg0);
  }

  @Override
  public boolean containsAll(Collection<?> arg0) {
    return list.containsAll(arg0);
  }

  @Override
  public T get(int arg0) {
    return list.get(arg0);
  }

  @Override
  public int indexOf(Object arg0) {
    return list.indexOf(arg0);
  }

  @Override
  public boolean isEmpty() {
    return list.isEmpty();
  }

  @Override
  public Iterator<T> iterator() {
    return list.iterator();
  }

  @Override
  public int lastIndexOf(Object arg0) {
    return list.lastIndexOf(arg0);
  }

  @Override
  public ListIterator<T> listIterator() {
    return list.listIterator();
  }

  @Override
  public ListIterator<T> listIterator(int arg0) {
    return list.listIterator(arg0);
  }

  @Override
  public boolean remove(Object arg0) {
    boolean result = list.remove(arg0);
    notifyListeners();
    return result;
  }

  @Override
  public T remove(int arg0) {
    T result = list.remove(arg0);
    notifyListeners();
    return result;
  }

  @Override
  public boolean removeAll(Collection<?> arg0) {
    boolean result = list.removeAll(arg0);
    notifyListeners();
    return result;
  }

  @Override
  public boolean retainAll(Collection<?> arg0) {
    boolean result = list.retainAll(arg0);
    notifyListeners();
    return result;
  }

  @Override
  public T set(int arg0, T arg1) {
    T result = list.set(arg0, arg1);
    notifyListeners();
    return result;
  }

  @Override
  public int size() {
    return list.size();
  }

  @Override
  public List<T> subList(int arg0, int arg1) {
    return list.subList(arg0, arg1);
  }

  @Override
  public Object[] toArray() {
    return list.toArray();
  }

  @Override
  public <T2> T2[] toArray(T2[] arg0) {
    return list.toArray(arg0);
  }

  @Override
  public String toString() {
    return list.toString();
  }

}
