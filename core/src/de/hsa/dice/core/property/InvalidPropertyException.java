package de.hsa.dice.core.property;

public class InvalidPropertyException extends RuntimeException {
  
  private static final long serialVersionUID = 1L;

  public InvalidPropertyException() {
    super();
  }
  
  public InvalidPropertyException(String message) {
    super(message);
  }
  
}
