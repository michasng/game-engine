package de.hsa.dice.core.property;

public class PropertySubscription<T> {

  private Property<T> property;
  
  private PropertyListener<T> listener;
  
  public PropertySubscription(Property<T> property, PropertyListener<T> listener) {
    this.property = property;
    this.listener = listener;
  }
  
  public void cancelSubscription() {
    property.removeListener(listener);
  }
  
  public Property<T> getProperty() {
  	return property;
  }
  
  public PropertyListener<T> getListener() {
  	return listener;
  }
  
}
