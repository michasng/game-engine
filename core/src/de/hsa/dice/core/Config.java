package de.hsa.dice.core;

import java.io.File;
import java.util.Properties;

import de.hsa.dice.core.util.io.FileOperator;

public class Config {

  private static final String CONFIG_FILENAME = "config.properties";

  // prop "user.dir" returns working directory, "user.home" returns user directory
  public static final String BASE_DIR = System.getProperty("user.home") + File.separatorChar + "documents"
      + File.separatorChar + "game-engine" + File.separatorChar;

  // property-keys
  public static final String CURRENT_PROJECT_NAME = "currentProjectName";
  public static final String CURRENT_PROJECT_PATH = "currentProjectPath"; // path to the .dice file
  public static final String PROJECT_PATH_HISTORY = "projectPathHistory";

  private Properties props;

  private static Config instance;

  public static Config getInstance() {
    if (instance == null)
      instance = new Config();
    return instance;
  }

  private Config() {
    loadProps();

    String currentProjectPath = getString(CURRENT_PROJECT_PATH);
    if (currentProjectPath == null) {
      String defaultProjectName = "myProject";
      currentProjectPath = BASE_DIR + defaultProjectName + File.separatorChar + defaultProjectName + ".dice";
      setProject(currentProjectPath);
    }

    save();
  }

  public void setProject(String projectFilePath) {
    var projectDirPath = new File(projectFilePath).getParent() + File.separatorChar;
    setString(CURRENT_PROJECT_PATH, projectDirPath);
    String projectName = new File(projectDirPath).getName();
    setString(CURRENT_PROJECT_NAME, projectName);

    var historyStr = getString(PROJECT_PATH_HISTORY);
    for (var historyItem : getProjectHistory())
      if (historyItem.equals(projectFilePath))
        return;
    if (!historyStr.isEmpty())
      historyStr += ",";
    historyStr += projectFilePath;
    setString(PROJECT_PATH_HISTORY, historyStr);
  }

  public String[] getProjectHistory() {
    var historyStr = getString(PROJECT_PATH_HISTORY);
    if (historyStr.isEmpty())
      return new String[0];
    // would return empty String[1] on empty String
    return historyStr.split(",");
  }

  private void loadProps() {
    props = FileOperator.readProps(BASE_DIR + CONFIG_FILENAME);
    if (props == null)
      loadDefaultProps();
  }

  private void loadDefaultProps() {
    props = FileOperator.readPropsClasspath(CONFIG_FILENAME);
  }

  public void save() {
    FileOperator.writeProps(BASE_DIR + CONFIG_FILENAME, props);
  }

  public void setString(String key, String value) {
    this.props.put(key, value);
  }

  public void setInt(String key, String value) {
    this.props.put(key, value);
  }

  public void setFloat(String key, float value) {
    this.props.put(key, value);
  }

  public void setBool(String key, boolean value) {
    this.props.put(key, value);
  }

  public String getString(String key) {
    return props.getProperty(key);
  }

  public int getInt(String key) {
    return getInt(key, -1);
  }

  public int getInt(String key, int fallback) {
    String value = props.getProperty(key);
    try {
      return Integer.parseInt(value);
    } catch (NumberFormatException e) {
      return fallback;
    }
  }

  public float getFloat(String key) {
    return getFloat(key, -1f);
  }

  public float getFloat(String key, float fallback) {
    String value = props.getProperty(key);
    try {
      return Float.parseFloat(value);
    } catch (NumberFormatException e) {
      return fallback;
    }
  }

  public boolean getBoolean(String key) {
    String value = props.getProperty(key);
    if (value != null && value.toLowerCase().equals("true"))
      return true;
    return false;
  }

}
