package de.hsa.dice.core.view;

import javafx.scene.canvas.GraphicsContext;

public interface Renderable {

	void render(GraphicsContext gc);
	
}
