package de.hsa.dice.core.view;

public interface CanvasScene extends Renderable {

  void init(ScenePane sceneView);
  
  void update();
  
  void onDestroy();
  
}
