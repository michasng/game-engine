package de.hsa.dice.core.view;

import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

public class FXMLUtil {

  public static <T extends Node, S> FxNodeContainer<T, S> load(String pathOnClasspath) {
    URL location = FXMLUtil.class.getClassLoader().getResource(pathOnClasspath);
    FXMLLoader fxmlLoader = new FXMLLoader(location);
    T node;
    S controller;
    try {
      node = fxmlLoader.load();
      controller = fxmlLoader.getController();
      return new FxNodeContainer<>(node, controller);
    } catch (IOException e) {
      e.printStackTrace();
      throw new Error("Error loading fxml: " + pathOnClasspath);
    }
  }

  public static class FxNodeContainer<T extends Node, S> {

    public final T node;
    public final S controller;

    FxNodeContainer(T node, S controller) {
      this.node = node;
      this.controller = controller;
      node.setUserData(controller);
    }

  }

}
