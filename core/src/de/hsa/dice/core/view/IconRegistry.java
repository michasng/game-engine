package de.hsa.dice.core.view;

import de.hsa.dice.core.dataobject.DataType;
import javafx.scene.image.Image;

public class IconRegistry {

  private static IconRegistry instance;

  public final Image dice, play, folder, remove, toggleOn, toggleOff, sprites, tiles, maps, entities, sounds, scripts;

  private IconRegistry() {
    dice = load("dice");
    play = load("play32");
    folder = load("folder64");
    remove = load("remove");
    toggleOn = load("toggle_on");
    toggleOff = load("toggle_off");

    sprites = load("sprites");
    tiles = load("tiles");
    maps = load("maps");
    entities = load("entities");
    sounds = load("sounds");
    scripts = load("scripts");
  }

  public Image load(String iconName) {
    return new Image(getClass().getClassLoader().getResourceAsStream("view/icons/" + iconName + ".png"));
  }

  public Image fromDataType(DataType dataType) {
    switch (dataType) {
    case SPRITE:
      return sprites;
    case TILE:
      return tiles;
    case MAP:
      return maps;
    case ENTITY:
      return entities;
    case SOUND:
      return sounds;
    case SCRIPT:
      return scripts;
    default:
      return null;
    }
  }

  public static IconRegistry getInstance() {
    if (instance == null)
      instance = new IconRegistry();
    return instance;
  }

}
