package de.hsa.dice.core.view;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.InputManager;

public interface ScenePane {
  
  public double getWidth();
  
  public double getHeight();
  
  public Vec2 getSize();
  
  public InputManager getInputManager();

}
