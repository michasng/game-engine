package de.hsa.dice.core.view;

import java.util.logging.Logger;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.InputManager;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;

public class CanvasPane extends Pane implements Runnable, ScenePane {

  private static final Logger logger = Logger.getLogger(CanvasPane.class.getName());

  private CanvasScene canvasScene;
  private InputManager input;
  private boolean sceneInitialized;

  private boolean runRenderLoop;

  private Canvas canvas;

  // is a member variable, because local variables would have be final
  private ChangeListener<Scene> sceneChangeListener;

  public CanvasPane(CanvasScene canvasScene, boolean runRenderLoop) {
    this(canvasScene, runRenderLoop, true);
  }

  public CanvasPane(CanvasScene canvasScene, boolean runRenderLoop, boolean doRequestFocus) {
    this.canvasScene = canvasScene;
    this.runRenderLoop = runRenderLoop;

    this.canvas = new Canvas();
    this.getChildren().add(canvas);

    setStyle("-fx-background-color: black;");

    logger.fine("Initializing InputManager");
    input = new InputManager(this);

    // scene is null right now, so we wait until it's attached
    sceneChangeListener = (obs, oldScene, scene) -> {
      if (doRequestFocus)
        requestFocus();
      // only run once and allow garbage collection
      sceneProperty().removeListener(sceneChangeListener);
    };
    sceneProperty().addListener(sceneChangeListener);
  }

  private void initCanvasScene() {
    logger.finest("Initialize CanvasScene.");
    canvasScene.init(this);
    sceneInitialized = true;
  }

  @Override
  protected void layoutChildren() {
    logger.finest("layoutChildren in CanvasPane.");
    double parentWidth = getWidth();
    double parentHeight = getHeight();

    // initCanvasScene in layoutChildren, to make sure the dimensions are > 0
    if (!sceneInitialized && parentWidth != 0) {
      initCanvasScene();

      if (runRenderLoop)
        renderLoop();
    }

    if (parentWidth != canvas.getWidth() || parentHeight != canvas.getHeight()) {
      canvas.setWidth(parentWidth);
      canvas.setHeight(parentHeight);

      if (!runRenderLoop)
        render();
    }

  }

  public void stop() {
    logger.finest("Stop render loop for CanvasScene.");
    this.runRenderLoop = false;
    canvasScene.onDestroy();
  }

  @Override
  public void run() {
    logger.finest("Run render loop for CanvasScene.");

    long lastFrame = System.nanoTime();

    // int frames = 0;

    double nsPerFrame = 1000000000D / 30;

    double deltaFrame = 0;
    // long lastTimer = System.currentTimeMillis();
    try {
      while (runRenderLoop) {
        long now = System.nanoTime();

        deltaFrame += (now - lastFrame) / nsPerFrame;
        lastFrame = now;
        if (deltaFrame >= 1) {
          // frames++;
          Platform.runLater(() -> {
            if (runRenderLoop) { // TODO: don't render when the system can't keep up
              canvasScene.update(); // TODO: separate update and render loop
              render();
              input.reset();
            }
          });
          deltaFrame -= 1;
        }

        try {
          Thread.sleep(2);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }

        /*
         * if (System.currentTimeMillis() - lastTimer >= 1000) { lastTimer += 1000;
         * System.out.println("fps: " + frames); frames = 0; }
         */

      }
    } catch (Exception e) {
      e.printStackTrace();
      Platform.exit();
      System.exit(1);
    }

  }

  private void renderLoop() {
    logger.fine("Starting render loop.");
    new Thread(this).start();
  }

  private void render() {
    GraphicsContext gc = canvas.getGraphicsContext2D();

    gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

    canvasScene.render(gc);
  }

  @Override
  public Vec2 getSize() {
    return new Vec2((float) getWidth(), (float) getHeight());
  }

  @Override
  public InputManager getInputManager() {
    return input;
  }

}