package de.hsa.dice.core.game.entity;

import java.util.HashMap;
import java.util.LinkedList;

import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.Fixture;
import org.jbox2d.dynamics.contacts.Contact;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.dataobject.ScriptData;
import de.hsa.dice.core.dataobject.nested.MapEntityData;
import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.game.map.TileMap;
import de.hsa.dice.core.script.Script;

public class Entity extends AbstractEntity {

  private MapEntityData mapEntityData;

  private Body body;
  private LinkedList<EntityContact> contacts;

  private LinkedList<Script> scripts;

  private HashMap<String, Object> variables;

  public Entity(EntityType type, MapEntityData mapEntityData, TileMap map) {
    super(type);
    this.mapEntityData = mapEntityData;

    body = map.getWorld().createBody(type.getBodyDef());
    body.createFixture(type.getFixtureDef());

    float x = mapEntityData.getPos().x;
    float y = mapEntityData.getPos().y;
    body.setTransform(new Vec2(x, y), body.getAngle());

    body.setUserData(this);

    contacts = new LinkedList<>();

    scripts = new LinkedList<>();
    for (var scriptData : type.getDataObject().getScriptDatas())
      scripts.add(new Script(scriptData, this));

    variables = new HashMap<>();
  }

  @Override
  public void update() {
    super.update();
    for (Script script : scripts)
      script.update();
  }

  public void beginContact(Contact contact, Entity other) {
    contacts.add(new EntityContact(contact, other));
  }

  public void endContact(Contact contact, Entity other) {
    EntityContact toRemove = null;
    for (var entityContact : contacts)
      if (entityContact.contact == contact)
        toRemove = entityContact;
    contacts.remove(toRemove);
  }

  @Override
  public Vec2 getPosition() {
    return body.getPosition();
  }

  @Override
  public void setPosition(Vec2 pos) {
    body.setTransform(pos, body.getAngle());
  }

  @Override
  public float getRotation() {
    return body.getAngle() * MathUtils.RAD2DEG;
  }

  @Override
  public void setRotation(float degrees) {
    body.setTransform(body.getPosition(), MathUtils.DEG2RAD * degrees);
  }

  public Vec2 getLinearVel() {
    return body.getLinearVelocity();
  }

  public void setLinearVel(Vec2 vel) {
    body.setLinearVelocity(vel);
  }

  public float getAngularVel() {
    return body.getAngularVelocity() * MathUtils.RAD2DEG;
  }

  public void setAngularVel(float angVel) {
    body.setAngularVelocity(MathUtils.DEG2RAD * angVel);
  }

  public void applyForce(Vec2 force) {
    body.applyForceToCenter(force);
  }

  public void applyAngularForce(float deltaAngularVel) { // change per tick/call, not per second
    body.setAngularVelocity(body.getAngularVelocity() + (MathUtils.DEG2RAD * deltaAngularVel));
  }

  public void setRotationLocked(boolean rotationLocked) {
    body.setFixedRotation(rotationLocked);
  }

  public boolean isRotationLocked() {
    return body.isFixedRotation();
  }

  public void setDensity(float density) {
    body.getFixtureList().setDensity(density);
  }

  public float getDensity() {
    return body.getFixtureList().getDensity();
  }

  public void setFriction(float friction) {
    body.getFixtureList().setFriction(friction);
  }

  public float getFriction() {
    return body.getFixtureList().getFriction();
  }

  public void setElasticity(float elasticity) {
    body.getFixtureList().setRestitution(elasticity);
  }

  public float getElasticity() {
    return body.getFixtureList().getRestitution();
  }

  public boolean contains(Vec2 point) {
    Fixture f = body.getFixtureList();
    return f.testPoint(point);
  }

  public Body getBody() {
    return body;
  }

  public String getName() {
    return getType().getDataObject().getName();
  }

  public boolean hasName(String name) {
    return getName().equals(name);
  }

  public void addScript(ScriptData scriptData) {
    Game.getInstance().addToPostUpdate(() -> {
      if (hasScript(scriptData.getName()))
        Console.logWarning("Entity " + getMapEntityData().getName() + " already has Script: " + scriptData.getName());
      scripts.add(new Script(scriptData, this));
    });
  }

  public void removeScript(String scriptName) {
    Game.getInstance().addToPostUpdate(() -> {
      var script = findScript(scriptName);
      if (script == null)
        Console.logWarning("Entity " + getMapEntityData().getName() + " has no Script: " + scriptName);
      scripts.remove(script);
    });
  }

  public boolean hasScript(String scriptName) {
    return findScript(scriptName) != null;
  }

  public Script findScript(String scriptName) {
    for (var script : scripts)
      if (script.getDataObject().getName().equals(scriptName))
        return script;
    return null;
  }

  public LinkedList<Script> getScripts() {
    return scripts;
  }

  public Object getVariable(String name) {
    return variables.get(name);
  }

  public void setVariable(String name, Object value) {
    assert name != null;
    assert value != null;
    variables.put(name, value);
  }

  public MapEntityData getMapEntityData() {
    return mapEntityData;
  }

  public boolean hasContacts() {
    return (contacts.size() > 0);
  }

  public LinkedList<EntityContact> getContacts() {
    return contacts;
  }

  public static class EntityContact {
    public final Contact contact;
    public final Entity other;

    private EntityContact(Contact contact, Entity other) {
      this.contact = contact;
      this.other = other;
    }
  }

}
