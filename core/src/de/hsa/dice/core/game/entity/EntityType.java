package de.hsa.dice.core.game.entity;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.MathUtils;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.FixtureDef;

import de.hsa.dice.core.dataobject.EntityData;
import de.hsa.dice.core.dataobject.EntityData.GraphicsMode;
import javafx.scene.paint.Color;

public class EntityType {

  private EntityData dataObject;

  private Color color;

  private GraphicsMode graphicsMode;

  private BodyDef bodyDef;
  private PolygonShape shape;
  private FixtureDef fixtureDef;

  public EntityType(EntityData dataObject) {
    this.dataObject = dataObject;

    color = Color.web(dataObject.getColor());
    graphicsMode = dataObject.getGraphicsMode();

    bodyDef = new BodyDef();
    bodyDef.type = dataObject.getMobility(); // DYNAMIC, KINEMATIC, STATIC
    bodyDef.angle = dataObject.getRotation() * MathUtils.DEG2RAD;
    bodyDef.fixedRotation = dataObject.isRotationLocked();

    shape = new PolygonShape();
    shape.setAsBox(getWidth() / 2, getHeight() / 2); // half size

    fixtureDef = new FixtureDef();
    fixtureDef.density = dataObject.getDensity();
    fixtureDef.friction = dataObject.getFriction();
    fixtureDef.restitution = dataObject.getElasticity();
    fixtureDef.shape = shape;
  }

  public Color getColor() {
    return color;
  }

  public GraphicsMode getGraphicsMode() {
    return graphicsMode;
  }

  public float getWidth() {
    return dataObject.getWidth();
  }

  public float getHeight() {
    return dataObject.getHeight();
  }

  public BodyDef getBodyDef() {
    return bodyDef;
  }

  public PolygonShape getShape() {
    return shape;
  }

  public FixtureDef getFixtureDef() {
    return fixtureDef;
  }

  public EntityData getDataObject() {
    return dataObject;
  }

}
