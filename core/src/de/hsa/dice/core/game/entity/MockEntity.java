package de.hsa.dice.core.game.entity;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.dataobject.EntityData;

public class MockEntity extends AbstractEntity {

  protected Vec2 pos;
  protected float degrees;

  public MockEntity(EntityType type) {
    super(type);

    pos = new Vec2(0, 0);
    degrees = type.getDataObject().getRotation();
    setShowHitbox(true);
  }

  public MockEntity(EntityData dataObject) {
    this(new EntityType(dataObject));
  }

  @Override
  public void setPosition(Vec2 pos) {
    this.pos = pos;
  }

  @Override
  public Vec2 getPosition() {
    return pos;
  }

  @Override
  public float getRotation() {
    return degrees;
  }

  @Override
  public void setRotation(float degrees) {
    this.degrees = degrees;
  }

}
