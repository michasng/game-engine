package de.hsa.dice.core.game.entity;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.EntityData;
import de.hsa.dice.core.dataobject.EntityData.GraphicsMode;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.SpriteData;
import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.game.Sprite;
import de.hsa.dice.core.view.Renderable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public abstract class AbstractEntity implements Renderable {

  protected Sprite sprite;

  protected boolean showHitbox;
  protected Color hitboxColor;

  protected EntityType type;

  protected AbstractEntity(EntityType type) {
    this.type = type;

    var spriteMgr = ProjectData.getCurrent().getSubManager(DataType.SPRITE, SpriteData.class);
    var spriteData = spriteMgr.get(type.getDataObject().getSprite());
    if (spriteData != null)
      sprite = new Sprite(spriteData);

    hitboxColor = Color.WHITE;
  }

  public void update() {
    if (type.getGraphicsMode() == GraphicsMode.SPRITE && sprite != null)
      sprite.update();
  }

  public void render(GraphicsContext gc) {
    var rotationDegree = getRotation();
    var pixelCenterPos = getPosition().mul(Game.PIXEL_PER_METER);
    float pixelWidth = type.getWidth() * Game.PIXEL_PER_METER;
    float pixelHeight = type.getHeight() * Game.PIXEL_PER_METER;

    var paddings = type.getDataObject().getPaddings();
    var renderWidth = pixelWidth + paddings.width;
    var renderHeight = pixelHeight + paddings.height;

    gc.translate(pixelCenterPos.x, pixelCenterPos.y); // translate to the center
    gc.rotate(rotationDegree); // rotate around the center
    gc.translate(-renderWidth / 2 + paddings.x, -renderHeight / 2 + paddings.y); // translate to the upper-left corner

    if (type.getGraphicsMode() == GraphicsMode.COLOR) {
      gc.setFill(type.getColor());
      gc.fillRect(0, 0, renderWidth, renderHeight);
    } else if (type.getGraphicsMode() == GraphicsMode.SPRITE && sprite != null) {
      sprite.render(gc, renderWidth, renderHeight);
    }
    if (showHitbox) {
      gc.setStroke(hitboxColor);
      gc.setLineWidth(2);
      // subtract the paddings to render just the hitbox
      gc.strokeRect(-paddings.x + paddings.width / 2, -paddings.y + paddings.height / 2, renderWidth - paddings.width,
          renderHeight - paddings.height);
    }

    gc.translate(renderWidth / 2 - paddings.x, renderHeight / 2 - paddings.y);
    gc.rotate(-rotationDegree);
    gc.translate(-pixelCenterPos.x, -pixelCenterPos.y);
  }

  public void setShowHitbox(boolean showHitbox) {
    this.showHitbox = showHitbox;
  }

  public boolean isShowHitbox() {
    return showHitbox;
  }

  public void setHitboxColor(Color hitboxColor) {
    this.hitboxColor = hitboxColor;
  }

  public Color getHitboxColor() {
    return hitboxColor;
  }

  public abstract Vec2 getPosition();

  public abstract void setPosition(Vec2 pos);

  public abstract float getRotation();

  public abstract void setRotation(float degrees);

  public GraphicsMode getGraphicsMode() {
    return type.getGraphicsMode();
  }

  public Sprite getSprite() {
    return sprite;
  }

  public void setSprite(Sprite sprite) {
    this.sprite = sprite;
  }

  public EntityType getType() {
    return type;
  }

  public EntityData getEntityData() {
    return getType().getDataObject();
  }

}
