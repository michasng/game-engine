package de.hsa.dice.core.game;

import java.util.HashMap;
import java.util.LinkedList;

import de.hsa.dice.core.InputManager;
import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.MapData;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.manager.ProjectDataManager;
import de.hsa.dice.core.game.map.TileMap;
import de.hsa.dice.core.view.ScenePane;
import javafx.scene.canvas.GraphicsContext;

public class Game extends GameBase {

  // private static final Logger logger = Logger.getLogger(Game.class.getName());

  private static Game instance;

  protected ProjectData projectData;

  protected TileMap map;

  // used to reproduce a bug
  // TileVec lastTilePos;

  protected boolean doWorldUpdate;

  protected LinkedList<Runnable> postUpdate;

  private HashMap<String, Object> globals;

  // TODO: make this configurable for the project, or maybe even for maps
  public static final int PIXEL_PER_METER = 32;

  public Game() {
    this(ProjectDataManager.getInstance().getCurrentProject(), true);
  }

  public Game(ProjectData projectData, boolean doWorldUpdate) {
    super();
    instance = this;

    this.projectData = projectData;
    this.doWorldUpdate = doWorldUpdate;

    MapData mapData = projectData.getSubManager(DataType.MAP, MapData.class).get(projectData.getStartMapName());
    map = new TileMap(mapData);

    globals = new HashMap<>();
    postUpdate = new LinkedList<>();
  }

  @Override
  public void init(ScenePane sceneView) {
    super.init(sceneView);

    int numCols = map.getNumCols();
    int numRows = map.getNumRows();
    camera.scaleToShow(numCols, numRows, (float) sceneView.getWidth(), (float) sceneView.getHeight());
    camera.focus(numCols * PIXEL_PER_METER / 2, numRows * PIXEL_PER_METER / 2);
  }

  @Override
  public void onDestroy() {
    // only used in subclasses so far
    SoundPlayer.unloadAllClips();
    ImageLoader.getInstance().clearCache();
  }

  @Override
  public void handleInput() {

    /*
     * if (input.isMousePressed()) { mouse =
     * camera.pixelToWorld(input.getMousePos());
     * 
     * player.getBody().setTransform(mouse, 0);
     * player.getBody().setLinearVelocity(new Vec2(0, 0)); }
     */

    /*
     * if (input.isKeyClicked(KeyCode.W)) player.applyForce(0, -200f); if
     * (input.isKeyPressed(KeyCode.A)) player.applyForce(-10, 0); if
     * (input.isKeyPressed(KeyCode.D)) player.applyForce(10, 0);
     */

    camMouseMove();
    camMouseScroll();
    camera.updateScaledOffset(sceneView.getSize()); // always update the camera after it has moved

    // TileVec tilePos = new TileVec(camera.pixelToWorld(input.getMousePos()));
    // if (!tilePos.equals(lastTilePos) && lastTilePos != null) {
    // // System.out.println("how did we get here? " + tilePos + ", " +
    // lastTilePos);
    // }
    // lastTilePos = tilePos;
  }

  @Override
  public void update() {
    super.update();

    // camera.focus(player.getPos().mul(PIXEL_PER_METER));
    if (doWorldUpdate)
      map.update();

    for (var runnable : postUpdate)
      runnable.run();
    postUpdate.clear();
  }

  public void cameraRender(GraphicsContext gc) {
    gc.clearRect(0, 0, getWidth(), getHeight());

    map.render(gc);

    // show camera at scale = 1
    // gc.setStroke(Color.RED);
    // gc.setLineWidth(1);
    // gc.strokeRect(0, 0, sceneView.getWidth() - 1, sceneView.getHeight() - 1);
  }

  public Object getGlobalVariable(String name) {
    return globals.get(name);
  }

  public void setGlobalVariable(String name, Object value) {
    assert name != null;
    assert value != null;
    globals.put(name, value);
  }

  public void addToPostUpdate(Runnable runnable) {
    postUpdate.add(runnable);
  }

  public void setMap(TileMap map) {
    this.map = map;
  }

  public TileMap getMap() {
    return map;
  }

  public int getWidth() {
    return map.getNumCols() * Game.PIXEL_PER_METER;
  }

  public int getHeight() {
    return map.getNumRows() * Game.PIXEL_PER_METER;
  }

  public ProjectData getProjectData() {
    return projectData;
  }

  public InputManager getInput() {
    return input;
  }

  public static Game getInstance() {
    return instance;
  }

}
