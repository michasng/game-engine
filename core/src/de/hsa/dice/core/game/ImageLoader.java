package de.hsa.dice.core.game;

import java.util.HashMap;

import de.hsa.dice.core.util.io.FileOperator;
import javafx.scene.image.Image;

public final class ImageLoader {

  private static ImageLoader instance;

  private HashMap<String, Image> images;

  private ImageLoader() {
    images = new HashMap<>();
  }

  public Image load(String path) {
    if (images.containsKey(path))
      return images.get(path);

    var inputStream = FileOperator.getInputStream(path);
    if (inputStream == null)
      return null;
    var newImage = new Image(inputStream);
    images.put(path, newImage);
    return newImage;
  }

  public void clearCache() {
    images.clear();
  }

  public static ImageLoader getInstance() {
    if (instance == null)
      instance = new ImageLoader();
    return instance;
  }

}
