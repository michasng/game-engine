package de.hsa.dice.core.game.map;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.EntityData.GraphicsMode;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.SpriteData;
import de.hsa.dice.core.dataobject.TileData;
import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.game.Sprite;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Tile {

  private Color color;
  private Sprite sprite;

  private boolean showHitbox;
  private Color hitboxColor;

  private TileData dataObject;

  public Tile(TileData dataObject) {
    this.dataObject = dataObject;
    color = Color.web(dataObject.getColor());

    var spriteMgr = ProjectData.getCurrent().getSubManager(DataType.SPRITE, SpriteData.class);
    var spriteData = spriteMgr.get(dataObject.getSprite());
    if (spriteData != null)
      sprite = new Sprite(spriteData);

    hitboxColor = Color.WHITE;
  }

  public void update() {
    if (dataObject.getGraphicsMode() == GraphicsMode.SPRITE && sprite != null)
      sprite.update();
  }

  public void render(GraphicsContext gc, float col, float row) {
    var paddings = dataObject.getPaddings();
    var x = col * Game.PIXEL_PER_METER;
    var y = row * Game.PIXEL_PER_METER;
    var renderX = x - paddings.width / 2 + paddings.x;
    var renderY = y - paddings.height / 2 + paddings.y;
    var renderWidth = Game.PIXEL_PER_METER + paddings.width;
    var renderHeight = Game.PIXEL_PER_METER + paddings.height;

    // translate to upper-left corner of the tile
    gc.translate(renderX, renderY);

    if (dataObject.getGraphicsMode() == GraphicsMode.COLOR) {
      gc.setFill(color);
      gc.fillRect(0, 0, renderWidth, renderHeight);
    } else if (dataObject.getGraphicsMode() == GraphicsMode.SPRITE && sprite != null) {
      // gc.translate(paddings.x, paddings.y);
      sprite.render(gc, renderWidth, renderHeight);
      // gc.translate(-paddings.x, -paddings.y);
    }
    gc.translate(-renderX, -renderY);

    if (showHitbox) {
      gc.setStroke(hitboxColor);
      gc.setLineWidth(2);
      // subtract the paddings to render just the hitbox
      gc.strokeRect(x, y, Game.PIXEL_PER_METER, Game.PIXEL_PER_METER);
    }

  }

  public void setShowHitbox(boolean showHitbox) {
    this.showHitbox = showHitbox;
  }

  public boolean isShowHitbox() {
    return showHitbox;
  }

  public void setHitboxColor(Color hitboxColor) {
    this.hitboxColor = hitboxColor;
  }

  public Color getHitboxColor() {
    return hitboxColor;
  }

  public boolean isSolid() {
    return dataObject.isSolid();
  }

  public TileData getDataObject() {
    return dataObject;
  }

}
