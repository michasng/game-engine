package de.hsa.dice.core.game.map;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.TreeMap;

import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.jbox2d.dynamics.contacts.Contact;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.EntityData;
import de.hsa.dice.core.dataobject.MapData;
import de.hsa.dice.core.dataobject.TileData;
import de.hsa.dice.core.dataobject.nested.MapEntityData;
import de.hsa.dice.core.dataobject.nested.MapTileData;
import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.game.entity.Entity;
import de.hsa.dice.core.game.entity.EntityType;
import de.hsa.dice.core.util.ArrayUtil;
import de.hsa.dice.core.view.Renderable;
import javafx.scene.canvas.GraphicsContext;

public class TileMap implements Renderable {

  private MapData mapData;

  private int numCols, numRows;

  private int[][] tiles;
  private Body[][] solids;

  private TreeMap<Integer, Tile> tileIdMap;

  private World world;

  private HashMap<String, EntityType> entityTypes;
  private LinkedList<Entity> entities;

  public TileMap(MapData mapData) {
    this.mapData = mapData;
    var projectData = mapData.getManager().getProjectData();

    // construct the tileIdMap
    tileIdMap = new TreeMap<Integer, Tile>();
    var mapTileDatas = mapData.getTileIds();
    for (MapTileData mapTileData : mapTileDatas) {
      var tileName = mapTileData.getName();
      var tileData = projectData.getSubManager(DataType.TILE, TileData.class).get(tileName);
      if (tileData != null) {
        var tile = new Tile(tileData);
        tileIdMap.put(mapTileData.getId(), tile);
      }
    }

    world = new World(new Vec2(0, mapData.getGravity()));
    world.setContactListener(new ContactListener() {

      @Override
      public void beginContact(Contact contact) {
        Object obj1 = contact.getFixtureA().getBody().getUserData();
        Object obj2 = contact.getFixtureB().getBody().getUserData();
        Entity e1 = (obj1 instanceof Entity) ? (Entity) obj1 : null;
        Entity e2 = (obj2 instanceof Entity) ? (Entity) obj2 : null;
        if (e1 != null)
          e1.beginContact(contact, e2);
        if (e2 != null)
          e2.beginContact(contact, e1);
      }

      @Override
      public void endContact(Contact contact) {
        Object obj1 = contact.getFixtureA().getBody().getUserData();
        Object obj2 = contact.getFixtureB().getBody().getUserData();
        Entity e1 = (obj1 instanceof Entity) ? (Entity) obj1 : null;
        Entity e2 = (obj2 instanceof Entity) ? (Entity) obj2 : null;
        if (e1 != null)
          e1.endContact(contact, e2);
        if (e2 != null)
          e2.endContact(contact, e1);
      }

      @Override
      public void preSolve(Contact contact, Manifold oldManifold) {
        // disable the contact
        Object obj1 = contact.getFixtureA().getBody().getUserData();
        Object obj2 = contact.getFixtureB().getBody().getUserData();
        Entity e1 = (obj1 instanceof Entity) ? (Entity) obj1 : null;
        Entity e2 = (obj2 instanceof Entity) ? (Entity) obj2 : null;
        if ((e1 != null && e1.getEntityData().isContactsDisabled())
            || (e2 != null && e2.getEntityData().isContactsDisabled()))
          contact.setEnabled(false);
      }

      @Override
      public void postSolve(Contact contact, ContactImpulse impulse) {
      }

    });
    updateTilesFromData();

    var entityManager = projectData.getSubManager(DataType.ENTITY, EntityData.class);
    entityTypes = new HashMap<>();
    for (var entityTypeName : entityManager.getNames())
      entityTypes.put(entityTypeName, new EntityType(entityManager.get(entityTypeName)));
    entities = new LinkedList<>();
    for (var mapEntity : mapData.getEntities()) {
      spawnEntity(mapEntity);
    }
  }

  public void updateTilesFromData() {
    tiles = ArrayUtil.cloneArrayValues(mapData.getTiles());
    numCols = mapData.getNumCols();
    numRows = mapData.getNumRows();
    updateSolidsFromTiles();

  }

  private void updateSolidsFromTiles() {
    // remove old bodies
    if (solids != null) {
      for (int row = 0; row < solids.length; row++) {
        for (int col = 0; col < solids[row].length; col++) {
          if (solids[row][col] != null)
            world.destroyBody(solids[row][col]);
        }
      }
    }

    // create new bodies
    solids = new Body[numRows][numCols];
    TileVec tilePos;
    for (int row = 0; row < numRows; row++) {
      for (int col = 0; col < numCols; col++) {
        if (tiles[row][col] == 0)
          continue;
        var tile = tileIdMap.get(tiles[row][col]);
        if (!tile.isSolid())
          continue;

        tilePos = new TileVec(col, row);
        solids[row][col] = createSolid(tile, tilePos);
      }
    }
  }

  private Body createSolid(Tile tile, TileVec tilePos) {
    BodyDef bodyDef = new BodyDef();
    bodyDef.type = BodyType.STATIC;
    bodyDef.position.set(tilePos.x + 0.5f, tilePos.y + 0.5f);

    PolygonShape shape = new PolygonShape();
    shape.setAsBox(0.5f, 0.5f); // half size

    FixtureDef fixtureDef = new FixtureDef();
    fixtureDef.density = tile.getDataObject().getDensity();
    fixtureDef.friction = tile.getDataObject().getFriction();
    fixtureDef.restitution = tile.getDataObject().getElasticity();

    fixtureDef.shape = shape;
    Body body = world.createBody(bodyDef);
    body.createFixture(fixtureDef);
    return body;
  }

  public Entity spawnEntity(MapEntityData mapEntityData) {
    var type = entityTypes.get(mapEntityData.getName());
    if (type == null) {
      Console.logError("Missing entity type: " + mapEntityData.getName());
      return null;
    }
    Entity entity = new Entity(type, mapEntityData, this);
    entities.add(entity);
    return entity;
  }

  public void despawnEntity(Entity entity) {
    entities.remove(entity);
    if (world != entity.getBody().getWorld())
      return; // in case the map was changed and a body should despawn afterwards
    world.destroyBody(entity.getBody());
  }

  public void update() {
    // TODO: update according to fps
    world.step(1f / 30, 1, 1);
    // System.out.println("timestep: " + (1f / 30));
    for (var entity : entities)
      entity.update();

    for (var tile : tileIdMap.values())
      tile.update();
  }

  @Override
  public void render(GraphicsContext gc) {
    var camera = Game.getInstance().getCamera();

    for (int row = camera.getFirstVisibleRow(); row < camera.getFirstInvisibleRow(numRows); row++) {
      for (int col = camera.getFirstVisibleCol(); col < camera.getFirstInvisibleCol(numCols); col++) {
        Tile tile = tileIdMap.get(tiles[row][col]);
        if (tile != null) {
          tile.render(gc, col, row);
        }
      }
    }

    for (Entity entity : entities) {
      // TODO: why would the entity position be in scene cords? find mistake
      var pos = camera.sceneToWorld(entity.getPosition());
      // take 2 * the padding, bc pos is not affected by paddings on one side
      var w = entity.getType().getWidth() * Game.PIXEL_PER_METER
          + 2 * entity.getType().getDataObject().getPaddings().width;
      var h = entity.getType().getHeight() * Game.PIXEL_PER_METER
          + 2 * entity.getType().getDataObject().getPaddings().height;
      var maxSideLength = (float) Math.sqrt(w * w + h * h); // diagonal length
      var size = new Vec2(maxSideLength, maxSideLength);
      if (camera.isVisible(pos, size))
        entity.render(gc);
    }
  }

  public boolean isWithinBounds(Vec2 worldPos) {
    return isWithinBounds(new TileVec(worldPos));
  }

  public boolean isWithinBounds(TileVec tilePos) {
    return (tilePos.x >= 0 && tilePos.y >= 0 && tilePos.x < numCols && tilePos.y < numRows);
  }

  public TileVec worldToTile(Vec2 worldPos) {
    return new TileVec(worldPos);
  }

  public void setTile(Vec2 worldPos, int tileId) {
    setTile(new TileVec(worldPos), tileId);
  }

  public void setTile(TileVec tilePos, int tileId) {
    tiles[tilePos.y][tilePos.x] = tileId;
    var tile = tileIdMap.get(tileId);

    if (tileId != 0 && tile.isSolid()) {
      if (getSolid(tilePos) != null)
        world.destroyBody(getSolid(tilePos));
      solids[tilePos.y][tilePos.x] = createSolid(tile, tilePos);
    } else {
      if (getSolid(tilePos) != null) {
        world.destroyBody(getSolid(tilePos));
        solids[tilePos.y][tilePos.x] = null;
      }
    }
  }

  public Entity getEntityAtPos(Vec2 pos) {
    for (var entity : entities) {
      if (entity.getBody().getFixtureList().testPoint(pos))
        return entity;
    }
    return null;
  }

  public int getTile(Vec2 worldPos) {
    return getTile(new TileVec(worldPos));
  }

  public int getTile(TileVec tilePos) {
    return tiles[tilePos.y][tilePos.x];
  }

  public Body getSolid(TileVec tilePos) {
    return solids[tilePos.y][tilePos.x];
  }

  public int getNumCols() {
    return numCols;
  }

  public int getNumRows() {
    return numRows;
  }

  public TileVec getSize() {
    return new TileVec(numCols, numRows);
  }

  public void addTile(int id, Tile tile) {
    tileIdMap.put(id, tile);
  }

  public Tile getTile(int id) {
    return tileIdMap.get(id);
  }

  public int getTileId(String tileName) {
    for (var tileId : tileIdMap.keySet()) {
      var tile = tileIdMap.get(tileId);
      if (tile.getDataObject().getName().equals(tileName))
        return tileId;
    }
    return -1;
  }

  public World getWorld() {
    return world;
  }

  public LinkedList<Entity> getEntities() {
    return entities;
  }

  public MapData getDataObject() {
    return mapData;
  }

}
