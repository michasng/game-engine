package de.hsa.dice.core.game.map;

import org.jbox2d.common.Vec2;

public class TileVec {

  public final int x, y;

  public TileVec(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public TileVec(Vec2 vec) {
    this((int) Math.floor(vec.x), (int) Math.floor(vec.y));
  }

  public TileVec add(TileVec other) {
    return new TileVec(x + other.x, y + other.y);
  }

  public TileVec sub(TileVec other) {
    return new TileVec(x - other.x, y - other.y);
  }

  public TileVec mul(int factor) {
    return new TileVec(x * factor, y * factor);
  }

  public Vec2 mul(float factor) {
    return new Vec2(x * factor, y * factor);
  }

  public TileVec intDiv(int divisor) {
    return new TileVec(x / divisor, y / divisor);
  }

  public Vec2 div(float divisor) {
    return new Vec2(x / divisor, y / divisor);
  }

  public Vec2 toVec2() {
    return new Vec2(x, y);
  }

  // will return false, because TileVec will call the other method implicitly
  @Override
  public boolean equals(Object other) {
    if (other == null || !(other instanceof TileVec))
      return false;
    return equals((TileVec) other);
  }

  public boolean equals(TileVec other) {
    if (other == null)
      return false;
    return (x == other.x && y == other.y);
  }

  @Override
  public String toString() {
    return "(" + x + "," + y + ")";
  }

}
