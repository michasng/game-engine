package de.hsa.dice.core.game;

import java.util.logging.Logger;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.InputManager;
import de.hsa.dice.core.view.CanvasScene;
import de.hsa.dice.core.view.ScenePane;
import javafx.scene.canvas.GraphicsContext;

public abstract class GameBase implements CanvasScene {

  private static final Logger logger = Logger.getLogger(GameBase.class.getName());

  protected ScenePane sceneView;
  protected InputManager input;
  protected Camera camera;

  protected int mouseMapMovementBtn;

  public GameBase() {
    this.camera = new Camera();

    mouseMapMovementBtn = InputManager.MOUSE_SECONDARY;
  }

  @Override
  public void init(ScenePane sceneView) {
    logger.fine("Initializing Scene");
    this.sceneView = sceneView;
    this.input = sceneView.getInputManager();
  }

  @Override
  public void update() {
    handleInput();
  }

  public abstract void handleInput();

  public void camMouseMove() {
    if (input.isMousePressed(mouseMapMovementBtn) && input.isMouseMoving()) {
      Vec2 delta = input.getDeltaMouse().mul(1f / camera.getScale());
      camera.move(delta.negate());
    }
  }

  public void camMouseScroll() {
    if (input.isMouseScrolledY()) {
      var deltaScale = input.getMouseScrollY() * 0.001f * camera.getScale();
      camera.setScale(camera.getScale() + deltaScale);
//      if (deltaScale > 0) {
//        var deltaFocus = camera.pixelToScene(input.getMousePos()).sub(camera.getFocalPoint());
////        System.out.println("scrollY: " + input.getMouseScrollY());
////        System.out.println("scrollSpeed: " + 0.0002f);
////        System.out.println("deltaScale: " + deltaScale);
//        camera.focus(camera.getFocalPoint().add(deltaFocus.mul(1 - camera.getScale())));
//      }
    }
  }

  @Override
  public void render(GraphicsContext gc) {
    camera.doTransform(gc);
    cameraRender(gc);
    camera.undoTransform(gc);

    // visualize cursor
//    gc.setFill(Color.RED);
//    gc.fillRect(input.getMouseX() - 1, input.getMouseY() - 1, 2, 2);
//    gc.setStroke(Color.GREEN);
//    gc.strokeLine(input.getMouseX() - input.getDeltaMouseX(), input.getMouseY() - input.getDeltaMouseY(),
//        input.getMouseX(), input.getMouseY());
  }

  public void cameraRender(GraphicsContext gc) {
  }

  public void setCamera(Camera camera) {
    this.camera = camera;
  }

  public Camera getCamera() {
    return camera;
  }

}
