package de.hsa.dice.core.game;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import de.hsa.dice.core.util.io.FileOperator;

public class SoundPlayer {

  private static LinkedList<Clip> loadedClips = new LinkedList<>();

  public static void unloadClip(Clip clip) {
    if (clip.isRunning())
      clip.stop();
    loadedClips.remove(clip);
  }

  public static void unloadAllClips() {
    for (var clip : loadedClips)
      if (clip.isRunning())
        clip.stop();
    loadedClips.clear();
  }

  public static Clip loadClip(InputStream inputStream) throws FileNotFoundException {
    try {
      InputStream bin = new BufferedInputStream(inputStream);
      AudioInputStream ais = AudioSystem.getAudioInputStream(bin);
      Clip clip = AudioSystem.getClip();
      clip.open(ais);
      loadedClips.add(clip);
      return clip;
    } catch (LineUnavailableException | IOException | UnsupportedAudioFileException e) {
      throw new FileNotFoundException("Failed to load clip.");
    }
  }

  public static Clip loadInternalClip(String path) throws FileNotFoundException {
    var inputStream = FileOperator.getInputStreamClasspath(path);
    try {
      return loadClip(inputStream);
    } catch (FileNotFoundException e) {
      throw new FileNotFoundException("Failed to load internal clip: " + path);
    }
  }

  public static Clip loadExternalClip(String path) throws FileNotFoundException {
    var inputStream = FileOperator.getInputStream(path);
    try {
      return loadClip(inputStream);
    } catch (FileNotFoundException e) {
      throw new FileNotFoundException("Failed to load external clip: " + path);
    }
  }

  public static void play(Clip clip) {
    // restart the clip
    if (clip.isRunning()) {
      clip.stop();
    }
    clip.setFramePosition(0);

    clip.start();

  }

  public static void pause(Clip clip) {
    if (clip.isRunning())
      clip.stop();
  }

  public static void stop(Clip clip) {
    if (clip.isRunning())
      clip.stop();
    clip.setFramePosition(0);
  }

  public static void loop(Clip clip) {
    if (clip.isRunning())
      clip.stop();
    clip.setFramePosition(0);

    clip.loop(Clip.LOOP_CONTINUOUSLY);
  }

  public static void setVolume(Clip clip, float volume) {
    // make sure the speakers are capable of playing at that volume
    FloatControl control = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
    control.setValue(getLimitedVolume(control, volume));
  }

  /**
   * @param control the FloatControl object, that provides the max and min values
   *                for the volume
   * @param volume  the desired volume
   * @return a capped volume level, according to the FloatControl object
   */
  private static float getLimitedVolume(FloatControl control, float volume) {
    if (volume > control.getMaximum())
      return control.getMaximum();
    if (volume < control.getMinimum())
      return control.getMinimum();
    return volume;
  }

}
