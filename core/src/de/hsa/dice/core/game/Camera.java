package de.hsa.dice.core.game;

import org.jbox2d.common.Vec2;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

public class Camera {

  private float x, y;
  private float scale;

  private float pixelWidth, pixelHeight;
  private float scaledXOffset, scaledYOffset;

  public Camera() {
    this(0, 0, 1);
  }

  public Camera(float x, float y, float scale) {
    this.x = x;
    this.y = y;
    this.scale = scale;
  }

  // TODO: remove all uses of Game.PIXEL_PER_METER outside of Game
  public Camera(int numCols, int numRows, float pixelWidth, float pixelHeight) {
    scaleToShow(numCols, numRows, pixelWidth, pixelHeight);
    focus(numCols * Game.PIXEL_PER_METER / 2, numRows * Game.PIXEL_PER_METER / 2);
  }

  public Vec2 pixelToWorld(Vec2 pixelPos) {
    return pixelPos.add(getScaledOffset()).mul(1f / (Game.PIXEL_PER_METER * scale));
  }

  public Vec2 pixelToScene(Vec2 pixelPos) {
    return pixelPos.add(getScaledOffset()).mul(1f / scale);
  }

  public Vec2 worldToScene(Vec2 worldPos) {
    return worldPos.mul(1f / Game.PIXEL_PER_METER);
  }

  public Vec2 sceneToWorld(Vec2 scenePos) {
    return scenePos.mul(Game.PIXEL_PER_METER);
  }

  /**
   * Adjusts the scale, so that an area of at lease numCols * numRows meters is
   * visible within a frame of resolution: pixelWidth * pixelHeight.
   */
  public void scaleToShow(float numCols, float numRows, float pixelWidth, float pixelHeight) {
    float scaledPixelPerMeter;
    if (pixelWidth / numCols < pixelHeight / numRows)
      scaledPixelPerMeter = pixelWidth / numCols;
    else
      scaledPixelPerMeter = pixelHeight / numRows;
    this.scale = scaledPixelPerMeter / Game.PIXEL_PER_METER;
  }

  public void updateScaledOffset(Canvas canvas) {
    updateScaledOffset(new Vec2((float) canvas.getWidth(), (float) canvas.getHeight()));
  }

  public void updateScaledOffset(Vec2 size) {
    this.pixelWidth = size.x;
    this.pixelHeight = size.y;
    scaledXOffset = (x - pixelWidth / 2f) * scale - (pixelWidth - pixelWidth * scale) / 2f;
    scaledYOffset = (y - pixelHeight / 2f) * scale - (pixelHeight - pixelHeight * scale) / 2f;
  }

  public float getVisibleWidth() {
    return pixelWidth / scale;
  }

  public float getVisibleHeight() {
    return pixelHeight / scale;
  }

  public void doTransform(GraphicsContext gc) {
    gc.translate(-scaledXOffset, -scaledYOffset);
    gc.scale(scale, scale);
  }

  public void undoTransform(GraphicsContext gc) {
    gc.scale(1 / scale, 1 / scale);
    gc.translate(scaledXOffset, scaledYOffset);
  }

  public Vec2 getFocalPoint() {
    return new Vec2(x, y);
  }

  public void focus(float pixelX, float pixelY) {
    this.x = pixelX;
    this.y = pixelY;
  }

  public void focusWorld(float worldX, float worldY) {
    this.x = worldX * Game.PIXEL_PER_METER;
    this.y = worldY * Game.PIXEL_PER_METER;
  }

  public void focus(Vec2 pixelPos) {
    focus(pixelPos.x, pixelPos.y);
  }

  public void move(Vec2 deltaPixel) {
    x += deltaPixel.x;
    y += deltaPixel.y;
  }

  public int getFirstVisibleCol() {
    var leftMostCol = (int) Math.floor((x - getVisibleWidth() / 2) / Game.PIXEL_PER_METER);
    return leftMostCol < 0 ? 0 : leftMostCol;
  }

  public int getFirstInvisibleCol(int numCols) {
    var rightMostCol = (int) Math.floor((x + getVisibleWidth() / 2) / Game.PIXEL_PER_METER) + 1;
    return rightMostCol > numCols ? numCols : rightMostCol;
  }

  public int getFirstVisibleRow() {
    var topMostRow = (int) Math.floor((y - getVisibleHeight() / 2) / Game.PIXEL_PER_METER);
    return topMostRow < 0 ? 0 : topMostRow;
  }

  public int getFirstInvisibleRow(int numRows) {
    var bottomMostRow = (int) Math.floor((y + getVisibleHeight() / 2) / Game.PIXEL_PER_METER) + 1;
    return bottomMostRow > numRows ? numRows : bottomMostRow;
  }

  public boolean isVisible(Vec2 worldPos) {
    var center = new Vec2(x, y);
    var width = getVisibleWidth();
    var height = getVisibleHeight();
    return (worldPos.x > center.x - width / 2 && worldPos.x < center.x + width / 2 && worldPos.y > center.y - height / 2
        && worldPos.y < center.y + height / 2);
  }

  public boolean isVisible(Vec2 worldPos, Vec2 worldSize) {
    var center = new Vec2(x, y);
    var width = getVisibleWidth();
    var height = getVisibleHeight();
    return (worldPos.x + worldSize.x / 2 > center.x - width / 2 && worldPos.x - worldSize.x / 2 < center.x + width / 2
        && worldPos.y + worldSize.y / 2 > center.y - height / 2
        && worldPos.y - worldSize.y / 2 < center.y + height / 2);
  }

  public float getX() {
    return x;
  }

  public void setX(float x) {
    this.x = x;
  }

  public float getY() {
    return y;
  }

  public void setY(float y) {
    this.y = y;
  }

  public float getScale() {
    return scale;
  }

  public void setScale(float scale) {
    if (scale < 0.1f)
      scale = 0.1f;
    this.scale = scale;
  }

  public Vec2 getScaledOffset() {
    return new Vec2((float) scaledXOffset, (float) scaledYOffset);
  }

  public float getScaledXOffset() {
    return scaledXOffset;
  }

  public float getScaledYOffset() {
    return scaledYOffset;
  }

}
