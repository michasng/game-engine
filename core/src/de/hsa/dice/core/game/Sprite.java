package de.hsa.dice.core.game;

import java.util.LinkedList;

import de.hsa.dice.core.dataobject.SpriteData;
import de.hsa.dice.core.dataobject.nested.RectangleData;
import de.hsa.dice.core.util.io.ProjectFileOperator;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class Sprite {

  private int count, index;
  private LinkedList<Image> images;
  private LinkedList<Integer> totalDurations;
  private double maxImgWidth, maxImgHeight;

  private SpriteData dataObject;

  public Sprite(SpriteData dataObject) {
    this.dataObject = dataObject;

    var prjFileOp = ProjectFileOperator.getInstance();
    images = new LinkedList<>();
    totalDurations = new LinkedList<>();
    int totalDuration = 0;
    for (var frame : dataObject.getFrames()) {
      var relPath = frame.getPath();
      if (!relPath.isEmpty()) {
        var absPath = prjFileOp.getAbsolutePath(relPath);
        var image = ImageLoader.getInstance().load(absPath);
        if (image == null)
          continue;
        if (frame.getSubRegion() != null)
          image = getSubimage(image, frame.getSubRegion());
        if (image.getWidth() > maxImgWidth)
          maxImgWidth = image.getWidth();
        if (image.getHeight() > maxImgHeight)
          maxImgHeight = image.getHeight();
        images.add(image);
        totalDurations.add(totalDuration += frame.getDuration());
      }
    }
  }

  public void update() {
    if (!dataObject.isAnimated() || totalDurations.isEmpty())
      return;

    count++;
    if (count > totalDurations.get(index)) {
      index++;
      if (index == totalDurations.size()) {
        index = 0;
        count = 0;
      }
    }
//    System.out.println("count: " + count + ", index: " + index);
  }

  public void render(GraphicsContext gc, float width, float height) {
    var scaleX = width / maxImgWidth;
    var scaleY = height / maxImgHeight;
    var scale = scaleX < scaleY ? scaleX : scaleY;
    if (!images.isEmpty()) {
      var image = images.get(index);
      var scaledWidth = image.getWidth() * scale;
      var scaledHeight = image.getHeight() * scale;
      gc.drawImage(image, (width - scaledWidth) / 2, (height - scaledHeight) / 2, scaledWidth, scaledHeight);
    }
  }

  public int getFrameIndex() {
    return index;
  }

  public void setFrameIndex(int frameIndex) {
    this.index = frameIndex;

    if (index <= 0 || index >= totalDurations.size()) {
      index = 0;
      count = 0;
    } else {
      count = totalDurations.get(index - 1);
    }
  }

  public Image getCurrentImage() {
    return images.get(index);
  }

  public SpriteData getDataObject() {
    return dataObject;
  }

  public static Image getSubimage(Image image, RectangleData rect) {
    Canvas canvas = new Canvas(rect.width, rect.height);
    var gc = canvas.getGraphicsContext2D();
    gc.drawImage(image, rect.x, rect.y, rect.width, rect.height, 0, 0, rect.width, rect.height);
    var snapshotParams = new SnapshotParameters();
    snapshotParams.setFill(Color.TRANSPARENT);
    var writeableImage = new WritableImage((int) rect.width, (int) rect.height);
    canvas.snapshot(snapshotParams, writeableImage);
    return writeableImage;
  }

}
