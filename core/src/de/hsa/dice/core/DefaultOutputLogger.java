package de.hsa.dice.core;

public class DefaultOutputLogger implements OutputLogger {

  @Override
  public void log(String message) {
    System.out.println(message);
  }

  @Override
  public void logWarning(String warning) {
    System.out.println("Warning: " + warning);
  }

  @Override
  public void logError(String error) {
    new Error(error).printStackTrace();
  }

  @Override
  public void logError(Exception e) {
    e.printStackTrace();
  }

}
