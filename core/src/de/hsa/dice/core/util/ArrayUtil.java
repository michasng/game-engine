package de.hsa.dice.core.util;

public class ArrayUtil {

  /**
   * The standard array.clone() method will only copy the references, while this
   * method will perform a deep-copy where the values are also copied and even the
   * references within the array change.
   * 
   * @param source
   *          the original int matrix
   * @return a deep-copy of the source array
   */
  public static int[][] cloneArrayValues(int[][] source) {
    int[][] result = new int[source.length][];
    for (int row = 0; row < result.length; row++) {
      result[row] = new int[source[row].length];
      for (int col = 0; col < result[row].length; col++) {
        result[row][col] = source[row][col];
      }
    }
    return result;
  }
  
  public static int[] cloneArrayValues(int[] source) {
  	int[] result = new int[source.length];
  	for (int i = 0; i < result.length; i++) {
  		result[i] = source[i];
  	}
  	return result;
  }

  public static <T> int indexOf(T[] ts, T t) {
    for (int i = 0; i < ts.length; i++) {
      if (ts[i] == t)
        return i;
    }
    return -1;
  }

  public static <T> T[] append(T[] origin, T t, T[] target) {
    if (target.length != origin.length + 1)
      throw new Error("Wrong target length: " + target.length + " instead of " + (origin.length + 1));
    for (int i = 0; i < origin.length; i++)
      target[i] = origin[i];
    target[origin.length] = t;
    return target;
  }

  public static <T> T[] remove(T[] origin, int index, T[] target) {
    if (target.length != origin.length - 1)
      throw new Error("Wrong target length: " + target.length + " instead of " + (origin.length - 1));

    for (int i = 0; i < index; i++)
      target[i] = origin[i];
    for (int i = index; i < target.length; i++)
      target[i] = origin[i + 1];
    return target;
  }

  public static <T> T[] remove(T[] origin, T t, T[] target) {
    int index = indexOf(origin, t);
    if (index == -1)
      throw new Error("Cannot remove an object that doesn't exist.");
    return remove(origin, index, target);
  }

}
