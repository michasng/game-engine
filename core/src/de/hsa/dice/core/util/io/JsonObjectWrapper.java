package de.hsa.dice.core.util.io;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.property.Property;

/**
 * This class wraps the get()-Method of the JsonObject class in order to provide
 * a given fallback value in case the property doesn't exist in the original
 * JsonObject. This may specifically be needed when updating the models.
 */
public class JsonObjectWrapper {

  private JsonObject jsonObject;

  public JsonObjectWrapper(JsonObject jsonObject) {
    this.jsonObject = jsonObject;
  }

  // public <T> T get(String name, T fallback) {
  // var value = jsonObject.get(name);
  // return value == null ? fallback : value.asT(); // asT() wouldn't work
  // }

  public boolean getBoolean(Property<Boolean> prop) {
    return getBoolean(prop.getName(), prop.getValue());
  }

  public boolean getBoolean(String name, boolean fallback) {
    var value = jsonObject.get(name);
    return value == null ? fallback : value.asBoolean();
  }

  public int getInt(Property<Integer> prop) {
    return getInt(prop.getName(), prop.getValue());
  }

  public int getInt(String name, int fallback) {
    var value = jsonObject.get(name);
    return value == null ? fallback : value.asInt();
  }

  public float getFloat(Property<Float> prop) {
    return getFloat(prop.getName(), prop.getValue());
  }

  public float getFloat(String name, float fallback) {
    var value = jsonObject.get(name);
    return value == null ? fallback : value.asFloat();
  }

  public double getDouble(Property<Double> prop) {
    return getDouble(prop.getName(), prop.getValue());
  }

  public double getDouble(String name, double fallback) {
    var value = jsonObject.get(name);
    return value == null ? fallback : value.asDouble();
  }

  public long getLong(Property<Long> prop) {
    return getLong(prop.getName(), prop.getValue());
  }

  public long getLong(String name, long fallback) {
    var value = jsonObject.get(name);
    return value == null ? fallback : value.asLong();
  }

  public String getString(Property<String> prop) {
    return getString(prop.getName(), prop.getValue());
  }

  public String getString(String name, String fallback) {
    var value = jsonObject.get(name);
    return value == null ? fallback : value.asString();
  }

  public JsonObject getObject(String name, JsonObject fallback) {
    var value = jsonObject.get(name);
    return value == null ? fallback : value.asObject();
  }

  public JsonArray getArray(Property<JsonArray> prop) {
    return getArray(prop.getName(), prop.getValue());
  }

  public JsonArray getArray(String name, JsonArray fallback) {
    var value = jsonObject.get(name);
    return value == null ? fallback : value.asArray();
  }

}
