package de.hsa.dice.core.util.io;

import java.io.File;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.Config;
import de.hsa.dice.core.Console;
import de.hsa.dice.core.dataobject.DataObject;
import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;

public class ProjectFileOperator {

  private static ProjectFileOperator instance;

  public static ProjectFileOperator getInstance() {
    if (instance == null)
      instance = new ProjectFileOperator();
    return instance;
  }

  private ProjectFileOperator() {
    new File(getProjectPath()).mkdirs();
  }

  public String getProjectPath() {
    return Config.getInstance().getString(Config.CURRENT_PROJECT_PATH);
  }

  public String getRelativePath(String absTargetPath) {
    return FileOperator.getRelativePath(getProjectPath(), absTargetPath);
  }

  public String getAbsolutePath(String relTargetPath) {
    return FileOperator.getAbsolutePath(getProjectPath(), relTargetPath);
  }

  public String getProjectName() {
    return Config.getInstance().getString(Config.CURRENT_PROJECT_NAME);
  }

  public String getNodesPath() {
    return getProjectPath() + "Nodes" + File.separatorChar;
  }

  public String getDataTypePath(DataType dataType) {
    return getProjectPath() + dataType.pluralName + File.separatorChar;
  }

  public String getDataObjectPath(DataType dataType, String objectName) {
    return getDataTypePath(dataType) + objectName + ".json";
  }

  public String getProjectDataPath() {
    return getProjectDataPath(getProjectName());
  }

  public String getProjectDataPath(String projectName) {
    return getProjectPath() + projectName + ".dice";
  }

  public JsonObject loadDataObjectJson(DataType dataType, String dataFileName) {
    String path = getDataTypePath(dataType) + dataFileName;
    return FileOperator.readJson(path);
  }

  public void clearDataTypeDir(DataType dataType) {
    String path = getDataTypePath(dataType);
    FileOperator.deleteDirContent(path);
  }

  public void saveDataObject(DataObject data) {
    String path = getDataObjectPath(data.getDataType(), data.getName());
    try {
      FileOperator.writeJson(path, data.toJson());
    } catch (UnsupportedSaveException e) {
      Console.logError(e);
    }
  }

  public void saveProjectData(ProjectData projectData) {
    FileOperator.writeJson(getProjectDataPath(projectData.getName()), projectData.toJson());
  }

  public JsonObject loadProjectDataFile() {
    String projectDataPath = getProjectDataPath();
    if (FileOperator.fileExists(new File(projectDataPath)))
      return FileOperator.readJson(projectDataPath);
    else
      return null;
  }

}
