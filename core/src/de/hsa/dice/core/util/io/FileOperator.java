package de.hsa.dice.core.util.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Logger;

import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.ParseException;

// TODO: maybe rewrite some of it using java.nio for performance
public class FileOperator {

  private static final Logger logger = Logger.getLogger(FileOperator.class.getName());

  /**
   * @param path a path to a file within this project
   * @return a file within this project, independent on whether the project has
   *         already compiled, if the file at the given path exists. Otherwise
   *         returns null.
   */
  public static File getFileClasspath(String path) {
    URL dirURL = FileOperator.class.getClassLoader().getResource(path);
    if (dirURL == null) // no log message in this method
      return null;
    return new File(dirURL.getFile());
  }

  public static InputStream getInputStream(String path) {
    try {
      return new FileInputStream(path);
    } catch (FileNotFoundException e) {
      logger.warning("File not found " + path);
      // e.printStackTrace();
      return null;
    }
  }

  public static InputStream getInputStreamClasspath(String path) {
    InputStream stream = FileOperator.class.getClassLoader().getResourceAsStream(path);
    if (stream == null) {
      logger.warning("File not found in classpath " + path);
    }
    return stream;
  }

  public static String getRelativePath(String absBasePath, String absTargetPath) {
    var pathAbsolute = Paths.get(absTargetPath);
    var pathBase = Paths.get(absBasePath);
    var pathRelative = pathBase.normalize().relativize(pathAbsolute);
    return pathRelative.toString();
  }

  public static String getAbsolutePath(String absBasePath, String relTargetPath) {
    return Paths.get(absBasePath, relTargetPath).normalize().toString();
  }

  // getDirs

  /**
   * @param path the path to a directory outside of the project
   * @return all sub-directories within the directory given by path
   */
  public static File[] getDirs(String path) {
    return getDirs(new File(path));
  }

  /**
   * @param path the path to a directory within the project
   * @return all sub-directories within the directory given by path
   */
  public static File[] getDirsClasspath(String path) {
    return getDirs(getFileClasspath(path));
  }

  /**
   * @param path the path to a directory within the project
   * @return all sub-directories within the directory given by path
   */
  public static File[] getDirs(File file) {
    return file.listFiles(File::isDirectory);
  }

  // fileExists

  /**
   * @param path a path to a potentially existing file outside of this project
   * @return true, if the file exists and is not a directory
   */
  public static boolean fileExists(String path) {
    return fileExists(new File(path));
  }

  /**
   * @param path a path to a potentially existing file inside of this project
   * @return true, if the file exists and is not a directory
   */
  public static boolean fileExistsClasspath(String path) {
    return fileExists(getFileClasspath(path));
  }

  /**
   * @param file the file to check for
   * @return true, if the file exists and is not a directory
   */
  public static boolean fileExists(File file) {
    return file != null && file.exists() && !file.isDirectory();
  }

  // dirExists

  /**
   * @param path a path to a potentially existing file outside of this project
   * @return true, if the file exists and is a directory
   */
  public static boolean dirExists(String path) {
    return dirExists(new File(path));
  }

  /**
   * @param path a path to a potentially existing file inside of this project
   * @return true, if the file exists and is a directory
   */
  public static boolean dirExistsClasspath(String path) {
    return dirExists(getFileClasspath(path));
  }

  /**
   * @param file the file to check for
   * @return true, if the file exists and is a directory
   */
  public static boolean dirExists(File dir) {
    return dir != null && dir.exists() && dir.isDirectory();
  }

  // deleting files

  public static void deleteFile(String path) {
    new File(path).delete();
  }

  public static void deleteFileOnExit(String path) {
    new File(path).deleteOnExit();
  }

  public static void deleteDir(String path) {
    deleteFileRec(new File(path));
  }

  public static void deleteDirContent(String path) {
    File dir = new File(path);
    for (File f : dir.listFiles())
      f.delete();
  }

  private static void deleteFileRec(File file) {
    for (File f : file.listFiles())
      deleteFileRec(f);
    file.delete();
  }

  // properties

  /**
   * @param path the path to a properties file outside of this project
   * @return a Properties object containing all properties from the file, or null
   *         if the properties file could not be loaded
   */
  public static Properties readProps(String path) {
    Properties prop = new Properties();
    InputStream input = null;
    try {
      input = new FileInputStream(path);
      prop.load(input);
      input.close();
    } catch (FileNotFoundException e) {
      logger.warning("Could not load properties file: " + path);
      return null;
    } catch (IOException e) {
      e.printStackTrace();
    }
    return prop;

  }

  /**
   * @param path the path to a properties file inside of this project
   * @return a Properties object containing all properties from the file, or null
   *         if the properties file could not be loaded
   */
  public static Properties readPropsClasspath(String path) {
    Properties prop = new Properties();
    InputStream inputStream = getInputStreamClasspath(path);
    if (inputStream == null) {
      logger.warning("Could not load properties file in classpath: " + path);
      return null;
    }

    try {
      prop.load(inputStream);
      inputStream.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return prop;

  }

  /**
   * Will store a Properties object's keys and values in a file.
   * 
   * @param path  the path to a properties file outside of this project
   * @param props the Properties object that needs to be saved
   */
  public static void writeProps(String path, Properties props) {
    new File(path).getParentFile().mkdirs();

    OutputStream output = null;
    try {
      output = new FileOutputStream(path);
      props.store(output, null);
      output.close();
    } catch (IOException e) {
      logger.warning("Could not save properties file at " + path);
      e.printStackTrace();
    }
  }

  // json

  public static JsonObject readJson(String path) {
    try {
      FileReader fStream = new FileReader(path);
      BufferedReader in = new BufferedReader(fStream);
      try {
        JsonObject json = JsonObject.readFrom(in);
        return json;
      } catch (ParseException e) {
        return null;
      } finally {
        in.close();
        fStream.close();
      }
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static void writeJson(String path, JsonObject json) {
    try {
      new File(path).getParentFile().mkdirs();

      FileWriter fStream = new FileWriter(path);
      BufferedWriter out = new BufferedWriter(fStream);
      json.writeTo(out);
      out.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  // read

  /**
   * @param path the path of the file from which to read
   * @return the String of text contained within the file
   */
  public static String read(String path) {
    File file = new File(path);
    URI uri = file.toURI();
    byte[] bytes = null;
    try {
      bytes = Files.readAllBytes(Paths.get(uri));
    } catch (IOException e) {
      logger.warning("Error reading file: " + file.getName());
      e.printStackTrace();
      return "Error reading file: " + file.getName();
    }

    return new String(bytes);
  }

  public static String readClasspath(String path) {
    StringBuilder sb = new StringBuilder();
    try {
      InputStream stream = getInputStreamClasspath(path);
      BufferedReader br = new BufferedReader(new InputStreamReader(stream));
      String currentLine;
      while ((currentLine = br.readLine()) != null) {
        sb.append(currentLine);
        sb.append(System.lineSeparator());
      }
      br.close();
    } catch (IOException e) {
      logger.warning("Error reading file from classpath: " + path);
      e.printStackTrace();
    }
    return sb.toString();
  }

  public static String[] readLines(String path) {
    try {
      return readLines(getInputStream(path));
    } catch (IOException e) {
      logger.warning("Error reading file: " + path);
      e.printStackTrace();
      return null;
    }
  }

  public static String[] readLinesClasspath(String path) {
    try {
      return readLines(getInputStreamClasspath(path));
    } catch (IOException e) {
      logger.warning("Error reading file from classpath: " + path);
      e.printStackTrace();
      return null;
    }
  }

  public static String[] readLines(InputStream stream) throws IOException {
    ArrayList<String> lineList = new ArrayList<String>();
    BufferedReader br = new BufferedReader(new InputStreamReader(stream));
    String currentLine;
    while ((currentLine = br.readLine()) != null)
      lineList.add(currentLine);
    br.close();
    return lineList.toArray(new String[lineList.size()]);
  }

  // write

  /**
   * @param path the path of the file to which to write
   * @param text the text, that should be written to the file
   */
  public static void write(String path, String text) {
    new File(path).getParentFile().mkdirs();
    try {
      FileWriter fstream = new FileWriter(path);
      BufferedWriter out = new BufferedWriter(fstream);
      out.write(text);
      out.close();
    } catch (IOException e) {
      logger.warning("Error writing file: " + path);
      e.printStackTrace();
    }
  }

}
