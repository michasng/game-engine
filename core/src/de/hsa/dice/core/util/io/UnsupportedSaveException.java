package de.hsa.dice.core.util.io;

public class UnsupportedSaveException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public UnsupportedSaveException() {
    super();
  }

  public UnsupportedSaveException(String message) {
    super(message);
  }

}
