package de.hsa.dice.core.util.io;

import com.eclipsesource.json.JsonObject;

public interface JsonSerializable {

  JsonObject toJson();
  
}
