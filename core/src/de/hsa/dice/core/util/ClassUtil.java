package de.hsa.dice.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ClassUtil {

  public static Class<?> forName(String className) throws ClassNotFoundException {
    switch (className) {
    case "boolean":
      return boolean.class;
    case "byte":
      return byte.class;
    case "short":
      return short.class;
    case "int":
      return int.class;
    case "long":
      return long.class;
    case "float":
      return float.class;
    case "double":
      return double.class;
    case "char":
      return char.class;
    default:
      return ClassUtil.class.getClassLoader().loadClass(className);
    }
  }

  public static List<Class<?>> loadInternalClasses(String packageName)
      throws ClassNotFoundException, IOException, URISyntaxException {
    String jarPath = getJarPath();
    if (jarPath != null)
      return loadInternalClassesJar(jarPath, packageName);
    return loadInternalClassesDev(packageName);
  }

  /**
   * @return the path to the .jar file that contains this class or null, if this
   *         is executed outside of a .jar file
   * @throws URISyntaxException
   * @throws MalformedURLException
   */
  private static String getJarPath() throws MalformedURLException, URISyntaxException {
    String path = ClassUtil.class.getResource(ClassUtil.class.getSimpleName() + ".class").getFile();
    if (path.startsWith("/")) {
      // /C:/ProjectPath/bin/packageName/packageName/ClassName.class
      return null;
    }
    // packageName/packageName/ClassName.class
    path = ClassLoader.getSystemClassLoader().getResource(path).getFile();
    // file:/C:/Users/.../FileName.jar!/packageName/packageName/ClassName.class
    path = new URI(path).toURL().getFile();
    // /C:/Users/.../FileName.jar!/packageName/packageName/ClassName.class
    path = path.substring(0, path.lastIndexOf('!'));
    // /C:/Users/.../FileName.jar
    return path;
  }

  private static List<Class<?>> loadInternalClassesJar(String jarPath, String packageName)
      throws ClassNotFoundException, IOException {
    List<Class<?>> classes = new LinkedList<>();
    ZipInputStream zip = new ZipInputStream(new FileInputStream(jarPath));
    for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
      if (!entry.isDirectory() && entry.getName().endsWith(".class")) {
        // This ZipEntry represents a class. Now, what class does it represent?
        String className = entry.getName().replace('/', '.');
        if (className.contains(packageName)) {
          className = className.substring(0, className.length() - ".class".length());
          classes.add(Class.forName(className));
        }
      }
    }
    zip.close();
    return classes;
  }

  /**
   * Scans all classes accessible from the context class loader which belong to
   * the given package and subpackages.
   *
   * @param packageName The base package
   * @return The classes
   * @throws ClassNotFoundException
   * @throws IOException
   * @throws URISyntaxException
   */
  private static List<Class<?>> loadInternalClassesDev(String packageName) throws IOException, ClassNotFoundException {
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    assert classLoader != null;
    String path = packageName.replace('.', '/');
    Enumeration<URL> resources = classLoader.getResources(path);
    List<File> dirs = new ArrayList<File>();
    while (resources.hasMoreElements()) {
      URL resource = resources.nextElement();
      dirs.add(new File(resource.getFile()));
    }
    ArrayList<Class<?>> classes = new ArrayList<>();
    for (File directory : dirs) {
      classes.addAll(loadInternalClassesDevRec(directory, packageName));
    }
    return classes;
  }

  /**
   * Recursive method used to find all classes in a given directory and subdirs.
   *
   * @param directory   The base directory
   * @param packageName The package name for classes found inside the base
   *                    directory
   * @return The classes
   * @throws ClassNotFoundException
   */
  private static List<Class<?>> loadInternalClassesDevRec(File directory, String packageName)
      throws ClassNotFoundException {
    List<Class<?>> classes = new LinkedList<>();
    if (!directory.exists()) {
      return classes;
    }
    File[] files = directory.listFiles();
    for (File file : files) {
      if (file.isDirectory()) {
        assert !file.getName().contains(".");
        classes.addAll(loadInternalClassesDevRec(file, packageName + "." + file.getName()));
      } else if (file.getName().endsWith(".class")) {
        classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
      }
    }
    return classes;
  }

  public static List<Class<?>> loadExternalClasses(String dirPath)
      throws ClassNotFoundException, MalformedURLException {
    File dir = new File(dirPath);
    dir.mkdirs();
    List<Class<?>> classes = new LinkedList<>();

    // Convert File to a URL
    URL url = dir.toURI().toURL();
    // use this class's ClassLoader as parent for delegation
    ClassLoader classLoader = new URLClassLoader(new URL[] { url }, ClassUtil.class.getClassLoader());
    loadExternalClassesRec(classLoader, dir, "", classes);
    return classes;
  }

  private static void loadExternalClassesRec(ClassLoader classLoader, File dir, String packageName,
      List<Class<?>> classes_out) throws ClassNotFoundException {

    for (var file : dir.listFiles()) {
      if (file.isDirectory()) {
        // extend the package by the directoryName
        String newPackageName = packageName.isEmpty() ? file.getName() : packageName + '.' + file.getName();
        loadExternalClassesRec(classLoader, file, newPackageName, classes_out);
      } else if (file.getName().endsWith(".class")) {
        var className = packageName + '.' + file.getName();
        className = className.substring(0, className.length() - ".class".length());
        classes_out.add(classLoader.loadClass(className));
      }
      // other files will be ignored
    }
  }

}
