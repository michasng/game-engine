package de.hsa.dice.core;

import java.util.logging.Logger;

public abstract class Console {

  private static final Logger logger = Logger.getLogger(Console.class.getName());

  private static OutputLogger outputLogger;

  static {
    setOutputLogger(new DefaultOutputLogger());
  }

  public static void setOutputLogger(OutputLogger outputLogger) {
    Console.outputLogger = outputLogger;
  }

  public static void log(boolean message) {
    log(String.valueOf(message));
  }

  public static void log(int message) {
    log(String.valueOf(message));
  }

  public static void log(float message) {
    log(String.valueOf(message));
  }

  public static void log(String message) {
    outputLogger.log(message);
    logger.fine(message);
  }

  public static void logWarning(String warning) {
    outputLogger.logWarning(warning);
    logger.fine(warning);
  }

  public static void logError(String error) {
    outputLogger.logError(error);
    logger.fine(error);
  }

  public static void logError(Exception e) {
    outputLogger.logError(e);
    logger.fine(e.getMessage());
  }

}
