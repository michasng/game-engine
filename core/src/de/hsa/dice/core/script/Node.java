package de.hsa.dice.core.script;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Target(TYPE)
@Retention(RUNTIME)
public @interface Node {

  /**
   * How many inputs can/need to connect to this node, for it to run.
   */
  int numInputs();

  /**
   * Whether this node runs, even if all inputs are negative.
   */
  boolean alwaysRun() default false;

  /**
   * How to display the Node in the Editor.
   */
  String graphics() default "Warning"; // TODO: maybe replace graphics with enum values

  /**
   * Where to sort the node into in the Editor.
   */
  String category() default "Custom Nodes";

  /**
   * A short text describing what the node does
   */
  String description() default "";

}
