package de.hsa.dice.core.script;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Logger;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.util.ClassUtil;
import de.hsa.dice.core.util.io.ProjectFileOperator;

public class NodeRegistry {

  private static NodeRegistry instance;

  private static final Logger logger = Logger.getLogger(NodeRegistry.class.getName());

  private HashMap<String, NodeFactory> factories;

  public NodeRegistry() {
    factories = new HashMap<>();
    findNodes();
  }

  public void register(NodeFactory factory) {
    factories.put(factory.getTypeName(), factory);
  }

  public NodeFactory getFactory(String typeName) {
    return factories.get(typeName);
  }

  public Collection<NodeFactory> getFactories() {
    return factories.values();
  }

  public Collection<NodeFactory> getSortedFactories() {
    var factoryList = new LinkedList<>(factories.values());
    factoryList.sort((f1, f2) -> f1.getDisplayName().compareTo(f2.getDisplayName()));
    return factoryList;
  }

  public void findNodes() {
    loadInternalNodes();
    loadExternalNodes();
  }

  public void loadInternalNodes() {
    String packageName = "de.hsa.dice.nodes";

    try {
      var classes = ClassUtil.loadInternalClasses(packageName);
      addNodesFromClasses(classes);
    } catch (ClassNotFoundException | IOException | URISyntaxException e) {
      Console.logError(e);
    }
  }

  public void loadExternalNodes() {
    var nodesDirPath = ProjectFileOperator.getInstance().getNodesPath();
    try {
      var classes = ClassUtil.loadExternalClasses(nodesDirPath);
      addNodesFromClasses(classes);
    } catch (ClassNotFoundException | MalformedURLException e) {
      Console.logError(e);
    }
  }

  public void addNodesFromClasses(Iterable<Class<?>> classes) {
    for (Class<?> c : classes) {
      Node nodeAnnot = c.getDeclaredAnnotation(Node.class);
      if (nodeAnnot == null)
        continue;
      @SuppressWarnings("unchecked")
      var scriptClass = (Class<? extends ScriptNode>) c;
      LinkedList<Field> inspectedFields = new LinkedList<>();
      for (Field field : scriptClass.getDeclaredFields()) {
        var inspectAnnot = field.getDeclaredAnnotation(Inspect.class);
        if (inspectAnnot == null)
          continue;
        inspectedFields.add(field);
      }
      NodeFactory factory = new NodeFactory(scriptClass, nodeAnnot, inspectedFields);
      register(factory);
      logger.info("Initialized Node: " + factory.getTypeName());
    }
  }

  public static NodeRegistry getInstance() {
    if (instance == null)
      instance = new NodeRegistry();
    return instance;
  }

}
