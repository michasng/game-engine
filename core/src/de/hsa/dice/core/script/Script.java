package de.hsa.dice.core.script;

import java.util.LinkedList;
import java.util.logging.Logger;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.dataobject.ScriptData;
import de.hsa.dice.core.game.entity.Entity;
import de.hsa.dice.nodes.event.OnBeginNode;

public class Script {

  private static final Logger logger = Logger.getLogger(Script.class.getName());

  private ScriptData dataObject;

  private Entity target;

  private int numIds;

  private LinkedList<ScriptNode> nodes;
  private LinkedList<ScriptNode> beginNodes;

  public Script(ScriptData dataObject, Entity target) {
    this.dataObject = dataObject;
    this.target = target;
    this.nodes = new LinkedList<>();
    this.beginNodes = new LinkedList<>();

    NodeRegistry nodeReg = NodeRegistry.getInstance();
    for (var nodeData : dataObject.getNodes()) {
      NodeFactory factory = nodeReg.getFactory(nodeData.getType());
      if (factory == null) {
        Console.logError("Could not find node " + nodeData.getType());
        continue;
      }
      nodes.add(factory.createNode(this, nodeData));
      // nodes.add(ScriptNode.deserialize(target, nodeData));
    }

    logger.fine("Loading script: " + dataObject.getName());
    for (ScriptNode node : nodes) {
      var outputs = node.getDataObject().getOutputs();
      for (var output : outputs) {
        getNode(output.getTargetNodeId()).connectInput(node, output.getTargetPort());
      }

      if (node.getNumInputs() == 0) {
        if (node instanceof OnBeginNode)
          beginNodes.addFirst(node);
        else
          beginNodes.addLast(node);
      }
    }
  }

  public void update() {
    for (ScriptNode node : beginNodes) {
      node.reset();
    }
    for (ScriptNode node : beginNodes) {
      node.tryToInvoke();
    }
  }

  public int generateId() {
    return numIds++;
  }

  public void addNode(ScriptNode node) {
    nodes.add(node);
    node.setId(generateId());

    if (node.getNumInputs() == 0)
      beginNodes.add(node);
  }

  public void removeNode(ScriptNode node) {
    nodes.remove(node);
    beginNodes.remove(node);
  }

  public ScriptNode getNode(int id) {
    for (ScriptNode node : nodes) {
      if (node.getId() == id)
        return node;
    }
    return null;
  }

  public LinkedList<ScriptNode> getNodes() {
    return nodes;
  }

  public LinkedList<ScriptNode> getBeginNodes() {
    return beginNodes;
  }

  public Entity getTarget() {
    return target;
  }

  public ScriptData getDataObject() {
    return dataObject;
  }

}
