package de.hsa.dice.core.script;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.logging.Logger;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.dataobject.nested.FieldData;
import de.hsa.dice.core.dataobject.nested.NodeData;
import de.hsa.dice.core.game.entity.Entity;

public abstract class ScriptNode {

  private static final Logger logger = Logger.getLogger(ScriptNode.class.getName());

  private int id; // ids to make connections, starting at 1

  private ScriptNode[] inputNodes;
  private LinkedList<ScriptNode> outputNodes;

  private boolean value;
  private boolean hasExecuted;

  private NodeData dataObject;

  private Script script;

  protected ScriptNode() {
  }

  public void init(Script script, NodeData dataObject, int numInputs) {
    this.script = script;
    this.dataObject = dataObject;
    inputNodes = new ScriptNode[numInputs];
    outputNodes = new LinkedList<>();

    id = dataObject.getId();

    // set default values
    for (FieldData fieldData : dataObject.getFields()) {
      try {
        Field field = this.getClass().getDeclaredField(fieldData.getFieldName());
        try {
          field.setAccessible(true); // allow access to private fields
          field.set(this, fieldData.getValue());
        } catch (IllegalArgumentException | IllegalAccessException e) {
          logError("Could not set value " + fieldData.getFieldName() + " to " + fieldData.getValue());
        }
      } catch (NoSuchFieldException | SecurityException e) {
        logError("Field not found: " + fieldData.getFieldName());
      }
    }

  }

  public void updateVarFields() {
    for (FieldData fieldData : dataObject.getFields()) {
      if (!fieldData.isVariable())
        continue;

      try {
        Field field = this.getClass().getDeclaredField(fieldData.getFieldName());

        var objVar = getTarget().getVariable(fieldData.getVariable());
        if (objVar == null) {
          logError("Value of variable " + fieldData.getFieldName() + ":" + fieldData.getVariable() + " is null.");
          continue;
        }
        // convert numbers (or others) to string if needed
        if (field.getType().equals(String.class)) {
          objVar = objVar.toString();
        }

        // convert Strings to enums if needed
        if (field.getType().isEnum() && objVar instanceof String) {
          try {
            var valueOfMethod = field.getType().getDeclaredMethod("valueOf", String.class);
            valueOfMethod.setAccessible(true);
            // have the method call itself bc we are in a static context
            objVar = valueOfMethod.invoke(valueOfMethod, objVar);
          } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
              | InvocationTargetException e) {
            logError("Could not set field " + fieldData.getFieldName() + " from variable " + fieldData.getVariable()
                + ":" + objVar + " (invalid enum value).");
            continue;
          }
        }

        try {
          field.setAccessible(true); // allow access to private fields
          field.set(this, objVar);
        } catch (IllegalArgumentException | IllegalAccessException e) {
          logError("Could not set field " + fieldData.getFieldName() + " from variable " + fieldData.getVariable() + ":"
              + objVar);
        }

      } catch (NoSuchFieldException | SecurityException e) {
        logError("Field not found: " + fieldData.getFieldName());
      }
    }
  }

  public void setId(int id) {
    this.id = id;
  }

  public void connectInput(ScriptNode inputNode, int index) {
    if (inputNode == null) {
      logError("Invalid connection to " + getDataObject().getType() + " at index " + index);
      return;
    }
    inputNodes[index] = inputNode;
    logger.finest("connect input: " + inputNode.getId() + " index: " + index + " with " + getId());
    inputNode.outputNodes.add(this);
  }

  public void reset() {
    hasExecuted = false;
  }

  private boolean isSatisfied() {
    for (ScriptNode node : inputNodes) {
      if (node == null || !node.hasExecuted())
        return false;
    }
    return true;
  }

  public void tryToInvoke() {
    if (!isSatisfied() || hasExecuted)
      return;

    boolean inputTrue = false;
    for (int i = 0; i < getNumInputs(); i++)
      if (getInput(i))
        inputTrue = true;

    if (getNumInputs() == 0 || inputTrue || dataObject.getFactory().isAlwaysRun()) {
      updateVarFields();
      value = run();
    } else {
      value = false;
    }
    hasExecuted = true;

    for (ScriptNode node : outputNodes) {
      node.reset();
    }
    for (ScriptNode node : outputNodes) {
      node.tryToInvoke();
    }
  }

  protected abstract boolean run();

  public void logError(String error) {
    Console.logError("An error occurred in " + script.getDataObject().getName() + " - "
        + dataObject.getFactory().getDisplayName() + ":");
    Console.logError(error);
  }

  public boolean getInput(int index) {
    return inputNodes[index].getValue();
  }

  private boolean depthChecked;

  public int getMaxDepth() {
    depthChecked = true;
    var maxDepth = 0;
    for (var input : inputNodes) {
      if (input.depthChecked)
        continue;

      var inMax = input.getMaxDepth();
      if (inMax > maxDepth)
        maxDepth = inMax;
    }
    depthChecked = false;
    return maxDepth;
  }

  public int getId() {
    return id;
  }

  public ScriptNode[] getInputNodes() {
    return inputNodes;
  }

  public LinkedList<ScriptNode> getOutputNodes() {
    return outputNodes;
  }

  public boolean hasExecuted() {
    return hasExecuted;
  }

  public boolean getValue() {
    return value;
  }

  public int getNumInputs() {
    return inputNodes.length;
  }

  public Script getScript() {
    return script;
  }

  public Entity getTarget() {
    return script.getTarget();
  }

  public NodeData getDataObject() {
    return dataObject;
  }

}
