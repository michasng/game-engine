package de.hsa.dice.core.script;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;

import de.hsa.dice.core.dataobject.nested.NodeData;

public class NodeFactory {

  private Class<? extends ScriptNode> nodeClass;

  private Node nodeAnnot;

  private LinkedList<Field> inspectedFields;

  public NodeFactory(Class<? extends ScriptNode> nodeClass, Node nodeAnnot, LinkedList<Field> inspectedFields) {
    this.nodeClass = nodeClass;
    this.nodeAnnot = nodeAnnot;
    this.inspectedFields = inspectedFields;
  }

  public int getNumInputs() {
    return nodeAnnot.numInputs();
  }

  public boolean isAlwaysRun() {
    return nodeAnnot.alwaysRun();
  }

  public String getGraphics() {
    return nodeAnnot.graphics();
  }

  public String getCategory() {
    return nodeAnnot.category();
  }

  public String getDescription() {
    return nodeAnnot.description();
  }

  public LinkedList<Field> getInspectedFields() {
    return inspectedFields;
  }

  public Field getInspectedField(String fieldName) {
    for (var field : inspectedFields)
      if (field.getName().equals(fieldName))
        return field;
    return null;
  }

  public String getTypeName() {
    return nodeClass.getName();
  }

  /**
   * Takes the class's simple name and adds gaps after every lowercase letter
   * followed by an uppercase letter. For example "NodeFactory" becomes "Node
   * Factory" and "NavyCIS" becomes "Navy CIS". Also '_' is replaced by ' '. So
   * "Value_of" becomes "Value of".
   * 
   * @return a formatted version of the node's classname.
   */
  public String getDisplayName() {
    String simpleName = nodeClass.getSimpleName();
    char[] charArray = simpleName.toCharArray();
    int numGaps = 0;
    // start at 1 to ignore the first upper case letter letter
    for (int i = 1; i < charArray.length; i++) {
      char prevChar = charArray[i - 1];
      char currChar = charArray[i];
      if (prevChar >= 'a' && prevChar <= 'z' && currChar >= 'A' && currChar <= 'Z') {
        simpleName = simpleName.substring(0, i + numGaps) + " "
            + simpleName.substring(i + numGaps, simpleName.length());
        numGaps++;
      }
    }
    return simpleName.replace(' ', ' ');
  }

  public ScriptNode createNode(Script script, NodeData nodeData) {
    ScriptNode nodeInstance = null;
    try {
      nodeInstance = nodeClass.getDeclaredConstructor().newInstance();
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
        | NoSuchMethodException | SecurityException e) {
      e.printStackTrace();
      System.exit(1);
    }
    nodeInstance.init(script, nodeData, getNumInputs());

    return nodeInstance;
  }

}
