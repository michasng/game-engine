package de.hsa.dice.core.script;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Assigning Fields as inspected will bind their value to generated UI elements
 * in the editor. An inspected value will either keep it's value, if one is
 * assigned on instantiation or assign a default fallback value depending on the
 * type. Note: the default value for inspected Strings is "", not null! Use
 * String.isEmpty() to check for unchanged Strings.
 */
@Target(FIELD)
@Retention(RUNTIME)
public @interface Inspect {

  /**
   * Whether this field should always be displayed with a color corresponding to
   * it's values hash.
   */
  boolean colored() default false;

  /**
   * A short text describing what the field does
   */
  String description() default "";

}
