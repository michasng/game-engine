package de.hsa.dice.core.dataobject;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.manager.SoundDataManager;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.util.io.JsonObjectWrapper;

public class SoundData extends SubDataObject {

  private SoundDataManager manager;

  private Property<String> pathProperty;

  public SoundData(SoundDataManager manager) {
    super();
    this.manager = manager;

    pathProperty = new Property<>("path", "");

    // constraints:

    // successions:
    register(pathProperty);
  }

  public SoundData(JsonObject json, SoundDataManager manager) {
    this(manager);

    var wrapper = new JsonObjectWrapper(json);
    getNameProperty().setValue(wrapper.getString(getNameProperty()), false);
    pathProperty.setValue(wrapper.getString(pathProperty), false);
  }

  public String getPath() {
    return pathProperty.getValue();
  }

  public void setPath(String value) {
    pathProperty.setValue(value);
  }

  public Property<String> getPathProperty() {
    return pathProperty;
  }

  @Override
  public JsonObject toJson() {
    return new JsonObject().add(getNameProperty().getName(), getName()).add(pathProperty.getName(), getPath());
  }

  @Override
  public SoundDataManager getManager() {
    return manager;
  }

}
