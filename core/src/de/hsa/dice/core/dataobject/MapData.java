package de.hsa.dice.core.dataobject;

import java.util.LinkedList;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.manager.MapDataManager;
import de.hsa.dice.core.dataobject.nested.MapEntityData;
import de.hsa.dice.core.dataobject.nested.MapTileData;
import de.hsa.dice.core.property.InvalidPropertyException;
import de.hsa.dice.core.property.PropNotifyList;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.property.PropertyListener;
import de.hsa.dice.core.util.io.JsonObjectWrapper;

public class MapData extends SubDataObject {

  private MapDataManager manager;

  private Property<Integer> numColsProperty, numRowsProperty;
  private Property<LinkedList<MapTileData>> tileIdsProperty;
  private Property<int[][]> tilesProperty;
  private Property<PropNotifyList<MapEntityData>> entitiesProperty;
  private Property<Float> gravityProperty;

  public MapData(MapDataManager manager) {
    super();
    this.manager = manager;

    numColsProperty = new Property<>("numCols", 0);
    numRowsProperty = new Property<>("numRows", 0);
    tilesProperty = new Property<>("tiles", new int[0][0]);
    tileIdsProperty = new Property<>("tileIds", new LinkedList<>());
    entitiesProperty = new Property<>("entities", null);
    entitiesProperty.setValue(new PropNotifyList<>(entitiesProperty), false);
    gravityProperty = new Property<>("gravity", 9.81f);

    // constraints:
    numColsProperty.addListener((oldValue, value) -> {
      if (value < 1)
        throw new InvalidPropertyException("Number of columns must be at least 1. Invalid value: " + value);
    });
    numRowsProperty.addListener((oldValue, value) -> {
      if (value < 1)
        throw new InvalidPropertyException("Number of rows must be at least 1. Invalid value: " + value);
    });

    // successions:
    register(numColsProperty, numRowsProperty, tileIdsProperty, tilesProperty, entitiesProperty, gravityProperty);

    PropertyListener<Integer> tilesDimensionsListener = (oldValue, value) -> adjustTilesDimensions();
    numColsProperty.addListener(tilesDimensionsListener);
    numRowsProperty.addListener(tilesDimensionsListener);
  }

  public MapData(JsonObject json, MapDataManager manager) {
    this(manager);

    var wrapper = new JsonObjectWrapper(json);
    getNameProperty().setValue(wrapper.getString(getNameProperty()), false);
    numColsProperty.setValue(wrapper.getInt(numColsProperty), false);
    numRowsProperty.setValue(wrapper.getInt(numRowsProperty), false);
    gravityProperty.setValue(wrapper.getFloat(gravityProperty), false);

    JsonArray jsonTiles = json.get(tilesProperty.getName()).asArray();
    tilesProperty.setValue(new int[getNumRows()][getNumCols()], false);
    for (int row = 0; row < getNumRows(); row++) {
      JsonArray jsonTilesRow = jsonTiles.get(row).asArray();
      // result.tiles[row] = new int[result.numCols];
      for (int col = 0; col < getNumCols(); col++) {
        getTiles()[row][col] = jsonTilesRow.get(col).asInt();
      }
    }

    JsonArray jsonTileIds = json.get(tileIdsProperty.getName()).asArray();
    tileIdsProperty.setValue(new LinkedList<>(), false);
    for (int i = 0; i < jsonTileIds.size(); i++) {
      getTileIds().add(new MapTileData(this, jsonTileIds.get(i).asObject()));
    }

    getEntities().setNotifyEnabled(false);
    getEntities().clear();
    JsonArray jsonEntities = json.get(entitiesProperty.getName()).asArray();
    for (var jsonEntity : jsonEntities)
      getEntities().add(new MapEntityData(this, jsonEntity.asObject()));
    getEntities().setNotifyEnabled(true);
  }

  private void adjustTilesDimensions() {
    int[][] oldTiles = getTiles();
    int[][] newTiles = new int[getNumRows()][getNumCols()];

    for (int row = 0; row < oldTiles.length && row < getNumRows(); row++) {
      for (int col = 0; col < oldTiles[row].length && col < getNumCols(); col++) {
        newTiles[row][col] = oldTiles[row][col];
      }
    }
    setTiles(newTiles);
  }

  public int getNumCols() {
    return numColsProperty.getValue();
  }

  public void setNumCols(int value) {
    numColsProperty.setValue(value);
  }

  public Property<Integer> getNumColsProperty() {
    return numColsProperty;
  }

  public int getNumRows() {
    return numRowsProperty.getValue();
  }

  public void setNumRows(int value) {
    numRowsProperty.setValue(value);
  }

  public Property<Integer> getNumRowsProperty() {
    return numRowsProperty;
  }

  public int[][] getTiles() {
    return tilesProperty.getValue();
  }

  public void setTiles(int[][] value) {
    tilesProperty.setValue(value);
  }

  public Property<int[][]> getTilesProperty() {
    return tilesProperty;
  }

  public LinkedList<MapTileData> getTileIds() {
    return tileIdsProperty.getValue();
  }

  public void setTileIds(LinkedList<MapTileData> value) {
    tileIdsProperty.setValue(value);
  }

  public Property<LinkedList<MapTileData>> getTileIdsProperty() {
    return tileIdsProperty;
  }

  public PropNotifyList<MapEntityData> getEntities() {
    return entitiesProperty.getValue();
  }

  public void setEntities(PropNotifyList<MapEntityData> value) {
    entitiesProperty.setValue(value);
  }

  public Property<PropNotifyList<MapEntityData>> getEntitiesProperty() {
    return entitiesProperty;
  }

  public float getGravity() {
    return gravityProperty.getValue();
  }

  public void setGravity(float value) {
    gravityProperty.setValue(value);
  }

  public Property<Float> getGravityProperty() {
    return gravityProperty;
  }

  @Override
  public JsonObject toJson() {
    JsonArray jsonTiles = new JsonArray();
    for (int row = 0; row < getNumRows(); row++) {
      JsonArray jsonTilesRow = new JsonArray();
      for (int col = 0; col < getNumCols(); col++) {
        jsonTilesRow.add(getTiles()[row][col]);
      }
      jsonTiles.add(jsonTilesRow);
    }

    JsonArray jsonEntities = new JsonArray();
    for (MapEntityData entity : getEntities()) {
      jsonEntities.add(entity.toJson());
    }

    JsonArray jsonTileIds = new JsonArray();
    for (MapTileData tile : getTileIds()) {
      jsonTileIds.add(tile.toJson());
    }

    return new JsonObject().add(getNameProperty().getName(), getName()).add(numColsProperty.getName(), getNumCols())
        .add(numRowsProperty.getName(), getNumRows()).add(tilesProperty.getName(), jsonTiles)
        .add(tileIdsProperty.getName(), jsonTileIds).add(entitiesProperty.getName(), jsonEntities)
        .add(gravityProperty.getName(), getGravity());
  }

  @Override
  public MapDataManager getManager() {
    return manager;
  }

}
