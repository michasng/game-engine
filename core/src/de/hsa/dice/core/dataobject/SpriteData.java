package de.hsa.dice.core.dataobject;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.manager.SpriteDataManager;
import de.hsa.dice.core.dataobject.nested.SpriteFrameData;
import de.hsa.dice.core.property.PropNotifyList;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.util.io.JsonObjectWrapper;

public class SpriteData extends SubDataObject {

  private SpriteDataManager manager;

  private Property<Boolean> animatedProperty;

  private Property<PropNotifyList<SpriteFrameData>> framesProperty;

  public SpriteData(SpriteDataManager manager) {
    super();
    this.manager = manager;

    animatedProperty = new Property<>("animated", false);
    framesProperty = new Property<>("frames", null);
    framesProperty.setValue(new PropNotifyList<>(framesProperty), false);

    // constraints:

    // successions:
    register(framesProperty);
  }

  public SpriteData(JsonObject json, SpriteDataManager manager) {
    this(manager);

    var wrapper = new JsonObjectWrapper(json);

    getNameProperty().setValue(wrapper.getString(getNameProperty()), false);
    animatedProperty.setValue(wrapper.getBoolean(animatedProperty), false);
    JsonArray jsonFrames = json.get(framesProperty.getName()).asArray();
    getFrames().setNotifyEnabled(false);
    for (var jsonFrame : jsonFrames)
      getFrames().add(new SpriteFrameData(this, jsonFrame.asObject()));
    getFrames().setNotifyEnabled(true);
  }

  public boolean isAnimated() {
    return animatedProperty.getValue();
  }

  public void setAnimated(boolean value) {
    animatedProperty.setValue(value);
  }

  public Property<Boolean> getAnimatedProperty() {
    return animatedProperty;
  }

  public PropNotifyList<SpriteFrameData> getFrames() {
    return framesProperty.getValue();
  }

  public void setFrames(PropNotifyList<SpriteFrameData> value) {
    framesProperty.setValue(value);
  }

  public Property<PropNotifyList<SpriteFrameData>> getFramesProperty() {
    return framesProperty;
  }

  @Override
  public JsonObject toJson() {

    JsonArray jsonFrames = new JsonArray();
    for (var frame : getFrames()) {
      jsonFrames.add(frame.toJson());
    }

    return new JsonObject().add(getNameProperty().getName(), getName()).add(animatedProperty.getName(), isAnimated())
        .add(framesProperty.getName(), jsonFrames);
  }

  @Override
  public SpriteDataManager getManager() {
    return manager;
  }

}
