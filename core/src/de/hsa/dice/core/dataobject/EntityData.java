package de.hsa.dice.core.dataobject;

import java.util.LinkedList;

import org.jbox2d.dynamics.BodyType;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.dataobject.manager.EntityDataManager;
import de.hsa.dice.core.dataobject.manager.SubDataManager;
import de.hsa.dice.core.dataobject.nested.RectangleData;
import de.hsa.dice.core.dataobject.nested.ScriptCompData;
import de.hsa.dice.core.property.InvalidPropertyException;
import de.hsa.dice.core.property.PropNotifyList;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.util.io.JsonObjectWrapper;
import javafx.scene.paint.Color;

public class EntityData extends SubDataObject {

  private EntityDataManager manager;

  private Property<Float> widthProperty, heightProperty;
  private Property<BodyType> mobilityProperty;
  private Property<Boolean> contactsDisabledProperty;
  private Property<Float> densityProperty;
  private Property<Float> frictionProperty;
  private Property<Float> elasticityProperty;
  private Property<Float> rotationProperty;
  private Property<Boolean> rotationLockedProperty;
  private Property<GraphicsMode> graphicsModeProperty;
  private Property<String> colorProperty;
  private Property<String> spriteProperty;
  private Property<RectangleData> paddingsProperty;
  private Property<PropNotifyList<ScriptCompData>> scriptsProperty;

  public EntityData(EntityDataManager manager) {
    super();
    this.manager = manager;
    widthProperty = new Property<>("width", 1f);
    heightProperty = new Property<>("height", 1f);
    mobilityProperty = new Property<>("mobility", BodyType.DYNAMIC);
    contactsDisabledProperty = new Property<>("contactsDisabled", false);
    densityProperty = new Property<>("density", 1f);
    frictionProperty = new Property<>("friction", 0.5f);
    elasticityProperty = new Property<>("elasticity", 0f);
    rotationProperty = new Property<>("rotation", 0f);
    rotationLockedProperty = new Property<>("rotationLocked", false);
    graphicsModeProperty = new Property<>("graphicsMode", GraphicsMode.COLOR);
    colorProperty = new Property<>("color", "#FFFFFF");
    spriteProperty = new Property<>("sprite", "");
    paddingsProperty = new Property<>("paddings", new RectangleData(0, 0, 0, 0));
    scriptsProperty = new Property<>("scripts", null);
    scriptsProperty.setValue(new PropNotifyList<>(scriptsProperty), false);

    // constraints:
    widthProperty.addListener((oldValue, value) -> {
      if (value <= 0)
        throw new InvalidPropertyException("Width must be above 0. Invalid value: " + value);
    });
    heightProperty.addListener((oldValue, value) -> {
      if (value <= 0)
        throw new InvalidPropertyException("Height must be above 0. Invalid value: " + value);
    });

    frictionProperty.addListener((oldValue, value) -> {
      if (value < 0 || value > 1)
        Console.logWarning("Friction is recommended to be in range [0-1].");
    });

    elasticityProperty.addListener((oldValue, value) -> {
      if (value < 0 || value > 1)
        Console.logWarning("Elasticity is recommended to be in range [0-1].");
    });

    colorProperty.addListener((oldValue, value) -> {
      try {
        Color.web(value);
      } catch (IllegalArgumentException e) {
        throw new InvalidPropertyException("Invalid color format: " + value);
      }
    });

    // successions:
    register(widthProperty, heightProperty, mobilityProperty, contactsDisabledProperty, densityProperty,
        frictionProperty, elasticityProperty, rotationProperty, rotationLockedProperty, graphicsModeProperty,
        colorProperty, spriteProperty, paddingsProperty, scriptsProperty);
  }

  public EntityData(JsonObject json, EntityDataManager manager) {
    this(manager);

    var wrapper = new JsonObjectWrapper(json);
    getNameProperty().setValue(wrapper.getString(getNameProperty()), false);
    widthProperty.setValue(wrapper.getFloat(widthProperty), false);
    heightProperty.setValue(wrapper.getFloat(heightProperty), false);
    String mobilityStr = wrapper.getString(mobilityProperty.getName(), mobilityProperty.getValue().toString());
    mobilityProperty.setValue(BodyType.valueOf(mobilityStr), false);
    contactsDisabledProperty.setValue(wrapper.getBoolean(contactsDisabledProperty), false);
    densityProperty.setValue(wrapper.getFloat(densityProperty), false);
    frictionProperty.setValue(wrapper.getFloat(frictionProperty), false);
    elasticityProperty.setValue(wrapper.getFloat(elasticityProperty), false);
    rotationProperty.setValue(wrapper.getFloat(rotationProperty), false);
    rotationLockedProperty.setValue(wrapper.getBoolean(rotationLockedProperty), false);
    String graphicsModeStr = wrapper.getString(graphicsModeProperty.getName(),
        graphicsModeProperty.getValue().toString());
    graphicsModeProperty.setValue(GraphicsMode.valueOf(graphicsModeStr), false);
    colorProperty.setValue(wrapper.getString(colorProperty), false);
    spriteProperty.setValue(wrapper.getString(spriteProperty), false);
    var paddingsJson = wrapper.getObject(paddingsProperty.getName(), null);
    if (paddingsJson != null)
      paddingsProperty.setValue(new RectangleData(paddingsJson), false);
    scriptsProperty.getValue().setNotifyEnabled(false); // insert items without constantly notifying listeners
    JsonArray jsonScripts = wrapper.getArray(scriptsProperty.getName(), new JsonArray());
    for (var jsonScript : jsonScripts)
      scriptsProperty.getValue().add(new ScriptCompData(this, jsonScript.asObject()));
    scriptsProperty.getValue().setNotifyEnabled(true);
  }

  public float getWidth() {
    return widthProperty.getValue();
  }

  public void setWidth(float value) {
    widthProperty.setValue(value);
  }

  public Property<Float> getWidthProperty() {
    return widthProperty;
  }

  public float getHeight() {
    return heightProperty.getValue();
  }

  public void setHeight(float value) {
    heightProperty.setValue(value);
  }

  public Property<Float> getHeightProperty() {
    return heightProperty;
  }

  public BodyType getMobility() {
    return mobilityProperty.getValue();
  }

  public void setMobility(BodyType value) {
    mobilityProperty.setValue(value);
  }

  public Property<BodyType> getMobilityProperty() {
    return mobilityProperty;
  }

  public boolean isContactsDisabled() {
    return contactsDisabledProperty.getValue();
  }

  public void setContactsDisabled(boolean value) {
    contactsDisabledProperty.setValue(value);
  }

  public Property<Boolean> getContactsDisabledProperty() {
    return contactsDisabledProperty;
  }

  public float getDensity() {
    return densityProperty.getValue();
  }

  public void setDensity(float value) {
    densityProperty.setValue(value);
  }

  public Property<Float> getDensityProperty() {
    return densityProperty;
  }

  public float getFriction() {
    return frictionProperty.getValue();
  }

  public void setFriction(float value) {
    frictionProperty.setValue(value);
  }

  public Property<Float> getFrictionProperty() {
    return frictionProperty;
  }

  public float getElasticity() {
    return elasticityProperty.getValue();
  }

  public void setElasticity(float value) {
    elasticityProperty.setValue(value);
  }

  public Property<Float> getElasticityProperty() {
    return elasticityProperty;
  }

  public float getRotation() {
    return rotationProperty.getValue();
  }

  public void setRotation(float value) {
    rotationProperty.setValue(value);
  }

  public Property<Float> getRotationProperty() {
    return rotationProperty;
  }

  public Boolean isRotationLocked() {
    return rotationLockedProperty.getValue();
  }

  public void setRotationLocked(Boolean value) {
    rotationLockedProperty.setValue(value);
  }

  public Property<Boolean> getRotationLockedProperty() {
    return rotationLockedProperty;
  }

  public GraphicsMode getGraphicsMode() {
    return graphicsModeProperty.getValue();
  }

  public void setGraphicsMode(GraphicsMode value) {
    graphicsModeProperty.setValue(value);
  }

  public Property<GraphicsMode> getGraphicsModeProperty() {
    return graphicsModeProperty;
  }

  public String getColor() {
    return colorProperty.getValue();
  }

  public void setColor(String value) {
    colorProperty.setValue(value);
  }

  public Property<String> getColorProperty() {
    return colorProperty;
  }

  public String getSprite() {
    return spriteProperty.getValue();
  }

  public void setSprite(String value) {
    spriteProperty.setValue(value);
  }

  public Property<String> getSpriteProperty() {
    return spriteProperty;
  }

  public RectangleData getPaddings() {
    return paddingsProperty.getValue();
  }

  public void setPaddings(RectangleData value) {
    paddingsProperty.setValue(value);
  }

  public Property<RectangleData> getPaddingsProperty() {
    return paddingsProperty;
  }

  public PropNotifyList<ScriptCompData> getScripts() {
    return scriptsProperty.getValue();
  }

  public void setScripts(PropNotifyList<ScriptCompData> value) {
    scriptsProperty.setValue(value);
  }

  public Property<PropNotifyList<ScriptCompData>> getScriptsProperty() {
    return scriptsProperty;
  }

  public LinkedList<ScriptData> getScriptDatas() {
    SubDataManager<ScriptData> scriptManager = manager.getProjectData().getSubManager(DataType.SCRIPT,
        ScriptData.class);
    LinkedList<String> scriptNames = new LinkedList<>();
    for (var scriptComp : getScripts())
      scriptNames.add(scriptComp.getName());
    return scriptManager.getAll(scriptNames);
  }

  @Override
  public JsonObject toJson() {
    JsonArray jsonScripts = new JsonArray();
    for (var script : getScripts())
      jsonScripts.add(script.toJson());

    return new JsonObject().add(getNameProperty().getName(), getName()).add(widthProperty.getName(), getWidth())
        .add(heightProperty.getName(), getHeight()).add(mobilityProperty.getName(), getMobility().toString())
        .add(contactsDisabledProperty.getName(), isContactsDisabled()).add(densityProperty.getName(), getDensity())
        .add(frictionProperty.getName(), getFriction()).add(elasticityProperty.getName(), getElasticity())
        .add(rotationProperty.getName(), getRotation()).add(rotationLockedProperty.getName(), isRotationLocked())
        .add(graphicsModeProperty.getName(), getGraphicsMode().toString()).add(colorProperty.getName(), getColor())
        .add(spriteProperty.getName(), getSprite()).add(paddingsProperty.getName(), getPaddings().toJson())
        .add(scriptsProperty.getName(), jsonScripts);
  }

  @Override
  public EntityDataManager getManager() {
    return manager;
  }

  public static enum GraphicsMode {
    COLOR, SPRITE, INVISIBLE;
  }

}
