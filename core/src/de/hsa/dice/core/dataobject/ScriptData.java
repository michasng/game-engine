package de.hsa.dice.core.dataobject;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.manager.ScriptDataManager;
import de.hsa.dice.core.dataobject.nested.NodeData;
import de.hsa.dice.core.property.PropNotifyList;
import de.hsa.dice.core.property.Property;

public class ScriptData extends SubDataObject {

  private ScriptDataManager manager;

  private Property<PropNotifyList<NodeData>> nodesProperty;

  public ScriptData(ScriptDataManager manager) {
    super();
    this.manager = manager;

    nodesProperty = new Property<>("nodes", null);
    nodesProperty.setValue(new PropNotifyList<>(nodesProperty), false);

    // constraints:

    // successions:
    register(nodesProperty);
  }

  public ScriptData(JsonObject json, ScriptDataManager manager) {
    this(manager);

    getNameProperty().setValue(json.get(getNameProperty().getName()).asString(), false);

    JsonArray jsonNodes = json.get(nodesProperty.getName()).asArray();
    getNodes().setNotifyEnabled(false);
    for (var jsonNode : jsonNodes)
      getNodes().add(new NodeData(this, jsonNode.asObject()));
    getNodes().setNotifyEnabled(true);
  }

  public PropNotifyList<NodeData> getNodes() {
    return nodesProperty.getValue();
  }

  public void setNodes(PropNotifyList<NodeData> value) {
    nodesProperty.setValue(value);
  }

  public Property<PropNotifyList<NodeData>> getNodesProperty() {
    return nodesProperty;
  }

  @Override
  public JsonObject toJson() {

    JsonArray jsonNodes = new JsonArray();
    for (NodeData node : getNodes()) {
      jsonNodes.add(node.toJson());
    }

    return new JsonObject().add(getNameProperty().getName(), getName()).add(nodesProperty.getName(), jsonNodes);
  }

  @Override
  public ScriptDataManager getManager() {
    return manager;
  }

}
