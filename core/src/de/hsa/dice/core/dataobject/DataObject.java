package de.hsa.dice.core.dataobject;

import java.util.LinkedList;

import de.hsa.dice.core.dataobject.nested.DataObjectParent;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.util.io.JsonSerializable;

public abstract class DataObject implements JsonSerializable, DataObjectParent {

  /*
   * The current name of the DataObject. Set to null when DataObject should be
   * deleted.
   */
  private Property<String> nameProperty;

  /*
   * Whether the DataObject has been modified after loading, i.e. it needs to be
   * saved. Is not JsonSerialized.
   */
  private Property<Boolean> dirtyProperty;

  /*
   * A counter from 0, for how many times the dirtyProperty was set to true. Is
   * not JsonSerialized.
   */
  private Property<Integer> revisionProperty;

  private LinkedList<Property<?>> properties;

  DataObject() {
    nameProperty = new Property<>("name", null);
    dirtyProperty = new Property<>("dirty", false);
    revisionProperty = new Property<>("revision", 0);

    properties = new LinkedList<>();
    register(nameProperty);
  }

  public void register(Property<?>... newProps) {
    for (var prop : newProps) {
      properties.add(prop);
      prop.addListener((oldValue, value) -> {
        setDirty(true);
      });
    }
  }

  public final String getName() {
    return nameProperty.getValue();
  }

  public final void setName(String value) {
    nameProperty.setValue(value);
  }

  public final Property<String> getNameProperty() {
    return nameProperty;
  }

  public final boolean isDirty() {
    return dirtyProperty.getValue();
  }

  public final void setDirty(boolean value) {
    if (value == true)
      setRevision(getRevision() + 1);
    dirtyProperty.setValue(value);
  }

  public final Property<Boolean> getDirtyProperty() {
    return dirtyProperty;
  }

  public final int getRevision() {
    return revisionProperty.getValue();
  }

  private final void setRevision(int value) {
    revisionProperty.setValue(value);
  }

  public final Property<Integer> getRevisionProperty() {
    return revisionProperty;
  }

  public abstract DataType getDataType();

}
