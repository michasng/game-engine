package de.hsa.dice.core.dataobject;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.dataobject.EntityData.GraphicsMode;
import de.hsa.dice.core.dataobject.manager.TileDataManager;
import de.hsa.dice.core.dataobject.nested.RectangleData;
import de.hsa.dice.core.property.InvalidPropertyException;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.util.io.JsonObjectWrapper;
import javafx.scene.paint.Color;

public class TileData extends SubDataObject {

  private TileDataManager manager;

  private Property<Boolean> solidProperty;
  private Property<Float> densityProperty;
  private Property<Float> frictionProperty;
  private Property<Float> elasticityProperty;
  private Property<GraphicsMode> graphicsModeProperty;
  private Property<String> colorProperty;
  private Property<String> spriteProperty;
  private Property<RectangleData> paddingsProperty;

  public TileData(TileDataManager manager) {
    super();
    this.manager = manager;
    solidProperty = new Property<>("solid", true);
    densityProperty = new Property<>("density", 1f);
    frictionProperty = new Property<>("friction", 0.5f);
    elasticityProperty = new Property<>("elasticity", 0f);
    graphicsModeProperty = new Property<>("graphicsMode", GraphicsMode.COLOR);
    colorProperty = new Property<>("color", "#FFFFFF");
    spriteProperty = new Property<>("sprite", "");
    paddingsProperty = new Property<>("paddings", new RectangleData(0, 0, 0, 0));

    // constraints:
    frictionProperty.addListener((oldValue, value) -> {
      if (value < 0 || value > 1)
        Console.logWarning("Friction is recommended to be in range [0-1].");
    });

    elasticityProperty.addListener((oldValue, value) -> {
      if (value < 0 || value > 1)
        Console.logWarning("Elasticity is recommended to be in range [0-1].");
    });

    colorProperty.addListener((oldValue, value) -> {
      try {
        Color.web(value);
      } catch (IllegalArgumentException e) {
        throw new InvalidPropertyException("Invalid color format: " + value);
      }
    });

    // successions:
    register(solidProperty, densityProperty, frictionProperty, elasticityProperty, graphicsModeProperty, colorProperty,
        spriteProperty, paddingsProperty);
  }

  public TileData(JsonObject json, TileDataManager manager) {
    this(manager);

    var jsonWrapper = new JsonObjectWrapper(json);
    getNameProperty().setValue(jsonWrapper.getString(getNameProperty()), false);
    solidProperty.setValue(jsonWrapper.getBoolean(solidProperty), false);
    densityProperty.setValue(jsonWrapper.getFloat(densityProperty), false);
    frictionProperty.setValue(jsonWrapper.getFloat(frictionProperty), false);
    elasticityProperty.setValue(jsonWrapper.getFloat(elasticityProperty), false);
    String graphicsModeStr = jsonWrapper.getString(graphicsModeProperty.getName(),
        graphicsModeProperty.getValue().toString());
    graphicsModeProperty.setValue(GraphicsMode.valueOf(graphicsModeStr), false);
    colorProperty.setValue(jsonWrapper.getString(colorProperty), false);
    spriteProperty.setValue(jsonWrapper.getString(spriteProperty), false);
    var paddingsJson = jsonWrapper.getObject(paddingsProperty.getName(), null);
    if (paddingsJson != null)
      paddingsProperty.setValue(new RectangleData(paddingsJson), false);
  }

  public boolean isSolid() {
    return solidProperty.getValue();
  }

  public void setSolid(boolean value) {
    solidProperty.setValue(value);
  }

  public Property<Boolean> getSolidProperty() {
    return solidProperty;
  }

  public float getDensity() {
    return densityProperty.getValue();
  }

  public void setDensity(float value) {
    densityProperty.setValue(value);
  }

  public Property<Float> getDensityProperty() {
    return densityProperty;
  }

  public float getFriction() {
    return frictionProperty.getValue();
  }

  public void setFriction(float value) {
    frictionProperty.setValue(value);
  }

  public Property<Float> getFrictionProperty() {
    return frictionProperty;
  }

  public float getElasticity() {
    return elasticityProperty.getValue();
  }

  public void setElasticity(float value) {
    elasticityProperty.setValue(value);
  }

  public Property<Float> getElasticityProperty() {
    return elasticityProperty;
  }

  public GraphicsMode getGraphicsMode() {
    return graphicsModeProperty.getValue();
  }

  public void setGraphicsMode(GraphicsMode value) {
    graphicsModeProperty.setValue(value);
  }

  public Property<GraphicsMode> getGraphicsModeProperty() {
    return graphicsModeProperty;
  }

  public String getColor() {
    return colorProperty.getValue();
  }

  public void setColor(String value) {
    colorProperty.setValue(value);
  }

  public Property<String> getColorProperty() {
    return colorProperty;
  }

  public String getSprite() {
    return spriteProperty.getValue();
  }

  public void setSprite(String value) {
    spriteProperty.setValue(value);
  }

  public Property<String> getSpriteProperty() {
    return spriteProperty;
  }

  public RectangleData getPaddings() {
    return paddingsProperty.getValue();
  }

  public void setPaddings(RectangleData value) {
    paddingsProperty.setValue(value);
  }

  public Property<RectangleData> getPaddingsProperty() {
    return paddingsProperty;
  }

  @Override
  public JsonObject toJson() {
    return new JsonObject().add(getNameProperty().getName(), getName()).add(solidProperty.getName(), isSolid())
        .add(densityProperty.getName(), getDensity()).add(frictionProperty.getName(), getFriction())
        .add(elasticityProperty.getName(), getElasticity())
        .add(graphicsModeProperty.getName(), getGraphicsMode().toString()).add(colorProperty.getName(), getColor())
        .add(spriteProperty.getName(), getSprite()).add(paddingsProperty.getName(), getPaddings().toJson());
  }

  @Override
  public TileDataManager getManager() {
    return manager;
  }

}
