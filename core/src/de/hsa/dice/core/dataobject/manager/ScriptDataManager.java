package de.hsa.dice.core.dataobject.manager;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.ScriptData;

public class ScriptDataManager extends SubDataManager<ScriptData> {

	public ScriptDataManager(ProjectData projectData) {
		super(projectData);
	}

	@Override
	protected ScriptData fromJson(JsonObject json) {
		return new ScriptData(json, this);
	}

	@Override
	public ScriptData createNewImpl() {
		return new ScriptData(this);
	}

	@Override
	public DataType getDataType() {
		return DataType.SCRIPT;
	}

}
