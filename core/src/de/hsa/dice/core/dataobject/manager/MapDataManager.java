package de.hsa.dice.core.dataobject.manager;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.MapData;
import de.hsa.dice.core.dataobject.ProjectData;

public class MapDataManager extends SubDataManager<MapData> {

  public MapDataManager(ProjectData projectData) {
    super(projectData);
  }

  @Override
  protected MapData fromJson(JsonObject json) {
    return new MapData(json, this);
  }

  @Override
  public MapData createNewImpl() {
    MapData mapData = new MapData(this);
    mapData.setNumCols(16);
    mapData.setNumRows(16);
    return mapData;
  }

  @Override
  public DataType getDataType() {
    return DataType.MAP;
  }

}
