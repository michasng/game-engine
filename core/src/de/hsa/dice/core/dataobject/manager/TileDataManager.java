package de.hsa.dice.core.dataobject.manager;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.TileData;

public class TileDataManager extends SubDataManager<TileData> {

  public TileDataManager(ProjectData projectData) {
    super(projectData);
  }

  @Override
  protected TileData fromJson(JsonObject json) {
    return new TileData(json, this);
  }

  @Override
  public TileData createNewImpl() {
    return new TileData(this);
  }

  @Override
  public DataType getDataType() {
    return DataType.TILE;
  }

}
