package de.hsa.dice.core.dataobject.manager;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.SoundData;

public class SoundDataManager extends SubDataManager<SoundData> {

  public SoundDataManager(ProjectData projectData) {
    super(projectData);
  }

  @Override
  protected SoundData fromJson(JsonObject json) {
    return new SoundData(json, this);
  }

  @Override
  public SoundData createNewImpl() {
    return new SoundData(this);
  }

  @Override
  public DataType getDataType() {
    return DataType.SOUND;
  }

}
