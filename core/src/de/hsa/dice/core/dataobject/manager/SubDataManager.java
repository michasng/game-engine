package de.hsa.dice.core.dataobject.manager;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.SubDataObject;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.property.PropertySubscription;
import de.hsa.dice.core.util.io.ProjectFileOperator;

public abstract class SubDataManager<T extends SubDataObject> implements DataManager {

  private ProjectData projectData;

  private Property<HashMap<String, T>> dataObjectsProperty;

  private int revision;
  private LinkedList<PropertySubscription<?>> subscriptions;

  private Property<Boolean> doneLoadingProperty;

  protected SubDataManager(ProjectData projectData) {
    this.projectData = projectData;
    dataObjectsProperty = new Property<>("dataObjects", new HashMap<>());
    revision = 0;
    subscriptions = new LinkedList<>();
    doneLoadingProperty = new Property<>("doneLoading", false);
  }

  @SuppressWarnings("unchecked")
  public void setDataObjects(HashMap<String, T> dataObjects) {
    clear();
    this.dataObjectsProperty.setValue((HashMap<String, T>) dataObjects.clone());
    subscribeAll();
    revision++;
  }

  public HashMap<String, T> getDataObjects() {
    return dataObjectsProperty.getValue();
  }

  public Property<HashMap<String, T>> getDataObjectsProperty() {
    return dataObjectsProperty;
  }

  public void save() {
    var projectFileOperator = ProjectFileOperator.getInstance();
    projectFileOperator.clearDataTypeDir(getDataType());
    for (T dataObject : getDataObjects().values()) {
      projectFileOperator.saveDataObject(dataObject);
    }
  }

  public void reload() {
    String dataTypePath = ProjectFileOperator.getInstance().getDataTypePath(getDataType());
    File dir = new File(dataTypePath);
    dir.mkdirs();
    if (!dir.isDirectory())
      throw new RuntimeException(dataTypePath + " is not a directory");

    File[] files = dir.listFiles();

    getDataObjects().clear();
    for (File file : files) {
      T dataObject = fromJson(ProjectFileOperator.getInstance().loadDataObjectJson(getDataType(), file.getName()));
      add(dataObject);
    }
    doneLoadingProperty.setValue(true);
    doneLoadingProperty.clearListeners();
  }

  public void runOnDoneLoading(Runnable runnable) {
    if (doneLoadingProperty.getValue() == true) {
      runnable.run();
      return;
    }
    doneLoadingProperty.addListener((oldValue, value) -> runnable.run());
  }

  public void onDestroy() {
    clear();
  }

  public void add(final T data) {
    getDataObjects().put(data.getName(), data);
    dataObjectsProperty.setValue(getDataObjects()); // notify listeners, TODO: similar structure to PropNotifyList
    subscribe(data);
    revision++;
  }

  private void subscribeAll() {
    for (var sub : subscriptions)
      sub.cancelSubscription();
    subscriptions.clear();

    for (var data : getDataObjects().values())
      subscribe(data);
  }

  private void subscribe(T data) {
    PropertySubscription<?> sub = data.getNameProperty().addListener((oldName, newName) -> {
      if (newName.equals(oldName))
        return;

      getDataObjects().remove(oldName);
      getDataObjects().put(newName, data);
    });
    subscriptions.add(sub);

    sub = data.getDirtyProperty().addListener((oldValue, value) -> {
      if (value == true)
        revision++;
    });
    subscriptions.add(sub);
  }

  public void remove(T data) {
    getDataObjects().remove(data.getName());

  }

  public void remove(String name) {
    var data = getDataObjects().remove(name); // file will be deleted on the next call to save()
    var subsToCancel = new LinkedList<PropertySubscription<?>>();
    for (var sub : subscriptions) {
      if (sub.getProperty() == data.getNameProperty() || sub.getProperty() == data.getDirtyProperty()) {
        subsToCancel.add(sub);
      }
    }
    for (var subToCancel : subsToCancel) {
      subToCancel.cancelSubscription();
      subscriptions.remove(subToCancel);
    }
    revision++;
    dataObjectsProperty.setValue(getDataObjects()); // notify listeners, TODO: similar structure to PropNotifyList
  }

  public void clear() {
    for (var sub : subscriptions)
      sub.cancelSubscription();
    subscriptions.clear();

    getDataObjects().clear();
  }

  public T get(String name) {
    return getDataObjects().get(name);
  }

  public LinkedList<T> getAll(LinkedList<String> names) {
    var foundDataObjects = new LinkedList<T>();
    for (String name : names) {
      T current = get(name);
      if (current == null)
        Console.logError("Missing " + getDataType() + ": " + name);
      else
        foundDataObjects.add(current);
    }
    return foundDataObjects;
  }

  public Collection<String> getNames() {
    var dataObjectNames = new LinkedList<String>();
    for (T data : getDataObjects().values()) {
      dataObjectNames.add(data.getName());
    }
    return dataObjectNames;
  }

  protected String makeNewName() {
    return makeNewNameFrom(getDataType().name);
  }

  public String makeNewNameFrom(String baseName) {
    T dataObject;
    String objectName = baseName;
    int objectNum = 0;
    do {
      objectNum++;
      objectName = baseName + objectNum;
      dataObject = get(objectName);
    } while (dataObject != null);
    return objectName;
  }

  public final T createNew() {
    var dataObject = createNewImpl();
    dataObject.getNameProperty().setValue(makeNewName());
    dataObject.setDirty(true);
    add(dataObject);
    return dataObject;
  }

  public T duplicateFromJson(String originalName, JsonObject json) {
    var newDataObject = fromJson(json);
    newDataObject.setName(makeNewNameFrom(originalName));
    add(newDataObject);
    return newDataObject;
  }

  public ProjectData getProjectData() {
    return projectData;
  }

  public boolean isDirty() {
    for (var dataObject : getDataObjects().values()) {
      if (dataObject.isDirty())
        return true;
    }
    return false;
  }

  public int getRevision() {
    return revision;
  }

  protected abstract T createNewImpl();

  protected abstract T fromJson(JsonObject json);

}
