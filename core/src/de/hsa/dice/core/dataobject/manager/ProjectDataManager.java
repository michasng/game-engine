package de.hsa.dice.core.dataobject.manager;

import java.io.File;
import java.util.logging.Logger;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.util.io.ProjectFileOperator;

public class ProjectDataManager implements DataManager {

  private static final Logger logger = Logger.getLogger(ProjectDataManager.class.getName());

  private static ProjectDataManager instance;

  private ProjectData currentProject;

  private ProjectDataManager() {
  }

  private void init() {
    instance.reload();
  }

  @Override
  public void save() {
    var projectFileOperator = ProjectFileOperator.getInstance();
    projectFileOperator.saveProjectData(currentProject);

    for (var manager : currentProject.getSubManagers())
      manager.save();
  }

  @Override
  public boolean isDirty() {
    if (currentProject.isDirty())
      return true;

    for (var manager : currentProject.getSubManagers()) {
      if (manager.isDirty()) {
        logger.finest("changes detected in datatype: " + manager.getDataType());
        return true;
      }
    }
    return false;
  }

  @Override
  public void reload() {
    var projectFileOperator = ProjectFileOperator.getInstance();
    var projectPath = projectFileOperator.getProjectPath();
    var dir = new File(projectPath);
    dir.mkdirs();
    if (!dir.isDirectory())
      throw new RuntimeException(projectPath + " is not a directory");

    ProjectData projectData;
    var jsonObj = projectFileOperator.loadProjectDataFile();
    if (jsonObj != null)
      projectData = fromJson(jsonObj);
    else
      projectData = createNew();
    currentProject = projectData;

    for (var manager : currentProject.getSubManagers())
      manager.reload();
  }

  private ProjectData fromJson(JsonObject json) {
    return new ProjectData(json);
  }

  private ProjectData createNew() {
    var projectData = new ProjectData();
    var projectName = ProjectFileOperator.getInstance().getProjectName();
    projectData.getNameProperty().setValue(projectName);
    projectData.setDirty(true);
    return projectData;
  }

  public ProjectData getCurrentProject() {
    return currentProject;
  }

  public boolean startMapExists() {
    for (var mapObjectName : currentProject.getSubManager(DataType.MAP).getNames())
      if (mapObjectName.equals(getCurrentProject().getStartMapName()))
        return true;
    return false;
  }

  @Override
  public DataType getDataType() {
    return DataType.PROJECT;
  }

  public static ProjectDataManager getInstance() {
    if (instance == null) {
      instance = new ProjectDataManager();
      instance.init();
    }

    return instance;
  }

}
