package de.hsa.dice.core.dataobject.manager;

import de.hsa.dice.core.dataobject.DataType;

public interface DataManager {

  void save();

  void reload();

  boolean isDirty();

  DataType getDataType();

}
