package de.hsa.dice.core.dataobject.manager;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.EntityData;
import de.hsa.dice.core.dataobject.ProjectData;

public class EntityDataManager extends SubDataManager<EntityData> {

	public EntityDataManager(ProjectData projectData) {
		super(projectData);
	}

	@Override
	protected EntityData fromJson(JsonObject json) {
		return new EntityData(json, this);
	}

	@Override
	public EntityData createNewImpl() {
		return new EntityData(this);
	}

	@Override
	public DataType getDataType() {
		return DataType.ENTITY;
	}

}
