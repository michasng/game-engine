package de.hsa.dice.core.dataobject.manager;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.SpriteData;

public class SpriteDataManager extends SubDataManager<SpriteData> {

  public SpriteDataManager(ProjectData projectData) {
    super(projectData);
  }

  @Override
  protected SpriteData fromJson(JsonObject json) {
    return new SpriteData(json, this);
  }

  @Override
  public SpriteData createNewImpl() {
    return new SpriteData(this);
  }

  @Override
  public DataType getDataType() {
    return DataType.SPRITE;
  }

}
