package de.hsa.dice.core.dataobject;

public enum DataType {

  PROJECT("ProjectData", "Projects"), SPRITE("Sprite", "Sprites"), TILE("Tile", "Tiles"), MAP("Map", "Maps"),
  ENTITY("Entity", "Entities"), SOUND("Sound", "Sounds"), SCRIPT("Script", "Scripts");

  public final String name, pluralName;

  private DataType(String name, String pluralName) {
    this.name = name;
    this.pluralName = pluralName;
  }

}
