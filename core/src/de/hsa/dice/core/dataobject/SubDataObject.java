package de.hsa.dice.core.dataobject;

import java.util.Collection;

import de.hsa.dice.core.dataobject.manager.SubDataManager;
import de.hsa.dice.core.property.InvalidPropertyException;

public abstract class SubDataObject extends DataObject {

  public SubDataObject() {
    // constraints:
    getNameProperty().addListener((oldValue, value) -> {
      if (value.equals(oldValue))
        return;

      // if the name already existed, cancel renaming
      Collection<String> objectNames = getManager().getNames();
      boolean foundOnce = false;
      for (String name : objectNames) {
        if (name.equals(value)) {
          if (!foundOnce)
            foundOnce = true;
          else
            throw new InvalidPropertyException("The name " + value + " already exists for " + getDataType().name);
        }
      }
    });
  }

  @Override
  public final DataType getDataType() {
    return getManager().getDataType();
  }

  public abstract SubDataManager<?> getManager();

}
