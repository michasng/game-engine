package de.hsa.dice.core.dataobject;

import java.util.Collection;
import java.util.EnumMap;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.manager.EntityDataManager;
import de.hsa.dice.core.dataobject.manager.MapDataManager;
import de.hsa.dice.core.dataobject.manager.ProjectDataManager;
import de.hsa.dice.core.dataobject.manager.ScriptDataManager;
import de.hsa.dice.core.dataobject.manager.SoundDataManager;
import de.hsa.dice.core.dataobject.manager.SpriteDataManager;
import de.hsa.dice.core.dataobject.manager.SubDataManager;
import de.hsa.dice.core.dataobject.manager.TileDataManager;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.util.io.JsonObjectWrapper;

@SuppressWarnings("unused")
public class ProjectData extends DataObject {

  private EnumMap<DataType, SubDataManager<?>> subManagers;

  private Property<String> startMapNameProperty;
  private Property<Integer> pixelPerMeterProperty;
  private Property<Boolean> maximizedProperty;
  private Property<Boolean> fullscreenProperty;
  private Property<String> themeProperty;

  public ProjectData() {
    super();

    subManagers = new EnumMap<>(DataType.class);
    subManagers.put(DataType.MAP, new MapDataManager(this));
    subManagers.put(DataType.TILE, new TileDataManager(this));
    subManagers.put(DataType.ENTITY, new EntityDataManager(this));
    subManagers.put(DataType.SCRIPT, new ScriptDataManager(this));
    subManagers.put(DataType.SPRITE, new SpriteDataManager(this));
    subManagers.put(DataType.SOUND, new SoundDataManager(this));

    startMapNameProperty = new Property<>("startMapName", "");
    pixelPerMeterProperty = new Property<>("pixelPerMeter", 32);
    maximizedProperty = new Property<>("maximized", false);
    fullscreenProperty = new Property<>("fullscreen", false);
    themeProperty = new Property<>("theme", "#EEEEEE");

    register(startMapNameProperty);
  }

  public ProjectData(JsonObject json) {
    this();

    var wrapper = new JsonObjectWrapper(json);

    getNameProperty().setValue(wrapper.getString(getNameProperty()), false);
    startMapNameProperty.setValue(wrapper.getString(startMapNameProperty), false);
    pixelPerMeterProperty.setValue(wrapper.getInt(pixelPerMeterProperty), false);
    maximizedProperty.setValue(wrapper.getBoolean(maximizedProperty), false);
    fullscreenProperty.setValue(wrapper.getBoolean(fullscreenProperty), false);
    themeProperty.setValue(wrapper.getString(themeProperty), false);
  }

  public Collection<SubDataManager<?>> getSubManagers() {
    return subManagers.values();
  }

  public SubDataManager<?> getSubManager(DataType dataType) {
    return subManagers.get(dataType);
  }

  @SuppressWarnings("unchecked")
  public <T extends SubDataObject> SubDataManager<T> getSubManager(DataType dataType, Class<T> dataTypeClass) {
    return (SubDataManager<T>) subManagers.get(dataType);
  }

  public void onDestroy() {
    for (var subManager : getSubManagers()) {
      subManager.onDestroy();
    }
  }

  public String getStartMapName() {
    return startMapNameProperty.getValue();
  }

  public void setStartMapName(String value) {
    setDirty(true);
    startMapNameProperty.setValue(value);
  }

  public Property<String> getStartMapNameProperty() {
    return startMapNameProperty;
  }

  public MapData getStartMap() {
    return (MapData) getSubManager(DataType.MAP).get(getStartMapName());
  }

  public int getPixelPerMeter() {
    return pixelPerMeterProperty.getValue();
  }

  public void setPixelPerMeter(int value) {
    pixelPerMeterProperty.setValue(value);
  }

  public Property<Integer> getPixelPerMeterProperty() {
    return pixelPerMeterProperty;
  }

  public boolean isMaximized() {
    return maximizedProperty.getValue();
  }

  public void setMaximized(boolean value) {
    maximizedProperty.setValue(value);
  }

  public Property<Boolean> getMaximizedProperty() {
    return maximizedProperty;
  }

  public boolean isFullscreen() {
    return fullscreenProperty.getValue();
  }

  public void setFullscreen(boolean value) {
    fullscreenProperty.setValue(value);
  }

  public Property<Boolean> getFullscreenProperty() {
    return fullscreenProperty;
  }

  public String getTheme() {
    return themeProperty.getValue();
  }

  public void setTheme(String value) {
    themeProperty.setValue(value);
  }

  public Property<String> getThemeProperty() {
    return themeProperty;
  }

  @Override
  public JsonObject toJson() {
    return new JsonObject().add(getNameProperty().getName(), getName())
        .add(startMapNameProperty.getName(), getStartMapName()).add(pixelPerMeterProperty.getName(), getPixelPerMeter())
        .add(maximizedProperty.getName(), isMaximized()).add(fullscreenProperty.getName(), isFullscreen())
        .add(themeProperty.getName(), getTheme());
  }

  @Override
  public DataType getDataType() {
    return DataType.PROJECT;
  }

  public static ProjectData getCurrent() {
    return ProjectDataManager.getInstance().getCurrentProject();
  }

}
