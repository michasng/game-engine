package de.hsa.dice.core.dataobject.nested;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.DataObject;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.util.io.JsonObjectWrapper;

public class ScriptCompData extends NestedDataObject {

  private Property<String> nameProperty;
  private Property<Boolean> previewProperty;

  public ScriptCompData(DataObject parent) {
    super(parent);

    nameProperty = new Property<>("name", "");
    previewProperty = new Property<>("preview", false);

    register(nameProperty, previewProperty);
  }

  public ScriptCompData(DataObject parent, JsonObject json) {
    this(parent);
    JsonObjectWrapper wrapper = new JsonObjectWrapper(json);
    nameProperty.setValue(wrapper.getString(nameProperty), false);
    previewProperty.setValue(wrapper.getBoolean(previewProperty), false);
  }

  public String getName() {
    return nameProperty.getValue();
  }

  public void setName(String value) {
    nameProperty.setValue(value);
  }

  public Property<String> getNameProperty() {
    return nameProperty;
  }

  public boolean getPreview() {
    return previewProperty.getValue();
  }

  public void setPreview(boolean value) {
    previewProperty.setValue(value);
  }

  public Property<Boolean> getPreviewProperty() {
    return previewProperty;
  }

  @Override
  public JsonObject toJson() {
    return new JsonObject().add(nameProperty.getName(), getName()).add(previewProperty.getName(), getPreview());
  }

}
