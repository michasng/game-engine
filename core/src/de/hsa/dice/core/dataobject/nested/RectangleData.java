package de.hsa.dice.core.dataobject.nested;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.util.io.JsonSerializable;

/**
 * Describes a Rectangle. Is an immutable NestedDataObject, which is why it
 * doesn't need to register any properties and only implements JsonSerializable.
 */
public class RectangleData implements JsonSerializable {

  public final float x, y, width, height;

  public RectangleData(float x, float y, float width, float height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  public RectangleData(JsonObject json) {
    this(json.get("x").asFloat(), json.get("y").asFloat(), json.get("width").asFloat(), json.get("height").asFloat());
  }

  @Override
  public JsonObject toJson() {
    return new JsonObject().add("x", x).add("y", y).add("width", width).add("height", height);
  }

}
