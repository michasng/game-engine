package de.hsa.dice.core.dataobject.nested;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.EntityData;
import de.hsa.dice.core.dataobject.MapData;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.property.PropertySubscription;

public class MapEntityData extends NestedDataObject {

  private Property<String> nameProperty;
  private Property<VectorData> posProperty;

  private PropertySubscription<String> nameSub;

  public MapEntityData(MapData parent) {
    super(parent);

    nameProperty = new Property<>("name", "");
    posProperty = new Property<>("pos", new VectorData(0, 0));

    // successions:
    register(nameProperty, posProperty);

    nameProperty.addListener((oldValue, value) -> createNameSub());
  }

  public MapEntityData(MapData parent, JsonObject json) {
    this(parent);
    nameProperty.setValue(json.get("name").asString(), false);
    createNameSub();
    posProperty.setValue(new VectorData(json.get("pos").asObject()), false);
  }

  public void createNameSub() {
    var entityMgr = ProjectData.getCurrent().getSubManager(DataType.ENTITY, EntityData.class);
    entityMgr.runOnDoneLoading(() -> {
      var entity = entityMgr.get(getName());
      if (entity != null) {
        if (nameSub != null)
          nameSub.cancelSubscription();
        nameSub = entity.getNameProperty().addListener((oldValue, value) -> {
          nameProperty.setValue(value, false);
        });
      }
    });
  }

  public String getName() {
    return nameProperty.getValue();
  }

  public void setName(String value) {
    nameProperty.setValue(value);
  }

  public Property<String> getNameProperty() {
    return nameProperty;
  }

  public VectorData getPos() {
    return posProperty.getValue();
  }

  public void setPos(VectorData value) {
    posProperty.setValue(value);
  }

  public Property<VectorData> getPosProperty() {
    return posProperty;
  }

  public void onDestory() {
    if (nameSub != null)
      nameSub.cancelSubscription();
  }

  @Override
  public JsonObject toJson() {
    return new JsonObject().add(nameProperty.getName(), getName()).add(posProperty.getName(), getPos().toJson());
  }

}
