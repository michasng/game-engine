package de.hsa.dice.core.dataobject.nested;

import de.hsa.dice.core.property.Property;

public interface DataObjectParent {

  void register(Property<?>... newProps);

}
