package de.hsa.dice.core.dataobject.nested;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.DataObject;
import de.hsa.dice.core.property.InvalidPropertyException;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.util.io.JsonObjectWrapper;

public class SpriteFrameData extends NestedDataObject {

  private Property<String> pathProperty;
  private Property<Integer> durationProperty;

  private Property<RectangleData> subRegionProperty;

  public SpriteFrameData(DataObject parent) {
    super(parent);

    pathProperty = new Property<>("path", "");
    durationProperty = new Property<>("duration", 10);
    subRegionProperty = new Property<>("subRegion", null);

    // constraints:
    durationProperty.addListener((oldValue, value) -> {
      if (value < 1)
        throw new InvalidPropertyException("Duration must be at least 1.");
    });

    // successions:
    register(pathProperty, durationProperty);
  }

  public SpriteFrameData(DataObject parent, JsonObject json) {
    this(parent);
    JsonObjectWrapper wrapper = new JsonObjectWrapper(json);
    pathProperty.setValue(wrapper.getString(pathProperty), false);
    durationProperty.setValue(wrapper.getInt(durationProperty), false);
    var subRegionJson = wrapper.getObject(subRegionProperty.getName(), null);
    if (subRegionJson != null)
      subRegionProperty.setValue(new RectangleData(subRegionJson), false);
  }

  public String getPath() {
    return pathProperty.getValue();
  }

  public void setPath(String value) {
    pathProperty.setValue(value);
  }

  public Property<String> getPathProperty() {
    return pathProperty;
  }

  public Integer getDuration() {
    return durationProperty.getValue();
  }

  public void setDuration(Integer value) {
    durationProperty.setValue(value);
  }

  public Property<Integer> getDurationProperty() {
    return durationProperty;
  }

  public RectangleData getSubRegion() {
    return subRegionProperty.getValue();
  }

  public void setSubRegion(RectangleData value) {
    subRegionProperty.setValue(value);
  }

  public Property<RectangleData> getSubRegionProperty() {
    return subRegionProperty;
  }

  @Override
  public JsonObject toJson() {
    var jsonObject = new JsonObject().add(pathProperty.getName(), getPath()).add(durationProperty.getName(),
        getDuration());
    if (getSubRegion() != null)
      jsonObject.add(subRegionProperty.getName(), getSubRegion().toJson());
    return jsonObject;
  }

}
