package de.hsa.dice.core.dataobject.nested;

import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.util.io.JsonSerializable;

public abstract class NestedDataObject implements JsonSerializable, DataObjectParent {

  private DataObjectParent parent;

  public NestedDataObject(DataObjectParent parent) {
    this.parent = parent;
  }

  public void register(Property<?>... newProps) {
    parent.register(newProps);
  }

  public DataObjectParent getParent() {
    return parent;
  }

  @Override
  public String toString() {
    return toJson().toString();
  }

}
