package de.hsa.dice.core.dataobject.nested;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.dataobject.ScriptData;
import de.hsa.dice.core.property.PropNotifyList;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.script.NodeFactory;
import de.hsa.dice.core.script.NodeRegistry;
import de.hsa.dice.core.util.io.JsonObjectWrapper;

public class NodeData extends NestedDataObject {

  // interpretation properties:
  private Property<Integer> idProperty;
  private Property<String> typeProperty;
  // array of int touples for target-node and port
  private Property<PropNotifyList<NodeOutputData>> outputsProperty;
  private Property<FieldData[]> fieldsProperty;

  // rendering properties:
  private Property<VectorData> posProperty;

  private NodeFactory factory; // depends on type

  public NodeData(ScriptData parent) {
    super(parent);

    idProperty = new Property<>("id", -1);
    typeProperty = new Property<>("type", "");
    outputsProperty = new Property<>("output", null);
    var outputsList = new PropNotifyList<NodeOutputData>(outputsProperty);
    outputsProperty.setValue(outputsList, false);
    fieldsProperty = new Property<>("fields", new FieldData[0]);
    posProperty = new Property<>("pos", new VectorData(0, 0));

    typeProperty.addListener((oldValue, value) -> {
      factory = NodeRegistry.getInstance().getFactory(value);
    });

    register(idProperty, typeProperty, outputsProperty, fieldsProperty, posProperty);
  }

  public NodeData(ScriptData parent, JsonObject json) {
    this(parent);

    JsonObjectWrapper wrapper = new JsonObjectWrapper(json);
    idProperty.setValue(json.get(idProperty.getName()).asInt(), false);
    typeProperty.setValue(json.get(typeProperty.getName()).asString(), false);
    JsonArray jsonOutputs = wrapper.getArray(outputsProperty.getName(), new JsonArray());
    outputsProperty.getValue().setNotifyEnabled(false);
    for (var jsonOutput : jsonOutputs)
      outputsProperty.getValue().add(new NodeOutputData(this, jsonOutput.asObject()));
    outputsProperty.getValue().setNotifyEnabled(true);

    factory = NodeRegistry.getInstance().getFactory(getType());
    if (factory == null) {
      Console.logError("Invalid node type: " + getType());
    } else {
      JsonArray jsonFields = wrapper.getArray(fieldsProperty.getName(), new JsonArray());
      fieldsProperty.setValue(new FieldData[jsonFields.size()], false);
      for (int i = 0; i < jsonFields.size(); i++) {
        var jsonField = jsonFields.get(i).asObject();
        var fieldName = jsonField.get(FieldData.FIELD_NAME).asString();
        var inspectedField = factory.getInspectedField(fieldName);
        if (inspectedField == null)
          Console.logError("Node field not found: " + fieldName + " in " + getType());
        else
          fieldsProperty.getValue()[i] = new FieldData(parent, jsonFields.get(i).asObject(), inspectedField);
      }
    }

    posProperty.setValue(new VectorData(json.get(posProperty.getName()).asObject()), false);

  }

  public int getId() {
    return idProperty.getValue();
  }

  public void setId(int value) {
    idProperty.setValue(value);
  }

  public Property<Integer> getIdProperty() {
    return idProperty;
  }

  public String getType() {
    return typeProperty.getValue();
  }

  public void setType(String value) {
    typeProperty.setValue(value);
  }

  public Property<String> getTypeProperty() {
    return typeProperty;
  }

  public PropNotifyList<NodeOutputData> getOutputs() {
    return outputsProperty.getValue();
  }

  public void setOutputs(PropNotifyList<NodeOutputData> value) {
    outputsProperty.setValue(value);
  }

  public Property<PropNotifyList<NodeOutputData>> getOutputsProperty() {
    return outputsProperty;
  }

  public FieldData[] getFields() {
    return fieldsProperty.getValue();
  }

  public void setFields(FieldData[] value) {
    fieldsProperty.setValue(value);
  }

  public Property<FieldData[]> getFieldsProperty() {
    return fieldsProperty;
  }

  public VectorData getPos() {
    return posProperty.getValue();
  }

  public void setPos(VectorData value) {
    posProperty.setValue(value);
  }

  public Property<VectorData> getPosProperty() {
    return posProperty;
  }

  public NodeFactory getFactory() {
    return factory;
  }

  @Override
  public JsonObject toJson() {
    JsonArray jsonOutputs = new JsonArray();
    for (var output : getOutputs())
      jsonOutputs.add(output.toJson());
    JsonArray jsonFields = new JsonArray();
    for (FieldData field : getFields())
      jsonFields.add(field.toJson());
    return new JsonObject().add(idProperty.getName(), getId()).add(typeProperty.getName(), getType())
        .add(outputsProperty.getName(), jsonOutputs).add(fieldsProperty.getName(), jsonFields)
        .add(posProperty.getName(), getPos().toJson());
  }

}
