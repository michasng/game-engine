package de.hsa.dice.core.dataobject.nested;

import org.jbox2d.common.Vec2;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.util.io.JsonSerializable;

/**
 * Describes a Vector. Is an immutable NestedDataObject, which is why it doesn't
 * need to register any properties and only implements JsonSerializable.
 */
public class VectorData implements JsonSerializable {

  public final float x;
  public final float y;

  public VectorData(float x, float y) {
    this.x = x;
    this.y = y;
  }

  public VectorData(Vec2 vec) {
    this(vec.x, vec.y);
  }

  public VectorData(JsonObject json) {
    this.x = json.get("x").asFloat();
    this.y = json.get("y").asFloat();
  }

  @Override
  public JsonObject toJson() {
    return new JsonObject().add("x", x).add("y", y);
  }

  public Vec2 toVec2() {
    return new Vec2(x, y);
  }

}
