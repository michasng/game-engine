package de.hsa.dice.core.dataobject.nested;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.MapData;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.util.io.JsonObjectWrapper;

public class MapTileData extends NestedDataObject {

  private Property<String> nameProperty;
  private Property<Integer> idProperty;

  public MapTileData(MapData parent) {
    super(parent);
    nameProperty = new Property<>("name", "");
    idProperty = new Property<>("id", 0);

    register(nameProperty, idProperty);
  }

  public MapTileData(MapData parent, JsonObject json) {
    this(parent);
    JsonObjectWrapper wrapper = new JsonObjectWrapper(json);
    nameProperty.setValue(wrapper.getString(nameProperty), false);
    idProperty.setValue(wrapper.getInt(idProperty), false);
  }

  public String getName() {
    return nameProperty.getValue();
  }

  public void setName(String value) {
    nameProperty.setValue(value);
  }

  public int getId() {
    return idProperty.getValue();
  }

  public Property<String> getNameProperty() {
    return nameProperty;
  }

  public void setId(int value) {
    idProperty.setValue(value);
  }

  public Property<Integer> getIdProperty() {
    return idProperty;
  }

  @Override
  public JsonObject toJson() {
    return new JsonObject().add(nameProperty.getName(), getName()).add(idProperty.getName(), getId());
  }

}
