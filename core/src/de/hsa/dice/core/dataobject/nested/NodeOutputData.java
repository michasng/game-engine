package de.hsa.dice.core.dataobject.nested;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.property.Property;

/**
 * Describes a Vector. Is an immutable NestedDataObject, which is why it doesn't
 * need to register any properties and only implements JsonSerializable.
 */
public class NodeOutputData extends NestedDataObject {

  private Property<Integer> targetNodeIdProperty;
  private Property<Integer> targetPortProperty;

  public NodeOutputData(NodeData parent, int targetNodeId, int targetPort) {
    super(parent);

    targetNodeIdProperty = new Property<>("targetNodeId", targetNodeId);
    targetPortProperty = new Property<>("targetPort", targetPort);

    register(targetNodeIdProperty, targetPortProperty);
  }

  public NodeOutputData(NodeData parent, JsonObject json) {
    this(parent, json.get("targetNodeId").asInt(), json.get("targetPort").asInt());
  }

  public int getTargetNodeId() {
    return targetNodeIdProperty.getValue();
  }

  public void setTargetNodeId(int value) {
    targetNodeIdProperty.setValue(value);
  }

  public Property<Integer> getTargetNodeIdProperty() {
    return targetNodeIdProperty;
  }

  public int getTargetPort() {
    return targetPortProperty.getValue();
  }

  public void setTargetPort(int value) {
    targetPortProperty.setValue(value);
  }

  public Property<Integer> getTargetPortProperty() {
    return targetPortProperty;
  }

  @Override
  public JsonObject toJson() {
    return new JsonObject().add(targetNodeIdProperty.getName(), getTargetNodeId()).add(targetPortProperty.getName(),
        getTargetPort());
  }

}
