package de.hsa.dice.core.dataobject.nested;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.logging.Logger;

import org.jbox2d.common.Vec2;

import com.eclipsesource.json.JsonObject;

import de.hsa.dice.core.dataobject.DataObject;
import de.hsa.dice.core.property.Property;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.util.ClassUtil;
import de.hsa.dice.core.util.io.JsonObjectWrapper;
import de.hsa.dice.core.util.io.UnsupportedSaveException;

public class FieldData extends NestedDataObject {

  private static final Logger logger = Logger.getLogger(FieldData.class.getName());

  private static final String TYPE_NAME = "typeName";

  // needed in ScriptData, TODO: refactor this reference away
  static final String FIELD_NAME = "fieldName";

  private static final String VALUE = "value";

  private String typeName;
  private Class<?> typeClass;
  private String fieldName;
  private Property<?> valueProperty;

  private Inspect inspectAnnot;

  private Property<Boolean> isVariableProperty;
  private Property<String> variableProperty;

  public FieldData(DataObject parent, String typeName, String fieldName, Field inspectedField) {
    super(parent);
    this.typeName = typeName;
    this.fieldName = fieldName;
    try {
      typeClass = ClassUtil.forName(typeName);
    } catch (ClassNotFoundException e) {
      logger.warning("Class not found " + typeName);
      // e.printStackTrace();
    }
    this.inspectAnnot = inspectedField.getDeclaredAnnotation(Inspect.class);

    if (typeClass == null)
      return;

    if (typeClass == boolean.class) {
      valueProperty = new Property<Boolean>(VALUE, false);
    } else if (typeClass == int.class) {
      valueProperty = new Property<Integer>(VALUE, 0);
    } else if (typeClass == float.class) {
      valueProperty = new Property<Float>(VALUE, 0f);
    } else if (typeClass == String.class) {
      valueProperty = new Property<String>(VALUE, "");
    } else if (typeClass == Vec2.class) {
      valueProperty = new Property<VectorData>(VALUE, new VectorData(0, 0));
    } else {
      if (!typeClass.isEnum() || typeClass.getEnumConstants().length == 0)
        throw new Error("Unsupported field type: " + typeName);
      // use first value in the enum by default
      valueProperty = new Property<Object>(VALUE, typeClass.getEnumConstants()[0]);
    }

    isVariableProperty = new Property<>("isVariable", false);
    variableProperty = new Property<>("variable", "");

    register(valueProperty);
  }

  @SuppressWarnings("unchecked")
  public FieldData(DataObject parent, JsonObject json, Field inspectedField) {
    this(parent, json.get(TYPE_NAME).asString(), json.get(FIELD_NAME).asString(), inspectedField);

    if (typeClass == null)
      return;

    if (typeClass == boolean.class) {
      ((Property<Boolean>) valueProperty).setValue(json.get(VALUE).asBoolean(), false);
    } else if (typeClass == int.class) {
      ((Property<Integer>) valueProperty).setValue(json.get(VALUE).asInt(), false);
    } else if (typeClass == float.class) {
      ((Property<Float>) valueProperty).setValue(json.get(VALUE).asFloat(), false);
    } else if (typeClass == String.class) {
      ((Property<String>) valueProperty).setValue(json.get(VALUE).asString(), false);
    } else if (typeClass == Vec2.class) {
      ((Property<VectorData>) valueProperty).setValue(new VectorData(json.get(VALUE).asObject()), false);
    } else {
      String valueName = json.get(VALUE).asString();
      Enum<?> enumInstance = createEnumInstance(typeClass, valueName);
      ((Property<Object>) valueProperty).setValue(enumInstance, false);
    }

    var wrapper = new JsonObjectWrapper(json);
    isVariableProperty.setValue(wrapper.getBoolean(isVariableProperty), false);
    variableProperty.setValue(wrapper.getString(variableProperty), false);
  }

  @SuppressWarnings("unchecked")
  private <T extends Enum<T>> T createEnumInstance(Type type, String name) {
    if (!typeClass.isEnum())
      throw new Error("Unsupported field type: " + typeName);
    return Enum.valueOf((Class<T>) type, name);
  }

  public String getFieldName() {
    return fieldName;
  }

  public String getTypeName() {
    return typeName;
  }

  public Class<?> getTypeClass() {
    return typeClass;
  }

  public Object getValue() {
    if (typeClass == Vec2.class)
      return ((VectorData) valueProperty.getValue()).toVec2();
    return valueProperty.getValue();
  }

  private Object getValueImpl() { // converted value in serializable form
    return valueProperty.getValue();
  }

  @SuppressWarnings("unchecked")
  public void setValue(Object value) {
    ((Property<Object>) valueProperty).setValue(value);
  }

  public Property<?> getValueProperty() {
    return valueProperty;
  }

  public boolean isVariable() {
    return isVariableProperty.getValue();
  }

  public void setIsVariable(boolean value) {
    isVariableProperty.setValue(value);
  }

  public Property<Boolean> getIsVariableProperty() {
    return isVariableProperty;
  }

  public String getVariable() {
    return variableProperty.getValue();
  }

  public void setVariable(String value) {
    variableProperty.setValue(value);
  }

  public Property<String> getVariableProperty() {
    return variableProperty;
  }

  public Inspect getInspectAnnot() {
    return inspectAnnot;
  }

  @Override
  public JsonObject toJson() {
    if (typeClass == null)
      throw new UnsupportedSaveException("Invalid field type " + typeName);

    var jsonObject = new JsonObject().add(FIELD_NAME, fieldName).add(TYPE_NAME, typeName)
        .add(isVariableProperty.getName(), isVariable()).add(variableProperty.getName(), getVariable());
    if (typeClass == boolean.class) {
      return jsonObject.add(VALUE, (boolean) getValue());
    } else if (typeClass == int.class) {
      return jsonObject.add(VALUE, (int) getValue());
    } else if (typeClass == float.class) {
      return jsonObject.add(VALUE, (float) getValue());
    } else if (typeClass == String.class) {
      return jsonObject.add(VALUE, (String) getValue());
    } else if (typeClass == Vec2.class) {
      return jsonObject.add(VALUE, ((VectorData) getValueImpl()).toJson());
    } else {
      if (!typeClass.isEnum())
        throw new Error("Unsupported field type: " + typeName);
      var fields = typeClass.getDeclaredFields();
      try {
        for (Field field : fields) {
          field.setAccessible(true);
          if (field.isEnumConstant() && field.get(this) == getValue())
            return jsonObject.add(VALUE, field.getName());
        }
      } catch (IllegalArgumentException | IllegalAccessException e) {
        e.printStackTrace();
      }
      throw new Error(typeClass.getName() + " does not contain the field " + getValue());
    }
  }

}
