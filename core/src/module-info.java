module dice.core {
  exports de.hsa.dice.core;
  exports de.hsa.dice.core.dataobject;
  exports de.hsa.dice.core.dataobject.manager;
  exports de.hsa.dice.core.dataobject.nested;
  exports de.hsa.dice.core.game;
  exports de.hsa.dice.core.game.entity;
  exports de.hsa.dice.core.game.map;
  exports de.hsa.dice.core.property;
  exports de.hsa.dice.core.script;
  exports de.hsa.dice.core.util;
  exports de.hsa.dice.core.util.io;
  exports de.hsa.dice.core.view;

  exports de.hsa.dice.nodes.controlflow;
  exports de.hsa.dice.nodes.entity;
  exports de.hsa.dice.nodes.event;
  exports de.hsa.dice.nodes.filter;
  exports de.hsa.dice.nodes.input;
  exports de.hsa.dice.nodes.list;
  exports de.hsa.dice.nodes.map;
  exports de.hsa.dice.nodes.maths;
  exports de.hsa.dice.nodes.random;
  exports de.hsa.dice.nodes.sound;

  // open for runtime access
  opens de.hsa.dice.nodes.controlflow;
  opens de.hsa.dice.nodes.entity;
  opens de.hsa.dice.nodes.event;
  opens de.hsa.dice.nodes.filter;
  opens de.hsa.dice.nodes.input;
  opens de.hsa.dice.nodes.list;
  opens de.hsa.dice.nodes.map;
  opens de.hsa.dice.nodes.maths;
  opens de.hsa.dice.nodes.random;
  opens de.hsa.dice.nodes.sound;
  opens view.icons;

  requires java.logging;

  requires javafx.fxml;
  requires transitive javafx.graphics;

  requires transitive java.desktop;

  requires transitive JBox2D;
  requires transitive minimal.json;
}