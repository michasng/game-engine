package de.hsa.dice.nodes.entity;

import java.util.LinkedList;

import org.jbox2d.collision.WorldManifold;
import org.jbox2d.common.Vec2;

import de.hsa.dice.core.game.entity.Entity.EntityContact;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Collision", category = "Entity", description = "Calculates the average direction of all contacts and writes it to a variable.")
public class GetContactDirectionNode extends ScriptNode {

  @Inspect(colored = true, description = "Use these contacts for calculation. Use all contacts, if this is empty.")
  private String contactsVariable = "";

  @Inspect(colored = true, description = "The variable to write the direction to. Must not be empty.")
  private String directionVariable = "contactDir";

  @SuppressWarnings("unchecked")
  @Override
  public boolean run() {
    var contacts = getTarget().getContacts();
    if (!contactsVariable.isEmpty()) {
      var obj = getTarget().getVariable(contactsVariable);
      if (obj instanceof LinkedList) {
        contacts = (LinkedList<EntityContact>) obj;
      } else {
        logError("Invalid variable contactsVariable:" + contactsVariable + " (not a list).");
        return false;
      }
    }

    try {
      var dir = getAverageDirection(contacts);
      getTarget().setVariable(directionVariable, dir);
    } catch (ClassCastException e) {
      logError("Invalid variable contactsVariable:" + contactsVariable + " (not a list of type EntityContact).");
      return false;
    }

    return true;
  }

  public Vec2 getAverageDirection(LinkedList<EntityContact> contacts) {
    var result = new Vec2(0, 0);
    var ownBody = getTarget().getBody();
    WorldManifold worldManifold = new WorldManifold();
    for (var contact : contacts) {
      var bodyA = contact.contact.getFixtureA().getBody();
      contact.contact.getWorldManifold(worldManifold);
      if (ownBody == bodyA) // normal from A to B
        result = result.addLocal(worldManifold.normal.mul(1f / contacts.size()));
      else
        result = result.addLocal(worldManifold.normal.mul(-1f / contacts.size()));
    }
    result.normalize();
    return result;
  }

}
