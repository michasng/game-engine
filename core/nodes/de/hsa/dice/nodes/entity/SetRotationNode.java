package de.hsa.dice.nodes.entity;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Spinner", category = "Entity", description = "Sets the rotation of the target.")
public class SetRotationNode extends ScriptNode {

  @Inspect(description = "The rotation to set on the center of the target's hitbox in degrees.")
  private float rotation;

  @Override
  public boolean run() {
    getTarget().setRotation(rotation);
    return true;
  }

}
