package de.hsa.dice.nodes.entity;

import java.util.LinkedList;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.game.entity.Entity;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Magnet", category = "Entity", description = "Calculates the average direction of all other entities to the target and writes it to a variable.")
public class GetEntitiesDirectionNode extends ScriptNode {

  @Inspect(colored = true, description = "Use these entities for calculation. Use all entities, if this is empty.")
  private String entitiesVariable = "";

  @Inspect(colored = true, description = "The variable to write the direction to. Must not be empty.")
  private String directionVariable = "entitiesDir";

  @SuppressWarnings("unchecked")
  @Override
  public boolean run() {
    var entities = Game.getInstance().getMap().getEntities();
    if (!entitiesVariable.isEmpty()) {
      var obj = getTarget().getVariable(entitiesVariable);
      if (obj instanceof LinkedList) {
        entities = (LinkedList<Entity>) obj;
      } else {
        logError("Invalid variable " + entitiesVariable + ":" + obj + " (not a list).");
        return false;
      }
    }

    try {
      var dir = getAverageDirection(entities);
      getTarget().setVariable(directionVariable, dir);
    } catch (ClassCastException e) {
      logError("Invalid variable " + entitiesVariable + ":" + entities + " (not a list of type Entity).");
      return false;
    }

    return true;
  }

  public Vec2 getAverageDirection(LinkedList<Entity> entities) {
    var result = new Vec2(0, 0);
    for (var entity : entities) {
      if (entity == getTarget())
        continue;
      result.addLocal(entity.getPosition().sub(getTarget().getPosition()).mul(1f / entities.size()));
    }
    result.normalize();
    return result;
  }

}
