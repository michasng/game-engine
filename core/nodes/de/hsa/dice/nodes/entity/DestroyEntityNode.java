package de.hsa.dice.nodes.entity;

import java.util.LinkedList;

import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.game.entity.Entity;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Destroy", category = "Entity", description = "Destroys all specified entities after the scripts ran.")
public class DestroyEntityNode extends ScriptNode {

  @Inspect(colored = true, description = "Destory these entities. If this is empty, destroy all entities.")
  private String entitiesVariable = "";

  @SuppressWarnings("unchecked")
  @Override
  protected boolean run() {
    var entities = Game.getInstance().getMap().getEntities();
    if (!entitiesVariable.isEmpty()) {
      var obj = getTarget().getVariable(entitiesVariable);
      if (obj instanceof LinkedList) {
        entities = (LinkedList<Entity>) obj;
      } else {
        logError("Invalid variable " + entitiesVariable + ":" + obj + " (not a list).");
        return false;
      }
    }

    // entities needs to be used in an enclosing scope
    final var finalEntities = entities;

    var game = Game.getInstance();
    try {
      game.addToPostUpdate(() -> {
        for (var entity : finalEntities)
          game.getMap().despawnEntity(entity);
      });
    } catch (ClassCastException e) {
      logError("Invalid variable " + entitiesVariable + ":" + entities + " (not a list of type Entity).");
      return false;
    }

    return true;
  }

}
