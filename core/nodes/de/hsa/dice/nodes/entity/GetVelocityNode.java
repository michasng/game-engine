package de.hsa.dice.nodes.entity;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "FastWheel", category = "Entity", description = "Gets the target's velocity vector and stores it in a variable.")
public class GetVelocityNode extends ScriptNode {

  @Inspect(colored = true, description = "The variable to write the target's velocity to. Must not be empty.")
  private String velocityVariable = "velocity";

  @Override
  public boolean run() {
    if (velocityVariable.isEmpty()) {
      logError("velocityVariable must not be empty.");
      return false;
    }

    var velocity = getTarget().getLinearVel().clone();
    getTarget().setVariable(velocityVariable, velocity);
    return true;
  }

}
