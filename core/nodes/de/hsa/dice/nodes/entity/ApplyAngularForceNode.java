package de.hsa.dice.nodes.entity;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Magnet", category = "Entity", description = "Applies an angular force to the target.")
public class ApplyAngularForceNode extends ScriptNode {

  @Inspect(description = "The change per tick in angular velocity around the center of the target.")
  private float deltaAngularVel;

  @Override
  public boolean run() {
    getTarget().applyAngularForce(deltaAngularVel);
    return true;
  }

}
