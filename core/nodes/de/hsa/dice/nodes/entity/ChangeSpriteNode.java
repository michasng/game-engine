package de.hsa.dice.nodes.entity;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.SpriteData;
import de.hsa.dice.core.game.Sprite;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Graphics", category = "Entity", description = "Changes the sprite on the entitiy to the specified one. Won't fire if the sprite doesn't exist.")
public class ChangeSpriteNode extends ScriptNode {

  @Inspect(description = "The name of the sprite to change to.")
  private String newSprite;

  @Override
  protected boolean run() {
    var spriteMgr = ProjectData.getCurrent().getSubManager(DataType.SPRITE, SpriteData.class);
    var spriteData = spriteMgr.get(newSprite);
    if (spriteData == null)
      return false;

    getTarget().setSprite(new Sprite(spriteData));
    return true;

  }

}
