package de.hsa.dice.nodes.entity;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Magnet", category = "Entity", description = "Applies a force to the target.")
public class ApplyForceNode extends ScriptNode {

  @Inspect(description = "The force to apply on the center of the target.")
  private Vec2 force;

  @Inspect(description = "Whether the x value of the force should be considered 0.")
  private boolean ignoreX = false;
  @Inspect(description = "Whether the y value of the force should be considered 0.")
  private boolean ignoreY = false;

  @Override
  public boolean run() {
    if (ignoreX)
      force.x = 0;
    if (ignoreY)
      force.y = 0;
    getTarget().applyForce(force);
    return true;
  }

}
