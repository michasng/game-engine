package de.hsa.dice.nodes.entity;

import de.hsa.dice.core.dataobject.EntityData.GraphicsMode;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Graphics", category = "Entity", description = "Gets the name of the target's current sprite and stores it in a variable. Won't fire if there is none or if the entity's graphicsmode does not show sprites")
public class GetSpriteNode extends ScriptNode {

  @Inspect(colored = true, description = "The variable to write the target's sprite name to. Must not be empty.")
  private String spriteVariable = "sprite";

  @Override
  protected boolean run() {
    if (spriteVariable.isEmpty()) {
      logError("rotationVariable must not be empty.");
      return false;
    }

    if (getTarget().getGraphicsMode() != GraphicsMode.SPRITE)
      return false;

    var sprite = getTarget().getSprite();
    if (sprite == null)
      return false;

    var spriteName = sprite.getDataObject().getName();
    getTarget().setVariable(spriteVariable, spriteName);
    return true;

  }

}
