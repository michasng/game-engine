package de.hsa.dice.nodes.entity;

import java.util.LinkedList;

import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.game.entity.Entity;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "MagnifyingGlass", category = "Entity", description = "Finds all entities with a given name or script-component and writes them to a variable. Will only fire, if there is more than 1 entity.")
public class FindEntitiesNode extends ScriptNode {

  @Inspect(description = "The name of the entity to filter for.")
  private String nameFilter = "";

  @Inspect(description = "The script-component name to filter for.")
  private String scriptFilter = "";

  @Inspect(colored = true, description = "The variable to write the found entities to. Must not be empty.")
  private String entitiesVariable = "entities";

  @Override
  protected boolean run() {
    if (entitiesVariable.isEmpty()) {
      logError("entitiesVariable must not be empty.");
      return false;
    }

    var entities = Game.getInstance().getMap().getEntities();
    var filteredEntities = new LinkedList<Entity>();
    for (var entity : entities) {
      if (!nameFilter.isEmpty() && !entity.hasName(nameFilter))
        continue;
      if (!scriptFilter.isEmpty() && !entity.hasScript(scriptFilter))
        continue;

      filteredEntities.add(entity);
    }

    getTarget().setVariable(entitiesVariable, filteredEntities);
    return true;
  }

}
