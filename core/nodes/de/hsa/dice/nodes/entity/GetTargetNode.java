package de.hsa.dice.nodes.entity;

import java.util.LinkedList;

import de.hsa.dice.core.game.entity.Entity;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Location", category = "Entity", description = "Stores the target entity in a list.")
public class GetTargetNode extends ScriptNode {

  @Inspect(colored = true, description = "The variable to write the target entity to. Must not be empty.")
  private String entitiesVariable = "self";

  @Override
  protected boolean run() {
    if (entitiesVariable.isEmpty()) {
      logError("entitiesVariable must not be empty.");
      return false;
    }
    var entities = new LinkedList<Entity>();
    entities.add(getTarget());
    getTarget().setVariable(entitiesVariable, entities);
    return true;
  }

}
