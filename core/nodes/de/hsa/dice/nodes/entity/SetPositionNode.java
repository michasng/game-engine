package de.hsa.dice.nodes.entity;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Grid", category = "Entity", description = "Sets the position of the target.")
public class SetPositionNode extends ScriptNode {

  @Inspect(description = "The position to set the target's hitbox center point to.")
  private Vec2 position;

  @Inspect(description = "Whether the x value of the position should not be changed.")
  private boolean ignoreX = false;
  @Inspect(description = "Whether the y value of the position should not be changed.")
  private boolean ignoreY = false;

  @Override
  public boolean run() {
    if (ignoreX)
      position.x = getTarget().getPosition().x;
    if (ignoreY)
      position.y = getTarget().getPosition().y;
    getTarget().setPosition(position);
    return true;
  }

}
