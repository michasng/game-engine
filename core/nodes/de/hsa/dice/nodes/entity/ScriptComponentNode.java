package de.hsa.dice.nodes.entity;

import java.util.LinkedList;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.dataobject.ScriptData;
import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.game.entity.Entity;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "DotGraph", category = "Entity", description = "Changes the script-components on the entities.")
public class ScriptComponentNode extends ScriptNode {

  @Inspect(description = "Whether the script should be added or removed.")
  private Mode mode;

  @Inspect(description = "The name of the script-component.")
  private String scriptName;

  @Inspect(colored = true, description = "The filtered entities varaible to use. Uses all entities if this is empty.")
  private String entitiesVariable;

  @SuppressWarnings("unchecked")
  @Override
  protected boolean run() {
    var entities = Game.getInstance().getMap().getEntities();
    if (!entitiesVariable.isEmpty()) {
      var obj = getTarget().getVariable(entitiesVariable);
      if (obj instanceof LinkedList) {
        entities = (LinkedList<Entity>) obj;
      } else {
        logError("Invalid variable " + entitiesVariable + ":" + obj + " (not a list).");
        return false;
      }
    }

    var scriptManager = ProjectData.getCurrent().getSubManager(DataType.SCRIPT, ScriptData.class);
    var scriptData = scriptManager.get(scriptName);
    if (scriptData == null) {
      logError("Invalid scriptName:" + scriptName);
      return false;
    }

    try {
      for (var entity : entities) {
        if (mode == Mode.ADD)
          entity.addScript(scriptData);
        else // if (mode == Mode.REMOVE)
          entity.removeScript(scriptName);
      }
    } catch (ClassCastException e) {
      logError("Invalid variable " + entitiesVariable + ":" + entities + " (not a list of type Entity).");
      return false;
    }
    return true;
  }

  private enum Mode {
    ADD, REMOVE
  }

}
