package de.hsa.dice.nodes.entity;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Graphics", category = "Entity", description = "Sets the frame index on the current entity sprite. Won't fire if the sprite doesn't exist.")
public class SetSpriteFrameNode extends ScriptNode {

  @Inspect(description = "The index of the new frame.")
  private int frameIndex;

  @Inspect(description = "Whether to add or subtract from the current index (true), or just set ignoring the current index (false).")
  private boolean relative;

  @Override
  protected boolean run() {
    var sprite = getTarget().getSprite();
    if (sprite == null)
      return false;

    var newIndex = frameIndex;
    if (relative)
      newIndex += sprite.getFrameIndex();
    sprite.setFrameIndex(newIndex);
    return true;
  }

}
