package de.hsa.dice.nodes.entity;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "DotGraph", category = "Entity", description = "Fires only if the target uses a given script-components.")
public class CheckForComponentNode extends ScriptNode {

  @Inspect(description = "The name of the script-component.")
  private String scriptName;

  @Override
  protected boolean run() {
    return getTarget().hasScript(scriptName);
  }

}
