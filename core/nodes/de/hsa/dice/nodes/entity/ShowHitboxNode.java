package de.hsa.dice.nodes.entity;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Box", category = "Entity", description = "Changes the visibility of the target's hitbox.")
public class ShowHitboxNode extends ScriptNode {

  @Inspect(description = "Whether the hitbox should be visible.")
  private boolean showHitbox = true;

  @Override
  protected boolean run() {
    getTarget().setShowHitbox(showHitbox);
    return true;
  }

}
