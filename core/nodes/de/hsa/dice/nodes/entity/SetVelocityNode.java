package de.hsa.dice.nodes.entity;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "FastWheel", category = "Entity", description = "Sets the linear velocity of the target.")
public class SetVelocityNode extends ScriptNode {

  @Inspect(description = "The velocity to set on the target.")
  private Vec2 velocity;

  @Inspect(description = "Whether the x value of the velocity should not be changed.")
  private boolean ignoreX = false;
  @Inspect(description = "Whether the y value of the velocity should not be changed.")
  private boolean ignoreY = false;

  @Override
  public boolean run() {
    if (ignoreX)
      velocity.x = getTarget().getLinearVel().x;
    if (ignoreY)
      velocity.y = getTarget().getLinearVel().y;
    getTarget().setLinearVel(velocity);
    return true;
  }

}
