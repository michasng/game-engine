package de.hsa.dice.nodes.entity;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.EntityData;
import de.hsa.dice.core.dataobject.nested.MapEntityData;
import de.hsa.dice.core.dataobject.nested.VectorData;
import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Brain", category = "Entity", description = "Spawns a given entity at a specific position after the scripts ran.")
public class SpawnEntityNode extends ScriptNode {

  @Inspect(description = "The name of the entity-type to spawn.")
  private String entityName;

  @Inspect(description = "The position to spawn the entity at.")
  private Vec2 position;

  @Override
  protected boolean run() {
    var entityManager = Game.getInstance().getProjectData().getSubManager(DataType.ENTITY, EntityData.class);
    var entityData = entityManager.get(entityName);
    if (entityData == null) {
      logError("Entity " + entityName + " does not exist.");
      return false;
    }

    var mapEntityData = new MapEntityData(Game.getInstance().getMap().getDataObject());
    mapEntityData.setName(entityData.getName());
    mapEntityData.setPos(new VectorData(position));
    var game = Game.getInstance();
    game.addToPostUpdate(() -> game.getMap().spawnEntity(mapEntityData));
    return true;
  }

}
