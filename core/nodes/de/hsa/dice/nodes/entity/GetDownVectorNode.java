package de.hsa.dice.nodes.entity;

import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Vec2;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Down", category = "Entity", description = "Gets a vector pointing from the (rotated) target downwards and stores it in a variable.")
public class GetDownVectorNode extends ScriptNode {

  @Inspect(colored = true, description = "The variable to write the vector to. Must not be empty.")
  private String directionVariable = "downwards";

  @Override
  public boolean run() {
    if (directionVariable.isEmpty()) {
      logError("directionVariable must not be empty.");
      return false;
    }

    // formula for rotation in 2d:
    // x = cos(rad) * x0 - sin(rad) * y0
    // y = sin(rad) * x0 + cos(rad) * y0

    var radians = getTarget().getRotation() * MathUtils.DEG2RAD;
    var normal = new Vec2(0, 1);
    var result = new Vec2(MathUtils.cos(radians) * normal.x - MathUtils.sin(radians) * normal.y,
        MathUtils.sin(radians) * normal.x + MathUtils.cos(radians) * normal.y);
    getTarget().setVariable(directionVariable, result);
    return true;
  }

}
