package de.hsa.dice.nodes.entity;

import org.jbox2d.collision.WorldManifold;
import org.jbox2d.common.Vec2;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Weight", category = "Entity", description = "Fires if the target entity is touching a ground tile or entity.")
public class IfOnGroundNode extends ScriptNode {

  @Inspect(description = "The maximum angle of the contact to the floor to count as touching the ground.")
  private float angleThreshold = 10;

  @Inspect(description = "The degree by which the vector pointing to the ground is rotated.")
  private float angle;

  @Override
  protected boolean run() {
    var groundDir = new Vec2(0, 1);
    var theta = Math.toRadians(angle);
    var cs = Math.cos(theta);
    var sn = Math.sin(theta);
    groundDir = new Vec2((float) (groundDir.x * cs - groundDir.y * sn), (float) (groundDir.x * sn + groundDir.y * cs));

    var ownBody = getTarget().getBody();
    var contacts = getTarget().getContacts();
    WorldManifold worldManifold = new WorldManifold();
    for (var contact : contacts) {
      contact.contact.getWorldManifold(worldManifold);
      var bodyB = contact.contact.getFixtureB().getBody();
      var dir = worldManifold.normal;
      if (ownBody == bodyB) // normal from A to B
        dir.mulLocal(-1);
//      System.out.println("dir: " + dir);
      var angleBetween = (float) Math.toDegrees(Math.acos(Vec2.dot(groundDir, dir)));
      if (angleBetween < angleThreshold)
        return true;
    }
    return false;
  }

}
