package de.hsa.dice.nodes.entity;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Spinner", category = "Entity", description = "Gets the target's floating point rotation in degrees and stores it in a variable.")
public class GetRotationNode extends ScriptNode {

  @Inspect(colored = true, description = "The variable to write the target's rotation to. Must not be empty.")
  private String rotationVariable = "rotation";

  @Override
  public boolean run() {
    if (rotationVariable.isEmpty()) {
      logError("rotationVariable must not be empty.");
      return false;
    }

    var rotation = getTarget().getRotation();
    getTarget().setVariable(rotationVariable, rotation);
    return true;
  }

}
