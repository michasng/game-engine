package de.hsa.dice.nodes.entity;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Grid", category = "Entity", description = "Gets the target's vector position and stores it in a variable.")
public class GetPositionNode extends ScriptNode {

  @Inspect(colored = true, description = "The variable to write the target's position to. Must not be empty.")
  private String positionVariable = "position";

  @Override
  public boolean run() {
    if (positionVariable.isEmpty()) {
      logError("positionVariable must not be empty.");
      return false;
    }

    var position = getTarget().getPosition().clone();
    getTarget().setVariable(positionVariable, position);
    return true;
  }

}
