package de.hsa.dice.nodes.input;

import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "ComputerMouse", category = "Input", description = "Fires on every update tick that the mouse is above the target.")
public class IsMouseOverNode extends ScriptNode {

  @Override
  protected boolean run() {
    var game = Game.getInstance();
    var input = game.getInput();
    var point = game.getCamera().pixelToWorld(input.getMousePos());
    var transform = getTarget().getBody().getTransform();
    var fixture = getTarget().getBody().getFixtureList();
    do {
      if (fixture.getShape().testPoint(transform, point))
        return true;
    } while ((fixture = fixture.getNext()) != null);
    return false;
  }

}
