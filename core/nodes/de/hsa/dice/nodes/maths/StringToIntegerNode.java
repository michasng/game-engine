package de.hsa.dice.nodes.maths;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Calculator", category = "Maths", description = "Converts a string to an integer and stores the result in a variable.")
public class StringToIntegerNode extends ScriptNode {

  @Inspect(description = "The string to convert.")
  private String string;

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("resultVariable must not be empty.");
      return false;
    }

    try {
      var result = Integer.parseInt(string);
      getTarget().setVariable(resultVariable, result);
      return true;
    } catch (NumberFormatException e) {
      logError("Cannot convert string to integer: " + string);
      return false;
    }
  }

}
