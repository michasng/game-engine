package de.hsa.dice.nodes.maths;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Invert", category = "Maths", description = "Takes a vector and splits it into it's x and y components.")
public class SplitVectorNode extends ScriptNode {

  @Inspect(description = "The vector to be split.")
  private Vec2 vector;

  @Inspect(colored = true, description = "The variable to write the x value to. Is ignored when left empty.")
  private String xVariable;

  @Inspect(colored = true, description = "The variable to write the y value to. Is ignored when left empty.")
  private String yVariable;

  @Override
  public boolean run() {
    if (!xVariable.isEmpty())
      getTarget().setVariable(xVariable, vector.x);
    if (!yVariable.isEmpty())
      getTarget().setVariable(yVariable, vector.y);
    return true;
  }

}
