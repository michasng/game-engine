package de.hsa.dice.nodes.maths;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Abs", category = "Maths", description = "Stores the absolute value of a vector in a variable.")
public class AbsVectorNode extends ScriptNode {

  @Inspect(description = "The vector to take the absolute of.")
  private Vec2 signedValue;

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("resultVariable must not be empty.");
      return false;
    }

    var result = signedValue.abs();
    getTarget().setVariable(resultVariable, result);
    return true;
  }

}
