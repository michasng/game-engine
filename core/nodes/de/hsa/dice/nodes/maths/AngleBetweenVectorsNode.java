package de.hsa.dice.nodes.maths;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Calculator", category = "Maths", description = "Calculates the angle between two vectors in degrees and stores the result in a variable.")
public class AngleBetweenVectorsNode extends ScriptNode {

  @Inspect(description = "The first operand.")
  private Vec2 op1;
  @Inspect(description = "The second operand.")
  private Vec2 op2;

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("resultVariable must not be empty.");
      return false;
    }

    var result = (float) Math.toDegrees(Math.acos(Vec2.dot(op1, op2)));
    getTarget().setVariable(resultVariable, result);
    return true;
  }

}
