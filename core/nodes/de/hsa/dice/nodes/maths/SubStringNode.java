package de.hsa.dice.nodes.maths;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Sub", category = "Maths", description = "Takes the substring of a string and stores the result in a variable.")
public class SubStringNode extends ScriptNode {

  @Inspect(description = "The string to take the substring of.")
  private String string;

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @Inspect(description = "The fist index to include in the string.")
  private int beginIndex = 0;

  @Inspect(description = "The length of the substring. Set to -1 for highest length possible.")
  private int length = -1;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("resultVariable must not be empty.");
      return false;
    }

    if (beginIndex < 0 || beginIndex >= string.length()) {
      logError("invalid beginIndex: " + beginIndex);
      return false;
    }

    if (length < -1 || length > string.length()) {
      logError("invalid length: " + length);
      return false;
    }

    if (length == -1)
      length = string.length() - beginIndex;
    var result = string.substring(beginIndex, length + beginIndex);
    getTarget().setVariable(resultVariable, result);
    return true;
  }

}
