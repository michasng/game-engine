package de.hsa.dice.nodes.maths;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Calculator", category = "Maths", description = "Stores the length of a string in a variable.")
public class GetStringLengthNode extends ScriptNode {

  @Inspect(description = "The string to get the length of.")
  private String string;

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("resultVariable must not be empty.");
      return false;
    }

    var result = string.length();
    getTarget().setVariable(resultVariable, result);
    return true;
  }

}
