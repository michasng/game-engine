package de.hsa.dice.nodes.maths;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Function", category = "Maths", description = "Sets a variable on the target.")
public class SetVariableNode extends ScriptNode {

  @Inspect(colored = true, description = "The name of the variable on the target. Must be set for this node to fire.")
  private String resultVariable;

  @Inspect(description = "The type of the variable to be set.")
  private VariableType type;

  @Inspect(description = "the Integer value, applies only if type == INT")
  private int intValue;

  @Inspect(description = "the Float value, applies only if type == FLOAT")
  private float floatValue;

  @Inspect(description = "the Boolean value, applies only if type == BOOLEAN")
  private boolean booleanValue;

  @Inspect(description = "the String value, applies only if type == STRING")
  private String StringValue;

  @Inspect(description = "the Vector value, applies only if type == VECTOR  ")
  private Vec2 vectorValue;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("variable must not be empty.");
      return false;
    }

    switch (type) {
    case INT:
      getTarget().setVariable(resultVariable, intValue);
      break;
    case FLOAT:
      getTarget().setVariable(resultVariable, floatValue);
      break;
    case BOOLEAN:
      getTarget().setVariable(resultVariable, booleanValue);
      break;
    case STRING:
      getTarget().setVariable(resultVariable, StringValue);
      break;
    case VECTOR:
      getTarget().setVariable(resultVariable, vectorValue);
      break;
    default:
      throw new Error("Undefined Variable Type: " + type);
    }

    return true;
  }

  public static enum VariableType {
    INT, FLOAT, BOOLEAN, STRING, VECTOR;
  }

}
