package de.hsa.dice.nodes.maths;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Abs", category = "Maths", description = "Stores the normalized value of a vector in a variable. The resulting vector has length 1")
public class NormVectorNode extends ScriptNode {

  @Inspect(description = "The vector to normalize.")
  private Vec2 vector;

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("resultVariable must not be empty.");
      return false;
    }

    var result = vector.clone().normalize();
    getTarget().setVariable(resultVariable, result);
    return true;
  }

}
