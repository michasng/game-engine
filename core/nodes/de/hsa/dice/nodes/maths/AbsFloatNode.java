package de.hsa.dice.nodes.maths;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Abs", category = "Maths", description = "Stores the absolute value of a floating point number in a variable.")
public class AbsFloatNode extends ScriptNode {

  @Inspect(description = "The number to take the absolute of.")
  private float signedValue;

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("resultVariable must not be empty.");
      return false;
    }

    var result = Math.abs(signedValue);
    getTarget().setVariable(resultVariable, result);
    return true;
  }

}
