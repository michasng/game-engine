package de.hsa.dice.nodes.maths;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Calculator", category = "Maths", description = "Casts a floating point number to an integer and stores the result in a variable.")
public class CastFloatToIntegerNode extends ScriptNode {

  @Inspect(description = "The value to convert.")
  private float toConvert;

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("resultVariable must not be empty.");
      return false;
    }

    var result = (int) toConvert;
    getTarget().setVariable(resultVariable, result);
    return true;
  }

}
