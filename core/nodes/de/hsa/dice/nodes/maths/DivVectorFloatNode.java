package de.hsa.dice.nodes.maths;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Div", category = "Maths", description = "Divides a vector by a floating point number and stores the result in a variable.")
public class DivVectorFloatNode extends ScriptNode {

  @Inspect(description = "The first operand.")
  private Vec2 op1;
  @Inspect(description = "The second operand.")
  private float op2;

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("resultVariable must not be empty.");
      return false;
    }

    var result = op1.mul(1 / op2);
    getTarget().setVariable(resultVariable, result);
    return true;
  }

}
