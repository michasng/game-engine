package de.hsa.dice.nodes.maths;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Div", category = "Maths", description = "Divides one floating point number by another and stores the result in a variable.")
public class DivFloatsNode extends ScriptNode {

  @Inspect(description = "The first operand.")
  private float op1;
  @Inspect(description = "The second operand.")
  private float op2;

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("resultVariable must not be empty.");
      return false;
    }

    var result = op1 / op2;
    getTarget().setVariable(resultVariable, result);
    return true;
  }

}
