package de.hsa.dice.nodes.maths;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Sqrt", category = "Maths", description = "Takes the square root of a value and stores the result in a variable.")
public class SqrtNode extends ScriptNode {

  @Inspect(description = "The base to take the square root of.")
  private float base;

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("resultVariable must not be empty.");
      return false;
    }

    var result = (float)Math.sqrt(base);
    getTarget().setVariable(resultVariable, result);
    return true;
  }

}
