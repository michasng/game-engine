package de.hsa.dice.nodes.maths;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Up", category = "Maths", description = "Creates a vector from two floating point numbers and stores it in a variable.")
public class MakeVectorNode extends ScriptNode {

  @Inspect(description = "The x value of the vector.")
  private float xValue;
  @Inspect(description = "The y value of the vector.")
  private float yValue;

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("resultVariable must not be empty.");
      return false;
    }

    var result = new Vec2(xValue, yValue);
    getTarget().setVariable(resultVariable, result);
    return true;
  }

}
