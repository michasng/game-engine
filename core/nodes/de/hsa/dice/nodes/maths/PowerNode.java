package de.hsa.dice.nodes.maths;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Pow", category = "Maths", description = "Takes one value to the power of another value and stores the result in a variable.")
public class PowerNode extends ScriptNode {

  @Inspect(description = "The base to take a power of.")
  private float base;
  @Inspect(description = "The exponent by which to take the power of base.")
  private float exponent;

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("resultVariable must not be empty.");
      return false;
    }

    var result = (float)Math.pow(base, exponent);
    getTarget().setVariable(resultVariable, result);
    return true;
  }

}
