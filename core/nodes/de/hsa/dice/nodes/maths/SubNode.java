package de.hsa.dice.nodes.maths;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Sub", category = "Maths", description = "Subtracts the value of two variables and stores the result in a variable.")
public class SubNode extends ScriptNode {

  @Inspect(colored = true, description = "The variable of the first operand.")
  private String var1;
  @Inspect(colored = true, description = "The variable of the second operand.")
  private String var2;

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("resultVariable must not be empty.");
      return false;
    }

    var o1 = getTarget().getVariable(var1);
    var o2 = getTarget().getVariable(var2);

    Object result = null;
    if (o1 instanceof Integer && o2 instanceof Integer)
      result = (int) o1 - (int) o2;
    else if (o1 instanceof Float && o2 instanceof Float)
      result = (float) o1 - (float) o2;
    else if (o1 instanceof Integer && o2 instanceof Float)
      result = (int) o1 - (float) o2;
    else if (o1 instanceof Float && o2 instanceof Integer)
      result = (float) o1 - (int) o2;
    else if (o1 instanceof String && o2 instanceof String)
      result = ((String) o1).replaceAll((String) o2, "");
    else if (o1 instanceof Vec2 && o2 instanceof Vec2)
      result = ((Vec2) o1).sub((Vec2) o2);
    else {
      logError("Subtraction not defined for " + var1 + ":" + o1 + " and " + var2 + ":" + o2);
      return false;
    }

    getTarget().setVariable(resultVariable, result);
    return true;
  }

}
