package de.hsa.dice.nodes.event;

import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 0, graphics = "Battery", category = "Event", description = "Fires on every update tick.")
public class OnUpdateNode extends ScriptNode {

  @Override
  protected boolean run() {
    return true;
  }

}
