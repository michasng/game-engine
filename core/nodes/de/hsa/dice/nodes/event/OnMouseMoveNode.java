package de.hsa.dice.nodes.event;

import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 0, graphics = "ComputerMouse", category = "Event", description = "Fires on every update tick that the mouse has moved.")
public class OnMouseMoveNode extends ScriptNode {

  @Inspect(colored = true, description = "The variable to write the current mouse position. Leave empty if not required.")
  private String mousePosVariable;

  @Inspect(colored = true, description = "The varaible to write the change in mouse position to. Leave empty if not required.")
  private String deltaMouseVariable;

  @Override
  protected boolean run() {
    var input = Game.getInstance().getInput();
    if (input.isMouseMoving()) {
      if (!mousePosVariable.isEmpty())
        getTarget().setVariable(mousePosVariable, input.getMousePos());
      if (!deltaMouseVariable.isEmpty())
        getTarget().setVariable(deltaMouseVariable, input.getDeltaMouse());
      return true;
    }
    return false;
  }

}
