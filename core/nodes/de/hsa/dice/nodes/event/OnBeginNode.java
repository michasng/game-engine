package de.hsa.dice.nodes.event;

import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 0, graphics = "Play", category = "Event", description = "Fires only on the first update tick and is guaranteed to fire before any other node in the script.")
public class OnBeginNode extends ScriptNode {

  boolean ranBefore;

  @Override
  protected boolean run() {
    if (ranBefore)
      return false;
    else
      return ranBefore = true;
  }

}
