package de.hsa.dice.nodes.event;

import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 0, graphics = "Collision", category = "Event", description = "Fires on every update tick that a contact has occurred.")
public class OnContactNode extends ScriptNode {

  @Override
  protected boolean run() {
    return getTarget().hasContacts();
  }

}
