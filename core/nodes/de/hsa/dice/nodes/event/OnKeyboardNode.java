package de.hsa.dice.nodes.event;

import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 0, graphics = "Keyboard", category = "Event", description = "Fires on every update tick that a keyboard action has occurred.")
public class OnKeyboardNode extends ScriptNode {

  @Override
  protected boolean run() {
    var input = Game.getInstance().getInput();
    return (input.isAnyKeyPressed() || input.isAnyKeyReleased());
  }

}
