package de.hsa.dice.nodes.controlflow;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Abacus", category = "ControlFlow", description = "Alternates between firing and not firing.")
public class AlternateNode extends ScriptNode {

  @Inspect(description = "Whether to fire on the first try.")
  private boolean firstValue;

  @Override
  protected boolean run() {
    // invert the value, but invert again to return the original
    return !(firstValue = !firstValue);
  }

}
