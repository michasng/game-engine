package de.hsa.dice.nodes.controlflow;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Scales", category = "ControlFlow", description = "Compares two integer numbers. Only fires, if the result is equal to the desired result.")
public class CompareIntegersNode extends ScriptNode {

  @Inspect(description = "The first number to compare.")
  private int value1;

  @Inspect(description = "The second number to compare.")
  private int value2;

  @Inspect(description = "The desired result upon which this node is fired.")
  private CompareResult desiredResult = CompareResult.EQUAL;

  @Override
  public boolean run() {
    int cmp = Integer.compare(value1, value2);
    var result = cmp < 0 ? CompareResult.SMALLER : cmp > 0 ? CompareResult.GREATER : CompareResult.EQUAL;
    return (result == desiredResult);
  }

  public enum CompareResult {
    EQUAL, SMALLER, GREATER
  }

}
