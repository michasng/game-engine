package de.hsa.dice.nodes.controlflow;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Scales", category = "ControlFlow", description = "Compares two strings in alphabetical order. Only fires, if the result is equal to the desired result.")
public class CompareStringsNode extends ScriptNode {

  @Inspect(description = "The first string to compare.")
  private String value1;

  @Inspect(description = "The second string to compare.")
  private String value2;

  @Inspect(description = "The desired result upon which this node is fired.")
  private CompareResult desiredResult = CompareResult.EQUAL;

  @Override
  public boolean run() {
    int cmp = value1.compareTo(value2);
    var result = cmp < 0 ? CompareResult.SMALLER : cmp > 0 ? CompareResult.GREATER : CompareResult.EQUAL;
    return (result == desiredResult);
  }

  public enum CompareResult {
    EQUAL, SMALLER, GREATER
  }

}
