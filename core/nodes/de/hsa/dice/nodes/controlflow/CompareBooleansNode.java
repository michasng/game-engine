package de.hsa.dice.nodes.controlflow;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Scales", category = "ControlFlow", description = "Compares two booleans. Only fires, if they are equal")
public class CompareBooleansNode extends ScriptNode {

  @Inspect(description = "The first boolean to compare.")
  private boolean value1;

  @Inspect(description = "The second boolean to compare.")
  private boolean value2;

  @Override
  public boolean run() {
    return value1 == value2;
  }

}
