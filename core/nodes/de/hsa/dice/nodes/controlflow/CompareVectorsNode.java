package de.hsa.dice.nodes.controlflow;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Scales", category = "ControlFlow", description = "Compares the absolute length of two vectors. Only fires, if the result is equal to the desired result.")
public class CompareVectorsNode extends ScriptNode {

  @Inspect(description = "The first vector to compare.")
  private Vec2 value1;

  @Inspect(description = "The second vector to compare.")
  private Vec2 value2;

  @Inspect(description = "The desired result upon which this node is fired.")
  private CompareResult desiredResult = CompareResult.EQUAL;

  @Override
  public boolean run() {
    int cmp = Float.compare((value1).lengthSquared(), (value2).lengthSquared());
    var result = cmp < 0 ? CompareResult.SMALLER : cmp > 0 ? CompareResult.GREATER : CompareResult.EQUAL;
    return (result == desiredResult);
  }

  public enum CompareResult {
    EQUAL, SMALLER, GREATER
  }

}
