package de.hsa.dice.nodes.controlflow;

import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 2, graphics = "Or", category = "ControlFlow", description = "Fires only when one or both of the inputs has/have fired.")
public class OrNode extends ScriptNode {

  @Override
  protected boolean run() {
    // no need for this, because always run is not set to true
    // return getInput(0) || getInput(1);
    return true;
  }

}
