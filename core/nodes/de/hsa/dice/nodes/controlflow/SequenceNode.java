package de.hsa.dice.nodes.controlflow;

import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "DotGraph", category = "ControlFlow", description = "Does nothing, is only an intersection that runs sequentially.")
public class SequenceNode extends ScriptNode {

  @Override
  protected boolean run() {
    return true;
  }

}
