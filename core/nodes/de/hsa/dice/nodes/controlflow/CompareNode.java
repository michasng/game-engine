package de.hsa.dice.nodes.controlflow;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Scales", category = "ControlFlow", description = "Compares two values of types Integer, Float, Vector or String and only fires, if the result is equal to the desired result.")
public class CompareNode extends ScriptNode {

  @Inspect(colored = true, description = "The first variable to compare.")
  private String var1;

  @Inspect(colored = true, description = "The second variable to compare.")
  private String var2;

  @Inspect(description = "The desired result upon which this node is fired.")
  private CompareResult desiredResult = CompareResult.EQUAL;

  @Override
  public boolean run() {
    var o1 = getTarget().getVariable(var1);
    var o2 = getTarget().getVariable(var2);

    int cmp;
    if (o1 instanceof Integer && o2 instanceof Integer)
      cmp = Integer.compare((int) o1, (int) o2);
    else if ((o1 instanceof Float && o2 instanceof Float) || (o1 instanceof Integer && o2 instanceof Float)
        || (o1 instanceof Float && o2 instanceof Integer))
      cmp = Float.compare((float) o1, (float) o2);
    else if (o1 instanceof String && o2 instanceof String)
      cmp = ((String) o1).compareTo((String) o2);
    else if (o1 instanceof Vec2 && o2 instanceof Vec2)
      cmp = Float.compare(((Vec2) o1).lengthSquared(), ((Vec2) o2).lengthSquared());
    else {
      logError("Compare not defined for var1:" + var1 + " and var2:" + var2);
      return false;
    }

    var result = cmp < 0 ? CompareResult.SMALLER : cmp > 0 ? CompareResult.GREATER : CompareResult.EQUAL;
    return (result == desiredResult);
  }

  public enum CompareResult {
    EQUAL, SMALLER, GREATER
  }

}
