package de.hsa.dice.nodes.controlflow;

import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 2, graphics = "And", category = "ControlFlow", description = "Fires only when both inputs have fired.")
public class AndNode extends ScriptNode {

  @Override
  protected boolean run() {
    return getInput(0) && getInput(1);
  }

}
