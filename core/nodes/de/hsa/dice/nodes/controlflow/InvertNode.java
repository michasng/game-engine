package de.hsa.dice.nodes.controlflow;

import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, alwaysRun = true, graphics = "Not", category = "ControlFlow", description = "Fires only if the input didn't fire.")
public class InvertNode extends ScriptNode {

  @Override
  protected boolean run() {
    return !getInput(0);
  }

}
