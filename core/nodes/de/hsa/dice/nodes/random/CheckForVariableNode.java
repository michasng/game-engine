package de.hsa.dice.nodes.random;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Function", category = "Maths", description = "Fires only if a variable exists on the target.")
public class CheckForVariableNode extends ScriptNode {

  @Inspect(colored = true, description = "The name of the variable on the target. Must be set for this node to fire.")
  private String variable;

  @Override
  public boolean run() {
    if (variable.isEmpty()) {
      logError("variable must not be empty.");
      return false;
    }

    var obj = getTarget().getVariable(variable);
    return (obj != null);
  }

}
