package de.hsa.dice.nodes.random;

import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Fullscreen", category = "Random", description = "Sets or unsets the fullscreen mode.")
public class SetFullscreenNode extends ScriptNode {

  @Inspect(description = "Whether to set the screen to fullscreen.")
  protected boolean fullscreen;

  @Override
  protected boolean run() {
    ProjectData.getCurrent().setFullscreen(fullscreen);
    return true;
  }

}
