package de.hsa.dice.nodes.random;

import java.util.Random;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Calculator", category = "Random", description = "Stores a random floating point number between 0 (included) and 1 (excluded) in a variable.")
public class RandomFloatNode extends ScriptNode {

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("ResultVariable must not be empty.");
      return false;
    }

    var result = new Random().nextFloat();
    getTarget().setVariable(resultVariable, result);
    return true;
  }

}
