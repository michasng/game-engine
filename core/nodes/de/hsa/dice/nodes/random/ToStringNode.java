package de.hsa.dice.nodes.random;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Console", category = "Random", description = "Converts a given value to a String and stores it in another variable.")
public class ToStringNode extends ScriptNode {

  @Inspect(colored = true, description = "The variable to be converted to a String. Must not be empty.")
  protected String originalVariable;

  @Inspect(colored = true, description = "The variable in which to store the converted value (optional).")
  protected String stringVariable;

  @Override
  protected boolean run() {
    if (originalVariable.isEmpty()) {
      logError("originalVariable must not be empty.");
      return false;
    }

    var obj = getTarget().getVariable(originalVariable);
    if (obj == null) {
      logError("Value of variable originalVariable:" + originalVariable + " does not exist.");
      return false;
    }

    var str = obj.toString();
    if (!stringVariable.isEmpty()) {
      getTarget().setVariable(stringVariable, str);
    }
    return true;
  }

}
