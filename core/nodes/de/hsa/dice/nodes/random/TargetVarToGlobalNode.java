package de.hsa.dice.nodes.random;

import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Function", category = "Maths", description = "Sets a variable from the target to the global variables.")
public class TargetVarToGlobalNode extends ScriptNode {

  @Inspect(colored = true, description = "The name of the variable on the target. Must be set for this node to fire.")
  private String targetVariable;

  @Inspect(colored = true, description = "The name of the global variable. Must be set for this node to fire.")
  private String globalVariable;

  @Override
  public boolean run() {
    if (targetVariable.isEmpty()) {
      logError("targetVariable must not be empty.");
      return false;
    }
    if (globalVariable.isEmpty()) {
      logError("globalVariable must not be empty.");
      return false;
    }

    var targetVar = getTarget().getVariable(targetVariable);
    if (targetVar == null) {
      logError("Variable " + targetVariable + " does not exist.");
      return false;
    }

    Game.getInstance().setGlobalVariable(globalVariable, targetVar);
    return true;
  }

}
