package de.hsa.dice.nodes.random;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Console", category = "Random", description = "Logs/Prints a given value to the console.")
public class LogNode extends ScriptNode {

  @Inspect(description = "The value to be logged.")
  protected String value;

  @Override
  protected boolean run() {
    Console.log(value);
    return true;
  }

}
