package de.hsa.dice.nodes.random;

import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Function", category = "Maths", description = "Gets a variable from the globals and sets it on the target.")
public class GlobalVarToTargetNode extends ScriptNode {

  @Inspect(colored = true, description = "The name of the global variable. Must be set for this node to fire.")
  private String globalVariable;

  @Inspect(colored = true, description = "The name of the variable on the target. Must be set for this node to fire.")
  private String targetVariable;

  @Override
  public boolean run() {
    if (targetVariable.isEmpty()) {
      logError("targetVariable must not be empty.");
      return false;
    }
    if (globalVariable.isEmpty()) {
      logError("globalVariable must not be empty.");
      return false;
    }

    var globalVar = Game.getInstance().getGlobalVariable(globalVariable);
    if (globalVar == null) {
      logError("Global variable " + globalVariable + " does not exist.");
      return false;
    }

    getTarget().setVariable(targetVariable, globalVar);
    return true;
  }

}
