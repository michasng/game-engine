package de.hsa.dice.nodes.random;

import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Fullscreen", category = "Random", description = "Runs only when screen is fullscreen.")
public class IsFullscreenNode extends ScriptNode {

  @Override
  protected boolean run() {
    return ProjectData.getCurrent().isFullscreen();
  }

}
