package de.hsa.dice.nodes.random;

import java.util.Random;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Calculator", category = "Random", description = "Stores a random integer number in a variable.")
public class RandomIntegerNode extends ScriptNode {

  @Inspect(description = "The lowest possible number to generate (included).")
  private int lowerLimit = 0;

  @Inspect(description = "The number right above the highest possible number to generate (excluded). So if this is 100, the highest possible number would be 99.")
  private int upperLimit = 100;

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @Override
  public boolean run() {
    if (resultVariable.isEmpty()) {
      logError("ResultVariable must not be empty.");
      return false;
    }

    var result = lowerLimit + new Random().nextInt(upperLimit - lowerLimit);
    getTarget().setVariable(resultVariable, result);
    return true;
  }

}
