package de.hsa.dice.nodes.random;

import de.hsa.dice.core.dataobject.ProjectData;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Fullscreen", category = "Random", description = "Sets or unsets the maximized mode.")
public class SetMaximizedNode extends ScriptNode {

  @Inspect(description = "Whether to set the screen to maximized.")
  protected boolean maximized;

  @Override
  protected boolean run() {
    ProjectData.getCurrent().setMaximized(maximized);
    return true;
  }

}
