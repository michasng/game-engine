package de.hsa.dice.nodes.sound;

import javax.sound.sampled.Clip;

import de.hsa.dice.core.game.SoundPlayer;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Music", category = "Sound", description = "Stops a soundeffect.")
public class StopSoundNode extends ScriptNode {

  @Inspect(description = "The name of the soundeffect variable.")
  private String soundVariable;

  @Override
  protected boolean run() {
    var obj = getTarget().getVariable(soundVariable);
    if (!(obj instanceof Clip)) {
      logError("Invalid variable " + soundVariable + ":" + obj + " (not a soundeffect).");
      return false;
    }
    var clip = (Clip) obj;

    SoundPlayer.stop(clip);
    return true;
  }

}
