package de.hsa.dice.nodes.sound;

import java.io.FileNotFoundException;

import de.hsa.dice.core.Console;
import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.SoundData;
import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.game.SoundPlayer;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;
import de.hsa.dice.core.util.io.ProjectFileOperator;

@Node(numInputs = 1, graphics = "Music", category = "Sound", description = "Loads a soundeffect and saves it to a variable.")
public class LoadSoundNode extends ScriptNode {

  @Inspect(description = "The name of the soundeffect.")
  private String soundName;

  @Inspect(description = "The variable to save the soundeffect to.")
  private String soundVariable = "sound";

  @Override
  protected boolean run() {
    var soundManager = Game.getInstance().getProjectData().getSubManager(DataType.SOUND, SoundData.class);
    var soundData = soundManager.get(soundName);
    if (soundData == null) {
      logError("Sound " + soundName + " does not exist.");
      return false;
    }

    var prjFileOp = ProjectFileOperator.getInstance();
    var absPath = prjFileOp.getAbsolutePath(soundData.getPath());
    try {
      var clip = SoundPlayer.loadExternalClip(absPath);
      getTarget().setVariable(soundVariable, clip);
    } catch (FileNotFoundException e) {
      Console.logError(e);
      return false;
    }
    return true;
  }

}
