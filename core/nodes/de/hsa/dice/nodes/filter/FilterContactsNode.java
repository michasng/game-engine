package de.hsa.dice.nodes.filter;

import java.util.LinkedList;

import de.hsa.dice.core.game.entity.Entity.EntityContact;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Filter", category = "Filter", description = "Filteres contacts and writes the result in a variable. Fires only if any contacts fit the filter.")
public class FilterContactsNode extends ScriptNode {

  @Inspect(description = "When not empty, only entities with this script-component will go through the filter.")
  private String componentFilter = "";

  @Inspect(description = "Whether tiles should be filtered out.")
  private boolean ignoreTiles = false;

  @Inspect(description = "Whether entities should be filtered out.")
  private boolean ignoreEntities = false;

  @Inspect(description = "The variable in which to store the filtered contacts.")
  private String filteredContactsVar = "filteredContacts";

  @Override
  protected boolean run() {
    var contacts = getTarget().getContacts();
    var filteredContacts = new LinkedList<EntityContact>();

    for (var contact : contacts) {
      if (ignoreEntities && contact.other != null)
        continue;
      if (ignoreTiles && contact.other == null)
        continue;
      if (!componentFilter.isEmpty() && contact.other != null && !contact.other.hasScript(componentFilter))
        continue;
      filteredContacts.add(contact);
    }
    getTarget().setVariable(filteredContactsVar, filteredContacts);
    return (!filteredContacts.isEmpty());
  }

}
