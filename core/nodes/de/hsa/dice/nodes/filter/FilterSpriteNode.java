package de.hsa.dice.nodes.filter;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Filter", category = "Filter", description = "Fires only if the target has the specified sprite set.")
public class FilterSpriteNode extends ScriptNode {

  @Inspect(description = "The name of the sprite to search on the target.")
  private String spriteName;

  @Override
  protected boolean run() {
    var sprite = getTarget().getSprite();
    return (sprite != null && sprite.getDataObject().getName().equals(spriteName));
  }

}
