package de.hsa.dice.nodes.filter;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Filter", category = "Filter", description = "Fires only if the target has the specified script component.")
public class FilterScriptNode extends ScriptNode {

  @Inspect(description = "The name of the script to search on the target.")
  private String scriptName;

  @Override
  protected boolean run() {
    return getTarget().hasScript(scriptName);
  }

}
