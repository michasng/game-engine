package de.hsa.dice.nodes.filter;

import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;
import javafx.scene.input.KeyCode;

@Node(numInputs = 1, graphics = "Filter", category = "Filter", description = "Filteres keyboard actions. Fires only if the specified key is in the specified mode.")
public class FilterKeyboardNode extends ScriptNode {

  @Inspect(description = "The required mode for the key.")
  private KeyMode mode = KeyMode.CLICKED;

  @Inspect(description = "The keyCode to search for.")
  private KeyCode keyCode;

  @Override
  protected boolean run() {
    switch (mode) {
    case CLICKED:
      return Game.getInstance().getInput().isKeyClicked(keyCode);
    case PRESSED:
      return Game.getInstance().getInput().isKeyPressed(keyCode);
    case RELEASED:
      return Game.getInstance().getInput().isKeyReleased(keyCode);
    default:
      throw new Error("Invalid KeyMode: " + mode);
    }
  }

  private enum KeyMode {
    CLICKED, PRESSED, RELEASED
  }

}
