package de.hsa.dice.nodes.map;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "VideoCamera", category = "Map", description = "Focus the camera on a given world position.")
public class SetCameraFocusNode extends ScriptNode {

  @Inspect(description = "The world position to focus the camera on.")
  private Vec2 position;

  @Override
  protected boolean run() {
    var game = Game.getInstance();
    game.getCamera().focusWorld(position.x, position.y);
    return true;
  }

}
