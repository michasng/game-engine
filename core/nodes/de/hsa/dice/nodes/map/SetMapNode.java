package de.hsa.dice.nodes.map;

import de.hsa.dice.core.dataobject.DataType;
import de.hsa.dice.core.dataobject.MapData;
import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.game.map.TileMap;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "TreasureMap", category = "Map", description = "Changes the current tile-map to a specified one. Runs after all scripts, because it will destroy existing entities.")
public class SetMapNode extends ScriptNode {

  @Inspect(description = "The name of the map to change to. Node won't fire if this is empty.")
  private String mapName;

  @Override
  protected boolean run() {
    if (mapName.isEmpty())
      return false;

    var game = Game.getInstance();
    MapData mapData = game.getProjectData().getSubManager(DataType.MAP, MapData.class).get(mapName);
    if (mapData == null) {
      logError("Map" + mapName + " does not exist.");
      return false;
    }
    game.addToPostUpdate(() -> game.setMap(new TileMap(mapData)));
    return true;
  }

}
