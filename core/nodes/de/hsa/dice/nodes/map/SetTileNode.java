package de.hsa.dice.nodes.map;

import org.jbox2d.common.Vec2;

import de.hsa.dice.core.game.Game;
import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Tiles", category = "Map", description = "Changes the tile at a given position.")
public class SetTileNode extends ScriptNode {

  @Inspect(description = "The position to change the tile at.")
  private String worldPosVariable;

  @Inspect(description = "The name of the tile to be changed on the map")
  private String tileName;

  @Override
  protected boolean run() {
    var obj = getTarget().getVariable(worldPosVariable);
    if (!(obj instanceof Vec2)) {
      logError("worldPosVariable " + worldPosVariable + " is not a Vector.");
      return false;
    }
    var worldPos = (Vec2) obj;

    var tileId = Game.getInstance().getMap().getTileId(tileName);
    if (tileId == -1) {
      logError("Tile does not exist: " + tileId);
      return false;
    }
    Game.getInstance().getMap().setTile(worldPos, tileId);
    return true;
  }

}
