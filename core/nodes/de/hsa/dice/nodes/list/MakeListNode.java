package de.hsa.dice.nodes.list;

import java.util.LinkedList;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Add", category = "List", description = "Creates the list and stores it in another variable.")
public class MakeListNode extends ScriptNode {

  @Inspect(colored = true, description = "The variable to write the resulting list to. Must not be empty.")
  private String listVariable;

  @Override
  public boolean run() {
    if (listVariable.isEmpty()) {
      logError("listVariable must not be empty.");
      return false;
    }

    LinkedList<Object> list = new LinkedList<>();
    getTarget().setVariable(listVariable, list);
    return true;
  }

}
