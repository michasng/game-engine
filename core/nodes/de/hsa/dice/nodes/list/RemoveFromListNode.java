package de.hsa.dice.nodes.list;

import java.util.LinkedList;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Sub", category = "List", description = "Removes by value or index from a list in another variable. Fires only if removal was successful.")
public class RemoveFromListNode extends ScriptNode {

  @Inspect(colored = true, description = "The variable containing the list. Must not be empty.")
  private String listVariable;

  @Inspect(description = "The mode used to remove an item from the list.")
  private RemoveMode removeMode;

  @Inspect(colored = true, description = "The value to remove from the list. Must not be empty if removeMode is BY_VALUE.")
  private String valueVariable;

  @Inspect(description = "The index by which to remove from the list. Must not be empty if removeMode is BY_INDEX.")
  private int index;

  @SuppressWarnings("unchecked")
  @Override
  public boolean run() {
    if (listVariable.isEmpty()) {
      logError("listVariable must not be empty.");
      return false;
    }

    var listObj = getTarget().getVariable(listVariable);
    if (!(listObj instanceof LinkedList)) {
      logError("listVariable does not contain a list.");
      return false;
    }
    LinkedList<Object> list = (LinkedList<Object>) listObj;

    switch (removeMode) {
    case BY_VALUE:
      if (valueVariable.isEmpty()) {
        logError("valueVariable must not be empty.");
        return false;
      }
      var value = getTarget().getVariable(valueVariable);
      return list.remove(value);
    case BY_INDEX:
      if (index < 0 || index >= list.size()) {
        logError("Index is not within list (size:" + list.size() + ") boundaries: " + index);
        return false;
      }
      return (list.remove(index) != null);
    default:
      logError("A strange error occurred. Calling unreachable code.");
      return false;
    }
  }

  private enum RemoveMode {
    BY_VALUE, BY_INDEX;
  }

}
