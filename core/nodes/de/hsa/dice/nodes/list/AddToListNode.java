package de.hsa.dice.nodes.list;

import java.util.LinkedList;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Add", category = "List", description = "Adds the value of a variable to a list in another variable.")
public class AddToListNode extends ScriptNode {

  @Inspect(colored = true, description = "The variable containing the list. Must not be empty.")
  private String listVariable;

  @Inspect(colored = true, description = "The value to add to the list. Must not be empty.")
  private String valueVariable;

  @SuppressWarnings("unchecked")
  @Override
  public boolean run() {
    if (listVariable.isEmpty()) {
      logError("listVariable must not be empty.");
      return false;
    }
    if (valueVariable.isEmpty()) {
      logError("valueVariable must not be empty.");
      return false;
    }

    var listObj = getTarget().getVariable(listVariable);
    if (!(listObj instanceof LinkedList)) {
      logError("listVariable does not contain a list.");
      return false;
    }
    LinkedList<Object> list = (LinkedList<Object>) listObj;

    var value = getTarget().getVariable(valueVariable);
    list.add(value);

    return true;
  }

}
