package de.hsa.dice.nodes.list;

import java.util.LinkedList;

import de.hsa.dice.core.script.Inspect;
import de.hsa.dice.core.script.Node;
import de.hsa.dice.core.script.ScriptNode;

@Node(numInputs = 1, graphics = "Function", category = "List", description = "Gets the size of a list in a variable and writes it to a variable.")
public class GetListSizeNode extends ScriptNode {

  @Inspect(colored = true, description = "The variable containing the list. Must not be empty.")
  private String listVariable;

  @Inspect(colored = true, description = "The variable to write the result to. Must not be empty.")
  private String resultVariable;

  @SuppressWarnings("unchecked")
  @Override
  public boolean run() {
    if (listVariable.isEmpty()) {
      logError("listVariable must not be empty.");
      return false;
    }
    if (resultVariable.isEmpty()) {
      logError("resultVariable must not be empty.");
      return false;
    }

    var listObj = getTarget().getVariable(listVariable);
    if (!(listObj instanceof LinkedList)) {
      logError("listVariable does not contain a list.");
      return false;
    }
    LinkedList<Object> list = (LinkedList<Object>) listObj;

    getTarget().setVariable(resultVariable, list.size());
    return true;
  }

}
