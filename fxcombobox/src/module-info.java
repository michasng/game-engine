module fxcombobox {
  exports fxcombobox;

  requires java.logging;

  requires javafx.base;
  requires javafx.graphics;
  requires transitive javafx.controls;
}