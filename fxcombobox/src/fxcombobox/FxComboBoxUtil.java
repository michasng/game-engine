package fxcombobox;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.ListView;
import javafx.scene.control.skin.ComboBoxListViewSkin;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class FxComboBoxUtil extends Application {

  private static final Logger logger = Logger.getLogger(FxComboBoxUtil.class.getName());

  public static class HideableItem<T> {
    private final ObjectProperty<T> object = new SimpleObjectProperty<>();
    private final BooleanProperty hidden = new SimpleBooleanProperty();

    private HideableItem(T object) {
      setObject(object);
    }

    private ObjectProperty<T> objectProperty() {
      return this.object;
    }

    public T getObject() {
      return this.objectProperty().get();
    }

    private void setObject(T object) {
      this.objectProperty().set(object);
    }

    private BooleanProperty hiddenProperty() {
      return this.hidden;
    }

    public boolean isHidden() {
      return this.hiddenProperty().get();
    }

    private void setHidden(boolean hidden) {
      this.hiddenProperty().set(hidden);
    }

    @Override
    public String toString() {
      return getObject() == null ? null : getObject().toString();
    }
  }

//  public static enum Numbers {
//    ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN
//  }

  public void start(Stage stage) {
    String[] countriesArr = { "Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antigua and Barbuda",
        "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados",
        "Belarus", "Belgium", "Belize", "Benin", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil",
        "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cabo Verde", "Cambodia", "Cameroon", "Canada",
        "Central African Republic (CAR)", "Chad", "Chile", "China", "Colombia", "Comoros",
        "Democratic Republic of the Congo", "Republic of the Congo", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba",
        "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt",
        "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Fiji", "Finland", "France", "Gabon",
        "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana",
        "Haiti", "Honduras", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy",
        "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos",
        "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg",
        "Macedonia (FYROM)", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands",
        "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Morocco",
        "Mozambique", "Myanmar (Burma)", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua",
        "Niger", "Nigeria", "North Korea", "Norway", "Oman", "Pakistan", "Palau", "Palestine", "Panama",
        "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Qatar", "Romania", "Russia",
        "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino",
        "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore",
        "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "South Sudan", "Spain",
        "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan",
        "Tanzania", "Thailand", "Timor-Leste", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey",
        "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates (UAE)", "United Kingdom (UK)",
        "United States of America (USA)", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City (Holy See)", "Venezuela",
        "Vietnam", "Yemen", "Zambia", "Zimbabwe" };
    List<String> countries = Arrays.asList(countriesArr);

//    List<Numbers> numbers = Arrays.asList(Numbers.values());
//    ComboBox<HideableItem<Numbers>> comboBox = createComboBoxWithAutoCompletionSupport(numbers);

    ComboBox<HideableItem<String>> comboBox = createComboBoxWithAutoCompletionSupport(countries);
    comboBox.setMaxWidth(Double.MAX_VALUE);

    HBox root = new HBox();
    root.getChildren().add(comboBox);

    Scene scene = new Scene(root);
    stage.setScene(scene);
    stage.show();

    comboBox.setMinWidth(comboBox.getWidth());
    comboBox.setPrefWidth(comboBox.getWidth());
  }

  public static void main(String[] args) {
    launch();
  }

  public static <T> ComboBox<HideableItem<T>> createComboBoxWithAutoCompletionSupport(Iterable<T> items) {
    ObservableList<HideableItem<T>> hideableHideableItems = FXCollections
        .observableArrayList(hideableItem -> new Observable[] { hideableItem.hiddenProperty() });

    items.forEach(item -> {
      HideableItem<T> hideableItem = new HideableItem<>(item);
      hideableHideableItems.add(hideableItem);
    });

    FilteredList<HideableItem<T>> filteredHideableItems = new FilteredList<>(hideableHideableItems, t -> !t.isHidden());

    ComboBox<HideableItem<T>> comboBox = new ComboBox<>();
    comboBox.setItems(filteredHideableItems);

    @SuppressWarnings("unchecked")
    HideableItem<T>[] selectedItem = (HideableItem<T>[]) new HideableItem[1];

    comboBox.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
      logger.finest("key pressed: " + event.getCode());
      if (!comboBox.isShowing())
        return;

      comboBox.setEditable(true);
      comboBox.getEditor().clear();
    });

    comboBox.showingProperty().addListener((observable, oldValue, newValue) -> {
      if (newValue) {
        @SuppressWarnings("unchecked")
        ListView<HideableItem<T>> lv = (ListView<HideableItem<T>>) ((ComboBoxListViewSkin<?>) comboBox.getSkin())
            .getPopupContent();

        Platform.runLater(() -> {
          if (selectedItem[0] == null) // first use
          {
            double cellHeight = ((Control) lv.lookup(".list-cell")).getHeight();
            lv.setFixedCellSize(cellHeight);
          }
        });

        lv.scrollTo(comboBox.getValue());
      } else {
        logger.finest("no longer showing");
        HideableItem<T> value = comboBox.getValue();
        if (value != null)
          selectedItem[0] = value;

        comboBox.setEditable(false);

        Platform.runLater(() -> {
          logger.finest("selecting " + selectedItem[0]);
          comboBox.getSelectionModel().select(selectedItem[0]);
          comboBox.setValue(selectedItem[0]);
        });
      }
    });

    comboBox.setOnHidden(event -> hideableHideableItems.forEach(item -> item.setHidden(false)));

    comboBox.getEditor().textProperty().addListener((obs, oldValue, newValue) -> {
      logger.finest("text changed: " + newValue);

      if (!comboBox.isShowing())
        return;

      Platform.runLater(() -> {
        if (comboBox.getSelectionModel().getSelectedItem() == null) {
          hideableHideableItems.forEach(
              item -> item.setHidden(!item.getObject().toString().toLowerCase().contains(newValue.toLowerCase())));
        } else {
          boolean validText = false;

          for (HideableItem<T> hideableItem : hideableHideableItems) {
            if (hideableItem.getObject().toString().equals(newValue)) {
              validText = true;
              break;
            }
          }

          if (!validText)
            comboBox.getSelectionModel().select(null);
        }
      });
    });

    return comboBox;
  }

  public static <T> void setValue(ComboBox<HideableItem<T>> comboBox, T item) {
    for (var cItem : comboBox.getItems()) {
      if (cItem.getObject().equals(item)) {
        comboBox.setValue(cItem);
        logger.finest("setValue: item found: " + cItem.getObject());
        return;
      }
    }
    logger.warning("setValue: no item found: " + item);
  }

}
